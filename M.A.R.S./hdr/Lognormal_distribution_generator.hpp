#ifndef LOGNORMAL_DISTRIBUTION_GENERATOR_HPP_
#define LOGNORMAL_DISTRIBUTION_GENERATOR_HPP_

std::vector<double> Lognormal_generator(double mu, double sigma, unsigned int size=1);

#endif
