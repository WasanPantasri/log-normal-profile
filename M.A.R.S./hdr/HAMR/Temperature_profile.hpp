/*
 * temperature_profile.hpp
 *
 *  Created on: 5 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Temperature_profile.hpp
 * \brief Header file for HAMR temperature profile functions. */

#ifndef TEMPERATURE_PROFILE_HPP_
#define TEMPERATURE_PROFILE_HPP_

#include "../../hdr/Structures.hpp"

extern int Temperature_profile(const Expt_laser_t, const double, double*, std::vector<double>*);

extern int Temperature_profile_spatial(const int,const Voronoi_t,const Expt_laser_t,const double,const double,const double, double*, Grain_t*);

extern int Temperature_profile_spatial_cont(const int,const Voronoi_t,const Expt_laser_t,const double,const double,const double,const double, double*, Grain_t*);

#endif /* TEMPERATURE_PROFILE_HPP_ */
