/*
 * HAMR_localised_write.hpp
 *
 *  Created on: 2 Oct 2018
 *      Author: ewan
 */

/** \file HAMR_localised_write.hpp
 * \brief Header file for simulation of HAMR on a centralised bit. */

#ifndef HAMR_LOCALISED_WRITE_HPP_
#define HAMR_LOCALISED_WRITE_HPP_

#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int HAMR_localised_write(ConfigFile);

#endif /* HAMR_LOCALISED_WRITE_HPP_ */
