/*
 * HAMR_Write_Data_continuous.hpp
 *
 *  Created on: 09 Sep 2019
 *      Author: Andrea Meo
 */

/** \file HAMR_Write_Data_continuous.hpp
 * \brief  Header file for simulation of HAMR writing process with a continuously moving write head. */

#ifndef HAMR_WRITE_DATA_CONT_HPP_
#define HAMR_WRITE_DATA_CONT_HPP_

#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int HAMR_write_data_cont(ConfigFile);

#endif /* HAMR_WRITE_DATA_CONT_HPP_ */
