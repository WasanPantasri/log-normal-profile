/*
 * Thermoremanence.hpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

/** \file Thermoremanence.hpp
 * \brief Header file for thermoremanence simulation. */

#ifndef SIGMA_TC_HPP_
#define SIGMA_TC_HPP_

extern int Thermoremanence(const ConfigFile cfg);

#endif /* SIGMA_TC_HPP_ */
