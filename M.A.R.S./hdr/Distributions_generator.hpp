/* Distributions_generator.hpp
 *  Created on: 31 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Distributions_generator.hpp
 * \brief Header file for Distribution generator function.  */
#ifndef DISTRIBUTIONS_GENERATOR_HPP_
#define DISTRIBUTIONS_GENERATOR_HPP_

#include <random>
#include <vector>

extern int Dist_gen(const unsigned int Num_grains,const std::string Type, const double Mean, const double StdDev, std::vector<double>*Data);

extern int Tc_dist_gen(const unsigned int Num_grains, std::vector<double> Grain_diameter, const double Tc_inf, std::vector<double>*Data);

#endif /* DISTRIBUTIONS_GENERATOR_HPP_ */
