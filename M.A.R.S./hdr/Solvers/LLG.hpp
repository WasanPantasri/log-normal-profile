/* LLG.hpp
 *  Created on: 30 Apr 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file LLG.hpp
 * \brief Header file for LLG solver function. */

#ifndef LLG_HPP_
#define LLG_HPP_

#include "../../hdr/Structures.hpp"

extern int LL_G(const int, const int, const Interaction_t, const Voronoi_t, const LLG_t,const bool,Grain_t*,const std::string="");

#endif /* LLG_HPP_ */
