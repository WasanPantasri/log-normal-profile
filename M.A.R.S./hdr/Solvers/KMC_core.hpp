/*
 * KMC_core.hpp
 *
 *  Created on: 1 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file KMC_core.hpp
 * \brief Header file for kMC core function. */

#ifndef KMC_HPP_
#define KMC_HPP_

#include <vector>

extern int KMC_core(const std::vector<double>,const std::vector<double>,const double,const double,const double,const double,std::vector<double>*);

#endif /* KMC_HPP_ */
