/* LLG_import.hpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file LLG_import.hpp
 * \brief Header file for LLG import function. */

#ifndef LLG_IMPORT_HPP_
#define LLG_IMPORT_HPP_

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int LLG_import(const ConfigFile,const std::vector<ConfigFile>,const int, LLG_t*);

#endif /* LLG_IMPORT_HPP_ */
