/*
 * Read_back_import.hpp
 *
 *  Created on: 7 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Read_back_import.hpp
 * \brief Header file for system import function. */

#ifndef READ_BACK_IMPORT_HPP_
#define READ_BACK_IMPORT_HPP_

#include "../../../hdr/Structures.hpp"
#include "../../../hdr/Config_File/ConfigFile_import.hpp"

extern int Read_Back_import(const ConfigFile cfg, Expt_data_read_t*Expt_data_read);

#endif /* READ_BACK_IMPORT_HPP_ */
