/*
 * Experiment_time_evolution_import.hpp
 *
 *  Created on: 2 Dec 2019
 *      Author: Ewan Rannala
 */

/** \file Experiment_time_evolution_import.hpp
 * \brief Header file for time evolution simulation parameters import function. */

#ifndef EXPERIMENT_TIME_EVOLUTION_IMPORT_HPP_
#define EXPERIMENT_TIME_EVOLUTION_IMPORT_HPP_

#include "../../../hdr/Config_File/ConfigFile_import.hpp"

extern int Time_evol_import(const ConfigFile cfg, SNR_t*SNR_data);

#endif /* EXPERIMENT_TIME_EVOLUTION_IMPORT_HPP_ */
