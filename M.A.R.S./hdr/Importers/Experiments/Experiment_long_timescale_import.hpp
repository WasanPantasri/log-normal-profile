/*
 * Experiment_long_timescale_import.hpp
 *
 *  Created on: 5 Dec 2018
 *      Author: Ewan Rannala
 */

/** \file Experiment_long_timescale_import.hpp
 * \brief Header function for Long timescale simulations parameters import function. */

#ifndef EXPERIMENT_HAMR_LONG_TIMESCALE_IMPORT_HPP_
#define EXPERIMENT_HAMR_LONG_TIMESCALE_IMPORT_HPP_

#include <string>

#include "../../../hdr/Structures.hpp"
#include "../../../hdr/Config_File/ConfigFile_import.hpp"

extern int Long_timescale_import(const ConfigFile, const std::string,Expt_data_write_t*,Expt_laser_t*,
		   	   	   	   	   	   	   	  Expt_H_app_t*,Expt_timings_t*);

#endif /* EXPERIMENT_HAMR_LONG_TIMESCALE_IMPORT_HPP_ */
