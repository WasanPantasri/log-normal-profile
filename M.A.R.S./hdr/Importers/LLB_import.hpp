/* LLB_import.hpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file LLB_import.hpp
 * \brief Header file for LLB import function. */

#ifndef LLB_IMPORT_HPP_
#define LLB_IMPORT_HPP_

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int LLB_import(const ConfigFile,const std::vector<ConfigFile>,const int, LLB_t*);

#endif /* LLB_IMPORT_HPP_ */
