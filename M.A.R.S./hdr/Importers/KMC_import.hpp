/*
 * KMC_import.hpp
 *
 *  Created on: 11 Dec 2018
 *      Author: Ewan Rannala
 */

/** \file KMC_import.hpp
 * \brief Header file for kMC import function. */

#ifndef KMC_IMPORT_HPP_
#define KMC_IMPORT_HPP_

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int KMC_import(const ConfigFile, KMC_t*);

#endif /* KMC_IMPORT_HPP_ */
