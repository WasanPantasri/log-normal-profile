/*
 * Solver.h
 *
 *  Created on: 11 May 2020
 *      Author: Ewan Rannala
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include "Config_File/ConfigFile_import.hpp"
#include "Structures.hpp"
#include <string>

enum struct Solvers {kMC,LLB,LLG};
enum struct Behaviours {Standard, Thermoremanence};

class Solver_t {
public:
    // The constructor will import all required values for the solver specified in the configuration file.
    Solver_t(unsigned int Num_Layers,ConfigFile cfg, const std::vector<ConfigFile> Materials_config);
    Solver_t(ConfigFile cfg);
    ~Solver_t();

    void Import(ConfigFile cfg);
    void Fittings_import(const unsigned int Num_Layers, std::vector<ConfigFile> Materials_config);
    void Force_specific_solver_construction(unsigned int Num_Layers,ConfigFile cfg, const std::vector<ConfigFile> Materials_config, Solvers desired);

    int Integrate(Voronoi_t*VORO, unsigned int Num_layers, Interaction_t*Int_sys,double Cx, double Cy, Grain_t*Grain);

    void set_dt(double timeStep, Solvers desired_solver);
    void set_dt(double timeStep);
    double get_dt(Solvers desired) const;
    double get_dt() const;
    void set_f0(double F0);
    double get_f0() const;
    void set_Solver(Solvers new_solver);
    Solvers get_Solver() const;
    void set_allow_Tc(bool option, Solvers desired_solver);
    void set_ml_output(bool option);
    std::vector<double> get_Alpha() const;
    double get_Gamma() const;
    double get_mEQ(unsigned int idx) const;
    double get_ChiPerp(unsigned int idx) const;
    bool get_Exclusion() const;

    void setMeasTime(double Time, Solvers desired_solver);
    double getMeasTime() const;
    double getMeasTime(Solvers Desired) const;
    unsigned int getOutputSteps() const;

    void disableExclusion();

private:

    int KMC(const unsigned int Num_Layers,const Interaction_t*Interac,
                   const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain);

    int LLG(const unsigned int Num_Layers,const Interaction_t*Interac,
            const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain);

    int LLB(const unsigned int Num_Layers,const Interaction_t*Interac,
             const Voronoi_t*VORO,const double Centre_x, const double Centre_y,Grain_t*Grain);

    std::vector<Vec3> LLB_STEP(unsigned int Num_Grains, unsigned int Num_Layers,unsigned int Integratable_grains_in_layer,
                               std::vector<unsigned int>*Included_grains_in_system,std::vector<Vec3>*RNG_PARA_NUM,
                               std::vector<Vec3>*RNG_PERP_NUM,const Interaction_t*Interac,Grain_t*Grain);

    int Susceptibilities(const unsigned int Num_Grains, const unsigned int Num_Layers,
                         const std::vector<unsigned int>*Included_grains_in_system,
                         const Grain_t*Grain);

    int KMC_core(const Vec3 Easy_axis,const Vec3 h_eff,
                 const double KV_over_kBT,const double MsVHk_over_kBT,Vec3*Spin);



    Solvers type;
    Behaviours behaviour_kMC;
    Behaviours behaviour_LLG;

    // Solver flags
    bool T_variation;            //!< Flag to determine kMC and LLG behaviour when T>=T_c
    bool M_length_variation;    //!< Flag to set m length variation in kMC
    bool Exclusion;             //!< Flag to set exclusion zone use

    // kMC only parameters
    double f0;                    //!< Attempt frequency (Hz)

    // kMC,LLG,LLB parameters
    double dt_kMC;                                    //!< Time step for kMC solver (s)
    double dt_LLG;                                    //!< Time step for LLG solver (s)
    double dt_LLB;                                    //!< Time step for LLB solver (s)
    double Exclusion_range_negX;                    //!< Exclusion zone range in negative x-axis
    double Exclusion_range_posX;                    //!< Exclusion zone range in positive x-axis
    double Exclusion_range_negY;                    //!< Exclusion zone range in negative y-axis
    double Exclusion_range_posY;                    //!< Exclusion zone range in positive y-axis
    double Measurement_time_kMC;                    //!< Time at which data should be output when using kMC solver
    double Measurement_time_LLG;                    //!< Time at which data should be output when using LLG solver
    double Measurement_time_LLB;                    //!< Time at which data should be output when using LLB solver


    // LLG/LLB parameters
    std::vector<double> Alpha;                        //!< Damping constant
    const double Gamma;                    //!< Gyro-magnetic ratio (Oe<SUP>-1</SUP>s<SUP-1</SUP>)

    // LLB only parameters
    std::vector<double> Chi_para;                    //!< Parallel susceptibility
    std::vector<double> Chi_perp;                    //!< Perpendicular susceptibility
    std::vector<double> Alpha_PARA;                    //!< Parallel damping constant
    std::vector<double> Alpha_PERP;                    //!< Perpendicular damping constant
    std::vector<double> m_EQ;                        //!< Equilibrium magnetisation

    std::vector<std::string> Susceptibility_Type;    //!< Method used for susceptibility modelling
    // Parallel susceptibility parameters
    std::vector<double> a0_PARA,a1_PARA,a2_PARA,
                        a3_PARA,a4_PARA,
                        a5_PARA,a6_PARA,a7_PARA,
                        a8_PARA,a9_PARA,a10_PARA,
                        a11_PARA,a1_2_PARA,b0_PARA,
                        b1_PARA,b2_PARA,b3_PARA,
                        b4_PARA;
    // Perpendicular susceptibility parameters
    std::vector<double> a0_PERP,a1_PERP,a2_PERP,
                        a3_PERP,a4_PERP,a5_PERP,
                        a6_PERP,a7_PERP,a8_PERP,
                        a9_PERP,a10_PERP,a11_PERP,
                        a1_2_PERP,b0_PERP,
                        b1_PERP,b2_PERP,b3_PERP,
                        b4_PERP;
    // Scaling factor for the susceptibility (parallel and perpendicular) to be applied to the input values in case the susceptibility is units of magnetisation/field (Am^-1 / T)
    std::vector<double> Chi_scaling_factor;            //!< Scailing factor for susceptibility (applied when susceptibility is in units of Am<SUP>-1</SUP>$frasl;T)
    std::string unit_susceptibility;

    // Terminal formating
    const std::string BoldCyanFont;
    const std::string BoldYellowFont;
    const std::string BoldRedFont;
    const std::string ResetFont;


};

#endif /* SOLVER_H_ */
