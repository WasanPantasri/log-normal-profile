/*
 * Pixel.hpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

#ifndef M_A_R_S__SRC_PIXEL_HPP_
#define M_A_R_S__SRC_PIXEL_HPP_

#include "Structures.hpp"
#include <vector>

class Pixel {
    private:
    // Pixel information
    unsigned int ID;
    std::pair<double,double> centre;
    // Pixel data
    int Grain;
    double Temperature;
    Vec3 Magnetisation,Field,Easy_Axis;

public:
    Pixel (unsigned int,double,double);
    void set_M(Vec3);
    void set_M(double M);
    void set_EA(Vec3 EA);
    void set_EA(double EA);
    void set_Temp(double);
    void set_H(Vec3);
    void set_Grain(unsigned int);
    unsigned int  get_ID() const ;
    int  get_Grain() const ;
    Vec3 get_M() const ;
    Vec3 get_EA() const ;
    double get_Mx() const ;
    double get_My() const ;
    double get_Mz() const ;
    double get_Temp() const ;
    Vec3 get_H() const ;
    std::pair<double,double> get_Coords() const ;
};

#endif /* M_A_R_S__SRC_PIXEL_HPP_ */
