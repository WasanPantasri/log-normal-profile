/*
 * HAMR_grain_output.hpp
 *
 *  Created on: 5 Oct 2018
 *      Author: ewan
 */

/** \file HAMR_grain_output.hpp
 * \brief Header file for HAMR output functions related to individual grain data. */

#ifndef HAMR_GRAIN_OUTPUT_HPP_
#define HAMR_GRAIN_OUTPUT_HPP_

#include <string>
#include "../../hdr/Structures.hpp"

extern int Idv_Grain_output(const int, const Voronoi_t, const Grain_t, const double,std::string,const std::string="");

#endif /* HAMR_GRAIN_OUTPUT_HPP_ */
