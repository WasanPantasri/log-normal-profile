/*
 * Export_system.hpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Export_system.hpp
 * \brief Header file for system export functions. */

#ifndef EXPORT_SYSTEM_HPP_
#define EXPORT_SYSTEM_HPP_

#include "../../hdr/Structures.hpp"

extern int Export_system(const int, const std::vector<double>, const Voronoi_t,
		          const Interaction_t, const Grain_t, const std::string);

#endif /* EXPORT_SYSTEM_HPP_ */
