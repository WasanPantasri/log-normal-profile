/* Convert.hpp
 *  Created on: 28 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Convert.hpp
 * \brief Header file for the Convert class. */

#ifndef CONVERT_HPP_
#define CONVERT_HPP_

#include <string>
#include <vector>

/** \brief Class for converting std::strings into other types.
 *
 * This class converts the std::string value obtained from the ConfigFile hash table into the
 * requested data type. It is also possible to convert primitive tyes into std::strings */

class Convert
{
private:
	static const std::string BoldRedFont;// = "\033[1;4;31m";
	static const std::string ResetFont;// = "\033[0m";
public:
	template <typename T>
	static std::string T_to_string(const std::string FileName, T const &val, std::string const &key);

	template <typename T>
	static T string_to_T(const std::string FileName, std::string const &val, std::string const &key);

};

template <>
std::string Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key);

template <>
std::vector<std::string> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key);

template<>
bool Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key);

template <>
std::vector<double> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key);

template <>
std::vector<int> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key);

#endif /* CONVERT_HPP_ */
