/* Test_LLG_timestep.hpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Test_LLG_timestep.hpp
 * \brief Header file for LLG timestep test. */

#ifndef TEST_LLG_TIMESTEP_HPP_
#define TEST_LLG_TIMESTEP_HPP_

extern int LLG_timestep_test();

#endif /* TEST_LLG_TIMESTEP_HPP_ */
