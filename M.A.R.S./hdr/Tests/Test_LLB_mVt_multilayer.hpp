/* Test_LLB_mVt_multilayer.hpp
 *  Created on: 2 Feb 2020
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

#ifndef TEST_LLB_MVT_MULTILAYER_HPP_
#define TEST_LLB_MVT_MULTILAYER_HPP_

#include "../../hdr/Structures.hpp"

extern double DotP(const Vec3, const Vec3);

extern int LLB_mVt_multilayer_test();

#endif /* TEST_LLB_MVT_MULTILAYER_HPP_ */
