/* Test_LLB_timestep.hpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Test_LLB_timestep.hpp
 * \brief Header file for LLB timestep test. */

#ifndef TEST_LLB_TIMESTEP_HPP_
#define TEST_LLB_TIMESTEP_HPP_

extern int LLB_timestep_test();

#endif /* TEST_LLB_TIMESTEP_HPP_ */
