/*
 * Test_LLB_transverse_relaxation.hpp
 *
 *  Created on: 11 Jun 2019
 *      Author: Ewan Rannala
 */

/** \file Test_LLB_transverse_relaxation.hpp
 * \brief Header file for LLB transverse relaxation test. */

#ifndef LLB_TRANSVERSE_RELAXATION_HPP_
#define LLB_TRANSVERSE_RELAXATION_HPP_

extern int LLB_trans();

#endif /* LLB_TRANSVERSE_RELAXATION_HPP_ */
