/* Test_analytical.hpp
 *  Created on: 22 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Test_analytical.hpp
 * \brief Header file for LLB/LLG analytical test. */

#ifndef TEST_ANALYTICAL_HPP_
#define TEST_ANALYTICAL_HPP_

#include "../Config_File/ConfigFile_import.hpp"

extern int analytic_test();

#endif /* TEST_ANALYTICAL_HPP_ */
