/* Test_hyst.hpp
 *  Created on: 10 Dec 2019
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

#ifndef TEST_HYST_HPP_
#define TEST_HYST_HPP_

#include "../../hdr/Structures.hpp"

extern int Test_hyst();

#endif /* TEST_HYST_HPP_ */
