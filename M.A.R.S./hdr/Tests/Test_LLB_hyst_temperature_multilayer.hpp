/* Test_LLB_hyst_multilayer.hpp
 *  Created on: 10 Dec 2019
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

#ifndef TEST_LLB_HYST_TEMPERATURE_MULTILAYER_HPP_
#define TEST_LLB_HYST_TEMPERATURE_MULTILAYER_HPP_

#include "../../hdr/Structures.hpp"

extern double DotP(const Vec3, const Vec3);

extern int LLB_hyst_temperature_multilayer_test();

#endif /* TEST_LLB_HYST_TEMPERATURE_MULTILAYER_HPP_ */
