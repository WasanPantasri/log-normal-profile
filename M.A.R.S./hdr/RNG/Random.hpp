/*
 * Random.hpp
 *
 *  Created on: 2 Mar 2018
 *      Author: ewan
 */

/** \file Random.hpp
 *  \brief Header file for mtrandom namespace. */

#pragma once
#ifndef RANDOM_HPP_
#define RANDOM_HPP_

#include "MTRand.h"

namespace mtrandom{
	extern MTRand grnd; // Single sequence of random numbers
	extern double gaussian();
	extern int voronoi_seed;
	extern int integration_seed;
}

#endif /* RANDOM_HPP_ */
