/* Structures.hpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Structures.hpp
 * \brief Header file for data structs. */

#ifndef STRUCTURES_HPP_
#define STRUCTURES_HPP_

#include <vector>
#include <string>
#include <iostream>

extern double PI, KB;

/** \brief Class for a 3-vector. */
class Vec3{

public:

    Vec3();
    Vec3(double x,double y,double z);
    Vec3(const Vec3& vector);
    ~Vec3();

    double x,y,z;

	// Operators
	friend std::ostream& operator<<(std::ostream& os, const Vec3& vec);
	// Mathematical operations

	friend Vec3 operator+(Vec3 const &lhs, Vec3 const &rhs);
	void operator+=(Vec3 const &rhs){
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
	}

	friend Vec3 operator-(Vec3 const &rhs){
	    Vec3 res(-rhs.x,-rhs.y,-rhs.z);
	    return res;
	}
	friend Vec3 operator-(Vec3 const &lhs, Vec3 const &rhs);
	void operator-=(Vec3 const &rhs){
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;
	}

	friend double operator*(Vec3 const &lhs, Vec3 const &rhs); // Dot product
	friend Vec3 operator*(Vec3 const &lhs,double const &rhs);
	friend Vec3 operator*(double const &lhs,Vec3 const &rhs);
	void operator*=(double const &rhs){
		x*=rhs;
		y*=rhs;
		z*=rhs;
	}

	friend Vec3 operator%(Vec3 const &lhs, Vec3 const &rhs); // Cross product

	friend Vec3 operator/(Vec3 const &lhs,double const &rhs);
	void operator/=(double const &rhs){
		x/=rhs;
		y/=rhs;
		z/=rhs;
	}

	void operator=(double const &rhs){
		x=y=z=rhs;
	}
};

/** \brief Structure for grain information.
 *
 * <P> This structure contains the information of every grain in a system </P> */
struct Grain_t {

	std::vector<std::string> mEQ_Type;				//!< Model used for equilibrium magnetisation
	std::vector<std::string> Callen_power_range; //!< Scheme used for Callen-Callen
	std::vector<Vec3> m;						 //!< Magnetisation 3-vector [emu&frasl;cc]
	std::vector<Vec3> Easy_axis;				 //!< Easy axis 3-vector
	std::vector<double> Temp;					 //!< Temperature [K]
	std::vector<double> diameter;				 //!< Diameter [nm]
	std::vector<double> Vol;					 //!< Volume [nm<SUP>2</SUP>]
	std::vector<double> K;						 //!< Anisotropy [erg&frasl;cc]
	std::vector<double> Tc;						 //!< Curie Temperature	[K]
	std::vector<double> Ms;						 //!< Saturation magnetisation [emu&frasl;cc]
	std::vector<std::string> Ani_method;				 //!< Method used to model the grain's anisotropy
	std::vector<double> Callen_power;			 //!< Callen-Callen exponent
	std::vector<double> Callen_factor_lowT;		 //!< Callen-Callen pre-factor for low range temperatures
	std::vector<double> Callen_factor_midT;		 //!< Callen-Callen pre-factor for mid range temperatures
	std::vector<double> Callen_factor_highT;	 //!< Callen-Callen pre-factor for high range temperatures
	std::vector<double> Callen_power_lowT;		 //!< Callen-Callen exponent for low range temperatures
	std::vector<double> Callen_power_midT;		 //!< Callen-Callen exponent for mid range temperatures
	std::vector<double> Callen_power_highT;		 //!< Callen-Callen exponent for high range temperatures
	std::vector<double> Callen_range_lowT;		 //!< Callen-Callen low temperature range definition
	std::vector<double> Callen_range_midT;		 //!< Callen-Callen mid temperature range definition
	std::vector<double> Crit_exp;				 //!< Critical exponent for M(T) behaviour if Heisenberg bulk-like behaviour is assumed
	std::vector<double> a0_mEQ,a1_mEQ,a2_mEQ,  	//!< Critical exponents for M(T) behaviour if polynomial expression is used
    						a3_mEQ,a4_mEQ,
							a5_mEQ,a6_mEQ,a7_mEQ,
							a8_mEQ,a9_mEQ,a1_2_mEQ,
							b0_mEQ,b1_mEQ,b2_mEQ,
							b3_mEQ,b4_mEQ;
	std::vector<Vec3> H_appl;					 //!< Applied field 3-vector [Oe]
	std::vector<std::vector<unsigned int>> Pixel; //!< List of cells within the grain

};

/** \brief Structure for material/layer information
 *
 * <P> This Structure contains the information for a material/layer in the system </P> */
struct Material_t {

	std::vector<std::string> Mag_Type;				//!< Type of initial magnetisation (Random or assigned)
	std::vector<Vec3> Initial_mag;					//!< Initial magnetisation for all grains within the layer
	std::vector<Vec3> Initial_anis;					//!< Initial easy axis for all grains within the layer
	std::vector<std::string> Type;					//!< Material type (Currently unused)
	std::vector<std::string> Tc_Dist_Type;			//!< Distribution type for Curie temperature
	std::vector<std::string> K_Dist_Type;			//!< Distribution type for anisotropy
	std::vector<std::string> J_Dist_Type;			//!< Distribution type for exchange interactions
	std::vector<std::string> mEQ_Type;				//!< Model used for equilibrium magnetisation
	std::vector<std::string> Susceptibility_Type;	//!< Model used for susceptibility
	std::vector<std::string> Callen_power_range;	//!< Scheme used for Callen-Callen
	std::vector<std::string> Ani_method;			//!< Method used to model material's anisotropy
	std::vector<double> Ms;							//!< Saturation magnetisation [emu&frasl;cc]
	std::vector<double> Tc_inf;						//!< Maximum value of Curie temperature [K]
	std::vector<double> Avg_Tc;						//!< Average Curie temperature [K]
	std::vector<double> StdDev_Tc;					//!< Standard deviation of Curie temperature
	std::vector<double> Avg_K;						//!< Average anisotropy [erg&frasl;cc]
	std::vector<double> StdDev_K;					//!< Standard deviation of anisotropy
	std::vector<double> StdDev_J;					//!< standard deviation of exchange strength
	std::vector<double> z;							//!< Vertical position of layer's centre [nm]
	std::vector<double> dz;							//!< Thickness of layer [nm]
	std::vector<double> Anis_angle;					//!< Anisotropy angle of dispersion [deg]
	std::vector<double> H_sat;						//!< Exchange field saturation [Oe]
	std::vector<double> Easy_axis_polar;			//!< Easy axis polar angle [deg]
	std::vector<double> Easy_axis_azimuth;			//!< Easy axis azimuthal angle [deg]
	std::vector<double> Callen_power;				//!< Callen-callen exponent
	std::vector<double> Callen_factor_lowT;			//!< Callen-Callen pre-factor for low range temperatures
	std::vector<double> Callen_factor_midT;			//!< Callen-Callen pre-factor for mid range temperatures
	std::vector<double> Callen_factor_highT;		//!< Callen-Callen pre-factor for high range temperatures
	std::vector<double> Callen_power_lowT;			//!< Callen-Callen exponent for low range temperatures
	std::vector<double> Callen_power_midT;			//!< Callen-Callen exponent for mid range temperatures
	std::vector<double> Callen_power_highT;			//!< Callen-Callen exponent for high range temperatures
	std::vector<double> Callen_range_lowT;			//!< Callen-Callen low temperature range definition
	std::vector<double> Callen_range_midT;			//!< Callen-Callen mid temperature range definition
	std::vector<double> Crit_exp;					//!< Critical exponent for M(T) behaviour if Heisenberg bulk-like behaviour is assumed
	std::vector<double> a0_mEQ,a1_mEQ,a2_mEQ,  	//!< Critical exponents for M(T) behaviour if polynomial expression is used
    						a3_mEQ,a4_mEQ,
							a5_mEQ,a6_mEQ,a7_mEQ,
							a8_mEQ,a9_mEQ,a1_2_mEQ,
							b0_mEQ,b1_mEQ,b2_mEQ,
							b3_mEQ,b4_mEQ;
	// Out of plane exchange
	std::vector<double> Hexch_str_out_plane_UP;		//!< Exchange field strength exerted on layers above [Oe]
	std::vector<double> Hexch_str_out_plane_DOWN;	//!< Exchange field strength exerted on layers below [Oe]

};

/** \brief Structure for system structure information
 *
 * <P> This structure contains information required to generate the system's structure </P> */
struct Structure_t {

	double Dim_x;							//!< System x-dimension [nm]
	double Dim_y;							//!< system y-dimension [nm]
	double Grain_width;						//!< Average grain width [nm]
	double Packing_fraction;				//!< Fraction of system area filled by grains
	double StdDev_grain_pos;				//!< Standard deviation in grain position
	double Magneto_Interaction_Radius;		//!< Interaction radius for magnetostatics [nm]
	unsigned int Num_layers;				//!< Number of layers within the system
	std::string Magnetostatics_gen_type;	//!< Method for magnetostatics generation [import OR dipole]

};

/** \brief Structure for voronoi construction data
 *
 * <P> This structure contains all information used in the Voronoi construction </P> */
struct Voronoi_t {

	unsigned int Num_Grains;											//!< Number of grains per layer
	double x_max;														//!< Maximum allowed grain centre in x-axis [nm]
	double y_max;														//!< Maximum allowed grain centre in y-axis [nm]
	double Int_Rad;														//!< Interaction radius for exchange [nm]
	double Centre_X;													//!< System centre in x-direction [nm]
	double Centre_Y;													//!< System centre on y-direction [nm]
	double Average_area=0.0;											//!< Average grain area [nm<SUP>2</SUP>]
	double Average_contact_length=0.0;									//!< Average grain contact length [nm]
	double Vx_MIN=0.0;													//!< Minimum x-position held by a vertex [nm]
	double Vx_MAX=0.0;													//!< Maximum x-position held by a vertex [nm]
	double Vy_MIN=0.0;													//!< Minimum y-position held by a vertex [nm]
	double Vy_MAX=0.0;													//!< Maximum y-position held by a vertex [nm]
	std::vector<double> Pos_X_final;									//!< Grain positions in x-axis [nm]
	std::vector<double> Pos_Y_final;									//!< Grain positions in y-axis [nm]
	std::vector<double> Geo_grain_centre_X;								//!< Grain geometrical centre in x-axis [nm]
	std::vector<double> Geo_grain_centre_Y;								//!< Grain geometrical centre in y-axis [nm]
	std::vector<double> Grain_diameter;									//!< Grain diameters [nm]
	std::vector<double> Grain_Area;										//!< Grain areas [nm<SUP>2</SUP>]
	std::vector<std::vector<double>> Contact_lengths;					//!< Grain contact lengths [nm]
	std::vector<std::vector<double>> Vertex_X_final;					//!< Grain vertices in x-axis [nm]
	std::vector<std::vector<double>> Vertex_Y_final;					//!< Grain vertices in y-axis [nm]
	std::vector<std::vector<unsigned int>> Neighbour_final;				//!< Grain nearest neighbour list
	std::vector<std::vector<unsigned int>> Magnetostatic_neighbours;	//!< Grain magnetostatic neighbour list

};

/** \brief Structure for Landau-Lifshitz-Gilbert
 *
 * <P> This structure contains all parameters required by LLG solver </P> */
struct LLG_t {

	double Gamma=1.760859644e+7;	//!< Gyro-magnetic ratio for LLG
	double dt;						//!< Time step for LLG
	std::vector<double> Alpha;		//!< Damping constant for LLG

};

/** \brief Structure for Landau-Lifshitz-Bloch solver
 *
 * <P> This structure contains all parameters required by the LLB solver </P> */
struct LLB_t {

//	std::string Ani_method;							//!< Flag to set method for anisotropy determination
	double Gamma=1.760859644e+7;					//!< Gyro-magnetic ratio for LLB
	double dt;										//!< Time step for LLB
	// Size based on number of layers
	std::vector<double> Alpha;						//!< Damping constant for LLB
	std::vector<double> Chi_para;					//!< Parallel susceptibility
	std::vector<double> Chi_perp;					//!< Perpendicular susceptibility
	std::vector<double> Alpha_PARA;					//!< Parallel damping constant
	std::vector<double> Alpha_PERP;					//!< Perpendicular damping constant
	std::vector<double> m_EQ;						//!< Equilibrium magnetisation

	std::vector<std::string> Susceptibility_Type;	//!< Method used for susceptibility modelling
    // Parallel susceptibility parameters
    std::vector<double> a0_PARA,a1_PARA,a2_PARA,
    					a3_PARA,a4_PARA,
    					a5_PARA,a6_PARA,a7_PARA,
    					a8_PARA,a9_PARA,a10_PARA,
    					a11_PARA,a1_2_PARA,b0_PARA,
    					b1_PARA,b2_PARA,b3_PARA,
    					b4_PARA;
    // Perpendicular susceptibility parameters
    std::vector<double> a0_PERP,a1_PERP,a2_PERP,
    					a3_PERP,a4_PERP,a5_PERP,
    					a6_PERP,a7_PERP,a8_PERP,
    					a9_PERP,a10_PERP,a11_PERP,
    					a1_2_PERP,b0_PERP,
    					b1_PERP,b2_PERP,b3_PERP,
    					b4_PERP;
    // Scaling factor for the susceptibility (parallel and perpendicular) to be applied to the input values in case the susceptibility is units of magnetisation/field (Am^-1 / T)
    std::vector<double> Chi_scaling_factor;			//!< Scailing factor for susceptibility (applied when susceptibility is in untis of Am<SUP>-1</SUP>$frasl;T)
    bool Exclusion_zone;							//!< Flag indicating if exclusion zone is to be used
    double Exclusion_range_negX;					//!< Exclusion zone range in negative x-axis
    double Exclusion_range_posX;					//!< Exclusion zone range in positive x-axis
    double Exclusion_range_negY;					//!< Exclusion zone range in negative y-axis
    double Exclusion_range_posY;					//!< Exclusion zone range in positive y-axis
};

/** \brief Structure for Kinetic Monte Carlo solver
 *
 * <P> This structure contains the parameters required by the kMC solver </P> */
struct KMC_t {

	double time_step;	//!< Time step for kMC solver
	bool ZeroKInputs;	//!< Flag to indicate if material parameters are zero-Kelvin values or not. If <B>True</B> then thermal dependencies are applied.
};

/** \brief Structure for interaction data
 *
 * <P> This structure contains all data required for interactions </P> */
struct Interaction_t {

	std::vector<std::vector<unsigned int>> Magneto_neigh_list;	//!< Magnetostatic neighbour lists
	std::vector<std::vector<unsigned int>> Exchange_neigh_list;	//!< Exchange interaction neighbour list
	std::vector<std::vector<double>> Wxx;						//!< W-matrix xx element
	std::vector<std::vector<double>> Wxy;						//!< W-matrix xy element
	std::vector<std::vector<double>> Wxz;						//!< W-matrix xz element
	std::vector<std::vector<double>> Wyy;						//!< W-matrix yy element
	std::vector<std::vector<double>> Wyz;						//!< W-matrix yz element
	std::vector<std::vector<double>> Wzz;						//!< W-matrix zz element
	std::vector<std::vector<double>> H_exch_str;				//!< Exchange field strength [Oe]

};

/** \brief Structure for various key timings
 *
 * <P> This structure contains the various available assignable timings </P> */
struct Expt_timings_t {

	double Equilibration_time;		//!< Time for initial system equilibration [s]
	double Run_time;				//!< Total simulation run time [s]
	double Meas_time;				//!< Time interval at which data is recorded [s]
	double Initialisation_time;		//!< Time to simulate until initial laser application [s]
	double Application_time;		//!< Time over which laser pulse is applied [s]
	double runoff_time;				//!< Time added to end of simulation to allow system to relax [s]
	double Cooling_rate;			//!< Cooling rate [K/s]
};

/** \brief Structure for applied field experimental parameters
 *
 * <P> This structure contains the various parameters required to configure the applied field </P> */
struct Expt_H_app_t {

	double H_appl_MAG_min;			//!< Minimum applied field strength [Oe]
	double H_appl_MAG_max;			//!< Maximum applied field strength [Oe]
	double H_appl_MAG;              //!< Current applied field strength [Oe]
	double Field_ramp_time;			//!< Time required for field to switch from Max to Min [s]
	double Field_width_X;			//!< Width of applied field in x-axis [nm]
	double Field_width_Y;			//!< Width of applied field in y-axis [nm]
	Vec3 H_appl_unit;				//!< Applied field unit vector

};

// TODO move to separate source and header files

#include <cmath>
#include "Config_File/ConfigFile_import.hpp"
#include <fstream>

class ExpHac {

protected:

    // Instantaneous values
    Vec3 field;       //!< Field vector
    double mag;       //!< Field magnitude

    // Descriptive values
    Vec3 dirn;         //!< Direction vector
    double amp;        //!< Amplitude (Oe)
    double freq;       //!< Frequency (Hz)
    double phaseShift; //!< Phase shift in radians

public:

    ExpHac(const ConfigFile cfg){
        std::vector<double> dirn_tmp = cfg.getValueOfKey<std::vector<double>>("FieldAC:Unit_vector");
        double tmpMag = sqrt(dirn_tmp[0]*dirn_tmp[0]+dirn_tmp[1]*dirn_tmp[1]+dirn_tmp[2]*dirn_tmp[2]);
        dirn.x = dirn_tmp[0]/tmpMag;
        dirn.y = dirn_tmp[1]/tmpMag;
        dirn.z = dirn_tmp[2]/tmpMag;
        amp  = cfg.getValueOfKey<double>("FieldAC:Amplitude");
        freq = cfg.getValueOfKey<double>("FieldAC:Frequency");
        phaseShift = (PI/180.0)*cfg.getValueOfKey<double>("FieldAC:Phase");

        updateField(0.0);
    }
    ~ExpHac(){}

    void updateField(double time){
        updateMag(time);
        field=dirn*mag;
    }
    void updateMag(double time){
        mag = amp*sin(2.0*PI*freq*time+phaseShift);
    }

    void setDirn(Vec3 Direction){
        Direction = Direction/sqrt(Direction*Direction);
        dirn = Direction;
    }

    Vec3 getField() const {return field;}
    double getMag() const {return mag;}
    Vec3 getDirn() const {return dirn;}
    double getFreq() const {return freq;}
    double getPhase() const {return phaseShift;}
    double getAmp() const {return amp;}


};

class ExpHdc {

protected:

    enum struct conditions{min,increasing,max,decreasing};

    // Instantaneous values
    Vec3 field;       //!< Field vector
    double mag;       //!< Field magnitude
    conditions state;
    bool On;

    // Descriptive values
    Vec3 dirn;          //!< Direction vector
    double min;         //!< Minimum strength (Oe)
    double max;         //!< Maximum strength (Oe)
    double ramp_time;    //!< Time required to switch between min/max (s)
    double rate;        //!< Rate of change of field during change between min/max (K/s)

public:

    ExpHdc(const ConfigFile cfg)
    : state(conditions::min)
    , On(false)
{
        std::vector<double> dirn_tmp = cfg.getValueOfKey<std::vector<double>>("FieldDC:Unit_vector");
        double tmpMag = sqrt(dirn_tmp[0]*dirn_tmp[0]+dirn_tmp[1]*dirn_tmp[1]+dirn_tmp[2]*dirn_tmp[2]);
        dirn.x = dirn_tmp[0]/tmpMag;
        dirn.y = dirn_tmp[1]/tmpMag;
        dirn.z = dirn_tmp[2]/tmpMag;

        min  = cfg.getValueOfKey<double>("FieldDC:Minimum_strength");
        max  = cfg.getValueOfKey<double>("FieldDC:Maximum_strength");
        if(min!=max){
            ramp_time = cfg.getValueOfKey<double>("FieldDC:Ramp_time");
            if(ramp_time==0){rate=0.0;}
            else{rate = (max-min)/ramp_time;}
        } else {
            ramp_time = 0.0;
            rate = 0.0;
        }

        updateField(0.0);
    }
    ~ExpHdc(){}

    void updateField(double dt){
        updateMag(dt);
        field=dirn*mag;
    }

    void turnOn(){
        On=true;
    }

    void turnOff(){
        On=false;
    }

    void updateState(){
        switch(state)
        {
        case conditions::min:
            if(On){state = conditions::increasing;}
            break;
        case conditions::increasing:
            if(mag>=max){state = conditions::max;}
            break;
        case conditions::max:
            if(!On){state = conditions::decreasing;}
            break;
        case conditions::decreasing:
            if(mag<=min){state = conditions::min;}
            break;
        }
    }

    void updateMag(double dt){
        switch(state)
        {
        case conditions::min:
            mag=min;
            break;
        case conditions::max:
            mag=max;
            break;
        case conditions::increasing:
            if(rate==0){mag=max;}
            else{mag += dt*rate;}
            break;
        case conditions::decreasing:
            if(rate==0){mag=min;}
            else{mag -= dt*rate;}
            break;
        }
        updateState();
    }

    void setDirn(Vec3 Direction){
        if(Direction.x==0.0 && Direction.y==0.0 && Direction.z==0.0){
        } else {
            Direction = Direction/sqrt(Direction*Direction);
        }
        dirn = Direction;
    }

    Vec3 getField() const {return field;}
    double getMag() const {return mag;}
    conditions getState() const {return state;}
    Vec3 getDirn() const {return dirn;}
    double getMin() const {return min;}
    double getMax() const {return max;}
    double getRampTime() const {return ramp_time;}
    double getRate() const {return rate;}

};

class Writer : public ExpHdc {

protected:
    double H_width_X;     //!< Width of applied field in x-axis [nm]
    double H_width_Y;     //!< Width of applied field in y-axis [nm]
    double Pos_X;         //!< x-coordinate for head centre [nm]
    double Pos_Y;         //!< y-coordinate for head centre [nm]
    double speed;         //!< Speed of head in x-direction [nm/s]
    double Skew;          //!< Rotation angle (skew) of write head [degree]

public:

    Writer(const ConfigFile cfg)
    : ExpHdc(cfg)
    , Pos_X(0.0)
    , Pos_Y(0.0)
{
        H_width_X = cfg.getValueOfKey<double>("WriteHead:Field_Width_X");
        H_width_Y = cfg.getValueOfKey<double>("WriteHead:Field_Width_Y");
        speed     = cfg.getValueOfKey<double>("WriteHead:Speed");
        Skew      = cfg.getValueOfKey<double>("WriteHead:Skew");

    }
    ~Writer(){}

    void setX(double x){Pos_X=x;}
    void setY(double y){Pos_Y=y;}
    void moveX(double dx){Pos_X += dx;}
    void moveY(double dy){Pos_Y += dy;}

    void ApplyHspatial(const Voronoi_t VORO, const unsigned int Num_Layers, const double dt, Grain_t*Grains){

        updateField(dt);

        const double skew_angle = getSkew()*PI/180.0; // convert to radians

        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
            double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];
            // Apply rotation due to Skew angle to field box
            double X_rotated =  (Pos_X-Grain_X)*cos( skew_angle) - (Pos_Y-Grain_Y)*sin( skew_angle);
            double Y_rotated =  (Pos_X-Grain_X)*sin( skew_angle) + (Pos_Y-Grain_Y)*cos( skew_angle);
            if( (X_rotated<=(H_width_X*0.5) && X_rotated>=-(H_width_X*0.5))&&
            	(Y_rotated<=(H_width_Y*0.5) && Y_rotated>=-(H_width_Y*0.5))  ){
                for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                    unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                    Grains->H_appl[grain_in_system] = getField();
                }
            }
            else{
                // Reset all the other grains not in range of writing head to zero applied field
                for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                    unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                    Grains->H_appl[grain_in_system] = Vec3(0.0,0.0,0.0);
                }
            }
        }
    }

    // TODO develop method to determine <H> in grain using pixelmap
/* WIP
    void ApplyHspatialPM(const Voronoi_t VORO, const unsigned int Num_Layers, const double dt,
                         const Grain_t&Grains,PixelMap&PixMap){

        updateField(dt);
        // Remove Field from all pixels
        PixMap.ClearH();
        std::vector<unsigned int> PixelList = PixMap.InRect((Pos_X-H_width_X*0.5), (Pos_Y-H_width_Y*0.5), (Pos_X+H_width_X*0.5), (Pos_Y+H_width_Y*0.5));
        // Apply field to all pixels
        for(unsigned int Pix=0;Pix<PixelList.size();++Pix){
            PixMap.Access(PixelList[Pix]).set_H(getField());
        }

        // Now determine effective field for each grain
        ?? Need some method to determine which grains to update otherwise this will be very very slow

    }
*/
    double getX() const {return Pos_X;}
    double getY() const {return Pos_Y;}
    double getHwidthX() const {return H_width_X;}
    double getHwidthY() const {return H_width_Y;}
    double getSpeed() const {return speed;}
    double getSkew() const {return Skew;}

};

class HAMR_Laser {

protected:

    double Temp_Max;
    double Gradient;
    double Cooling_time;

public:

    HAMR_Laser(const ConfigFile cfg){
        Temp_Max = cfg.getValueOfKey<double>("LaserHAMR:Temperature");
        Gradient = cfg.getValueOfKey<double>("LaserHAMR:Thermal_Gradient");
        Cooling_time = cfg.getValueOfKey<double>("LaserHAMR:Cooling_time");
    }
    ~HAMR_Laser(){}

    void ApplyT(const double Environ_Temp, const double Time, double*Temperature, Grain_t*Grains){

        *Temperature = Environ_Temp + (getTemp()-Environ_Temp)*exp( -pow( ((Time-3*getCoolingTime())/getCoolingTime()),2.0 ) );
        for(size_t grain_num=0;grain_num<Grains->Temp.size();grain_num++){Grains->Temp[grain_num] = *Temperature;}

    }
    double getTemp(){return Temp_Max;}
    double getGrad(){return Gradient;}
    double getCoolingTime(){return Cooling_time;}
    double getProfileTime(){return 6.0*Cooling_time;}

};

class RecHead : public Writer, public HAMR_Laser {

protected:

    bool Pulsing;
    double FWHM_X;        //!< Full width half maximum of laser profile in x-dir
    double FWHM_Y;        //!< Full width half maximum of laser profile in y-dir;
    double NFT_disp_X;    //!< Spacing between Laser (NFT) and write head in x-dir
    double NFT_disp_Y;    //!< Spacing between Laser (NFT) and write head in y-dir

public:

    RecHead(const ConfigFile cfg)
    : Writer(cfg)
    , HAMR_Laser(cfg)
{
        FWHM_X     = cfg.getValueOfKey<double>("Recordinghead:FWHM_X");
        FWHM_Y     = cfg.getValueOfKey<double>("Recordinghead:FWHM_Y");
        NFT_disp_X = cfg.getValueOfKey<double>("Recordinghead:NFT_spacing_X");
        NFT_disp_Y = cfg.getValueOfKey<double>("Recordinghead:NFT_spacing_Y");
        Pulsing    = cfg.getValueOfKey<bool>("Recordinghead:Pulsing");
    }
    ~RecHead(){}

    void ApplyTspatialCont(const unsigned int Num_Layers, const Voronoi_t VORO, const double Environ_Temp,
                           const double Time, double*Temperature, Grain_t*Grains){

        double Temporal_temp, Spatial_temp;
        const double sigma_X = FWHM_X / sqrt(8.0 * log(2.0));
        const double sigma_Y = FWHM_Y / sqrt(8.0 * log(2.0));
        const double skew_angle = getSkew()*PI/180.0; // convert to radians

        Cooling_time=0.0;

        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
            double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];
            // Apply skew angle rotation
            const double cen_X_new = getSpeed()*Time + NFT_disp_X*  cos(skew_angle);
            const double cen_Y_new = getY()          + NFT_disp_X*(-sin(skew_angle));
            const double X_rotated = (Grain_X-cen_X_new)*cos(skew_angle) - (Grain_Y-cen_Y_new)*sin(skew_angle);
            const double Y_rotated = (Grain_X-cen_X_new)*sin(skew_angle) + (Grain_Y-cen_Y_new)*cos(skew_angle);

            //########## Vogler definition of the continuous laser profile.
            Temporal_temp = exp(-pow(X_rotated,2.0)/(2.0*pow(sigma_X,2.0)));
            Spatial_temp  = exp(-pow(Y_rotated,2.0)/(2.0*pow(sigma_Y,2.0)));
            *Temperature = Environ_Temp + (getTemp()-Environ_Temp)*Temporal_temp*Spatial_temp;
            // Assign temperature to each layer.
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains->Temp[grain_in_system] = *Temperature;
            }
        }
    }

    void ApplyTspatial(const Voronoi_t VORO, const unsigned int Num_Layers, const double Environ_Temp, const double Time, double*Spatial_Temp, Grain_t*Grain){

        double Temporal_Temp = (getTemp()-Environ_Temp)*exp(-pow(((Time-3*getCoolingTime())/getCoolingTime()),2.0));

        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
            double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];

            //########## Generic definition of the laser profile.
            *Spatial_Temp = Environ_Temp + Temporal_Temp * exp(-(pow(Grain_X-getX(),2.0)/(getGrad()*pow(FWHM_X,2.0)) + pow(Grain_Y-getY(),2.0)/(getGrad()*pow(FWHM_Y,2.0))));
            //########## Vogler definition of the laser profile.
    //      *Spatial_Temp = Expt_laser.Laser_Temp_MIN + Temporal_Temp * exp(-( pow(Grain_X-cen_X,2.0)/(2.0*pow(sigma_X,2.0)) + pow(Grain_Y-cen_Y,2.0)/(2.0*pow(sigma_Y,2.0)) ) );
            // Assign temperature to each layer.
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grain->Temp[grain_in_system] = *Spatial_Temp;
            }
        }
    }

    bool getPulsing(){return Pulsing;}

};

class RecLayer{

protected:

    unsigned int Bit_number_X;
    unsigned int Bit_number_Y;
    double Bit_size_X;
    double Bit_size_Y;
    double Bit_spacing_X;
    double Bit_spacing_Y;
    std::vector<int> Writable_data;
    std::vector<std::vector<unsigned int>> Grain_in_bit;

public:

    RecLayer(const ConfigFile cfg, double H_zdir)
    : Grain_in_bit(0)
{
        Bit_number_X = cfg.getValueOfKey<unsigned int>("RecordingLayer:Bit_per_track");
        Bit_number_Y = cfg.getValueOfKey<unsigned int>("RecordingLayer:Tracks");
        Bit_size_X   = cfg.getValueOfKey<double>("RecordingLayer:Bit_width");
        Bit_size_Y   = cfg.getValueOfKey<double>("RecordingLayer:Bit_length");
        Bit_spacing_X = cfg.getValueOfKey<double>("RecordingLayer:Bit_spacing");
        Bit_spacing_Y = cfg.getValueOfKey<double>("RecordingLayer:Track_spacing");

       if(cfg.getValueOfKey<std::string>("WritableData:DataType")=="square-wave"){
           Writable_data.resize((Bit_number_X*Bit_number_Y));
           for(unsigned int i=0;i<(Bit_number_X*Bit_number_Y);++i){
               Writable_data[i]=H_zdir;
               H_zdir *= -1;
           }
       }
       else if(cfg.getValueOfKey<std::string>("WritableData:DataType")=="binary"){
           // Open and read in digits from file
           std::string File_location = "Input/" + cfg.getValueOfKey<std::string>("WritableData:Data_Location");
           std::ifstream BINARY_DATA(File_location.c_str());
           if(!BINARY_DATA){throw std::runtime_error("CFG error: " + File_location + " not found"  + "\n");}
           int value;
           Writable_data.resize((Bit_number_X*Bit_number_Y));
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
           for(unsigned int i=0;i<(Bit_number_X*Bit_number_Y);++i){
               value = BINARY_DATA.get();
               if(isdigit(value)){value = value - '0';}
               if(value==0){Writable_data[i]=-1*H_zdir;}
               else{Writable_data[i]=H_zdir;}
           }
       } else {
           std::cout << " CFG error: Unknown Data write type. -> " << cfg.getValueOfKey<std::string>("WritableData:DataType") << std::endl;
           exit (EXIT_FAILURE);
       }
    }
    ~RecLayer(){}

    void setGrainsinBit(const unsigned int Num_Grains,const std::vector<double> PosX,
            const std::vector<double> PosY,const std::vector<std::pair<double,double>> Bit_positions){

        Grain_in_bit.resize(Bit_positions.size());

        for(unsigned int Bit=0;Bit<Bit_positions.size();++Bit){
            double bit_position_X=Bit_positions[Bit].first;
            double bit_position_Y=Bit_positions[Bit].second;

            for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
                if(PosX[grain_in_layer]>(bit_position_X-Bit_size_X*0.5) && PosX[grain_in_layer]<(bit_position_X+Bit_size_X*0.5)){
                    if(PosY[grain_in_layer]>(bit_position_Y-Bit_size_Y*0.5) && PosY[grain_in_layer]<(bit_position_Y+Bit_size_Y*0.5)){
                        Grain_in_bit[Bit].push_back(grain_in_layer);
                    }
                }
            }
        }
    }

    std::vector<unsigned int> getGrainsinBit(const unsigned int idx) const {
        return Grain_in_bit[idx];
    }

    void setBitsX(unsigned int val){Bit_number_X = val;}
    void setBitsY(unsigned int val){Bit_number_Y = val;}

    double getBitSizeX() const {return Bit_size_X;}
    double getBitSizeY() const {return Bit_size_Y;}

    double getBitSpacingX() const {return Bit_spacing_X;}
    double getBitSpacingY() const {return Bit_spacing_Y;}

    unsigned int getBitsX() const {return Bit_number_X;}
    unsigned int getBitsY() const {return Bit_number_Y;}
    unsigned int getBitsA() const {return Bit_number_X*Bit_number_Y;}

    int getWritableData(unsigned int idx) const {return Writable_data[idx];}

};

class ReadLayer{

protected:

    double Track_spacing;
    double Track_size;
    double Track_number;

public:

    ReadLayer(const RecLayer& RecordingLayer){
        Track_spacing = RecordingLayer.getBitSpacingY();
        Track_size    = RecordingLayer.getBitSizeY();
        Track_number  = RecordingLayer.getBitsY();
    }
    ReadLayer(const ConfigFile cfg)
        :Track_spacing(0.0)
        ,Track_size(0)
        ,Track_number(0)
    {
        if(cfg.getValueOfKey<bool>("ReadBack:Perform_after_write")){
            Track_spacing = cfg.getValueOfKey<double>("RecordingLayer:Track_spacing");
            Track_size    = cfg.getValueOfKey<double>("RecordingLayer:Bit_length");
            Track_number  = cfg.getValueOfKey<double>("RecordingLayer:Tracks");
        }
    }
    ~ReadLayer(){}

    double getBitSpacingY() const {return Track_spacing;}
    double getBitsY() const {return Track_number;}
    double getBitSizeY() const {return Track_size;}
};

class ReadHead{

protected:

    double SizeX;            //!< Width of read head
    double SizeY;            //!< Length of read head
    double PosX;
    double PosY;

public:

    ReadHead(const ConfigFile cfg)
    : SizeX(0.0)
    , SizeY(0.0)
    , PosX(0.0)
    , PosY(0.0)
{
        if(cfg.getValueOfKey<bool>("ReadBack:Perform_after_write")){
            SizeX = cfg.getValueOfKey<double>("ReadHead:SizeX");
            SizeY = cfg.getValueOfKey<double>("ReadHead:SizeY");
        }
    }
    ~ReadHead(){}

    double getSizeX() const {return SizeX;}
    double getSizeY() const {return SizeY;}
    double getPosX() const {return PosX;}
    double getPosY() const {return PosY;}

    void setPosX(double x){PosX = x;}
    void setPosY(double y){PosY = y;}

};

class BasicLaser {

protected:

    enum struct conditions{Min,Heating,Max,Cooling};

    conditions state;
    bool Active;
    bool Instantaneous;
    double TempMax;
    double TempMin;
    double TempCur;
    double Rate;

    void updateState(){
        switch(state)
        {
        case conditions::Min:
            if(Active){state = conditions::Heating;}
            break;
        case conditions::Heating:
            if(TempCur>=TempMax){state = conditions::Max;}
            break;
        case conditions::Max:
            if(!Active){state = conditions::Cooling;}
            break;
        case conditions::Cooling:
            if(TempCur<=TempMin){state = conditions::Min;}
            break;
        }
    }

public:
    BasicLaser(const ConfigFile cfg, const double Environ_temp)
    : state(conditions::Min)
    , Active(false)
    , TempMin(Environ_temp)
    , TempCur(Environ_temp)
{
    TempMax = cfg.getValueOfKey<double>("Laser:Temp");
    Instantaneous = !cfg.getValueOfKey<bool>("Laser:Include_heating_phase");
    if(!Instantaneous){
        Rate = cfg.getValueOfKey<double>("Laser:Rate");
    } else {
        Rate = 0.0;
    }
}
    ~BasicLaser(){}

    void turnOn(){Active=true;}
    void turnOff(){Active=false;}

    void updateLaser(double dt){
        updateState();
        switch(state)
        {
        case conditions::Min:
            TempCur=TempMin;
            break;
        case conditions::Max:
            TempCur=TempMax;
            break;
        case conditions::Heating:
            if(Instantaneous){TempCur=TempMax;}
            else{
                TempCur += dt*Rate;
                if(TempCur>TempMax){TempCur=TempMax;}
            }
            break;
        case conditions::Cooling:
            if(Instantaneous){TempCur=TempMin;}
            else{
                TempCur -= dt*Rate;
                if(TempCur<TempMin){TempCur=TempMin;}
            }
            break;
        }
        updateState();
    }

    void setTempMax(double val){TempMax=val;}

    double getTemp() const {return TempCur;}
    double getTempMax() const {return TempMax;}
    double getRate() const {return Rate;}
    double getCoolingTime() const {
        if(Rate>0.0){
            return (TempMax-TempMin)/Rate;
        } else {
            return 0.0;
        }
    }
    bool isOn() const {return Active;}
    bool isMax() const {
        switch(state)
        {
        case conditions::Max:
            return true;
        default:
            break;
        }
        return false;
    }
    bool isMin() const {
         switch(state)
         {
         case conditions::Min:
             return true;
         default:
             break;
         }
         return false;
     }

};

/** \brief Structure for data write simulation
 *
 * <P> This structure contains the parameters required for a data write simulation </P> */
struct Expt_data_write_t {

	unsigned int Bit_number_X;		//!< Number of bit per track
	unsigned int Bit_number_Y;		//!< Number of data tracks
	double Bit_width;				//!< Width of bit [nm]
	double Bit_length;				//!< Length of bit [nm]
	double Bit_spacing_X;			//!< Bit spacing [nm]
	double Bit_spacing_Y;			//!< Track spacing [nm]
	double WriteHead_speed;			//!< Speed of write head [nm&frasl;s]
	std::string Data_Binary;		//!< Flag indicating data type used for writing simulations (Square-wave OR import)
	std::vector<int> Writable_data;	//!< Binary data to be written in writing simulations

};

/** \brief Structure for data read back experimental parameters
 *
 * <P> This structure contains the parameters required to perform a read back simulation </P> */
struct Expt_data_read_t {

	bool ReadBack;					//!< Flag indicating if read back simulation is to be performed
	double Head_speed; 				//!< Speed of read head
	double Head_timestep;			//!< Time step used for read head simulation
	double Head_width;				//!< Width of read head
	double Head_length;				//!< Length of read head
	double Bit_spacing_Y;			//!< Spacing between data tracks
	double Bit_length;				//!< Length of Bits
	double Bit_number_Y;			//!< Number of data tracks

};

/** \brief Structure for laser experimental parameters
 *
 * <P> This structure contains the various parameters which are used to configure the laser pulse </P> */
struct Expt_laser_t {

	   double Laser_Temp_MIN;		//!< Minimum peak laser temperature
	   double Laser_Temp_MAX;		//!< Maximum peak laser temperature
	   double Laser_Temp_interval;	//!< Interval between peak laser temperatures
	   double Environ_temp;			//!< System temperature in the absence of a laser pulse
	   double Tprofile_gradient;	//!< Rate of temperature increase w.r.t distance from laser centre
	   double Tprofile_FWHM_X;		//!< Full width half maximum of laser profile in x-dir
	   double Tprofile_FWHM_Y;		//!< Full width half maximum of laser profile in y-dir
	   double cooling_time;			//!< Rate of temperature increase w.r.t time (This is a sixth of the total pulse time)
	   double NFT_to_Pole_spacing_X; //!< Spacing between Laser and write head
};

/** \brief Structure for environment parameters
 *
 * <P> This structure contains information about the environment the simulation occurs in. </P> */
struct Expt_environment_t {

    double Temperature;         //!< System temperature in the absence of a laser pulse
};

/** \brief Structure for Bit contents
 *
 * <P> This structure contains a list of all bits which lists each grain within each bit </P> */
struct Data_bit_t {

	std::vector<std::vector<double>> Bit_grain_list;	//!< List bits with each element listing the grains within each bit

};

/** \brief Structure for data files used for entire system import
 *
 * <P> This structure contains the names of all files required to generate a system from imported data </P> */
struct Data_input_t {

	std::string Voro_file;			//!< Data file containing all information required by Voronoi_t
	std::string St_Mat_file;		//!< Data file containing Number of grains, layers and layer thickness
	std::string Pos_file;			//!< Data file containing grain positions
	std::string Vert_file;			//!< Data file containing grain vertices
	std::string Geo_file;			//!< Data file containing grain geometrical centres
	std::string Neigh_file;			//!< Data file containing grain nearest neighbours
	std::string Mag_neigh_file;		//!< Data file containing grain magnetostatic neighbours
	std::string Area_file;			//!< Data file containing grain areas
	std::string	CL_file;			//!< Data file containing grain contact lengths
	std::string GpV_file;			//!< Data file containing grain data vectors
	std::string GpC_file;			//!< Data file containing grain Callen-Callen data
	std::string Gp_file;			//!< Data file containing grain scalar data
	std::string IpM_file;			//!< Data file containing magnetostatic neighbours list
	std::string IpE_file;			//!< Data file containing exchange neighbours list
	std::string IpW_file;			//!< Data file containing W-matrix elements
	std::string IpH_file;			//!< Data file containing exchange field strengths

};

struct SNR_t {

	unsigned int Snapshots;					//!< Total number of snapshots to take
	std::vector<double> Times;		//!< Time in seconds at which each snapshot is taken

};

#endif /* STRUCTURES_HPP_ */
