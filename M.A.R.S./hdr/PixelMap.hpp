/*
 * PixelMap.hpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

#ifndef M_A_R_S__SRC_PIXELMAP_HPP_
#define M_A_R_S__SRC_PIXELMAP_HPP_

#include "Pixel.hpp"

enum struct DataType {M,EA,H,T,ALL};

class PixelMap {

private:
    std::vector<std::vector<Pixel>> Cells;
    double cellsize;
public:
    PixelMap(const double Xmin, const double Ymin, const double Xmax, const double Ymax, const double cellsize);
    virtual ~PixelMap();

    void Discretise(const Voronoi_t &VORO, const Grain_t &Grains);
    void MapVal(const Grain_t&Grains, const DataType type);
    void Print(const std::string filename);
    void PrintwUpdate(const std::string filename, const Grain_t&Grain, const DataType type);
    void StoreinGrain(Grain_t&Grains);
//  void ClearH();
//  void ClearT();


//  std::vector<unsigned int> InRect(const double xMin, const double yMin, const double xMax, const double yMax) const;
    Pixel Access(const unsigned int X, const unsigned int Y) const;
    Pixel Access(const unsigned int ID) const;
    unsigned int getRows() const;
    unsigned int getCols() const;
    double getCellsize() const;
};

#endif /* M_A_R_S__SRC_PIXELMAP_HPP_ */
