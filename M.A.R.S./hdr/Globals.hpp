/* Globals.hpp
 *  Created on: 3 Aug 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Globals.hpp
 * \brief Header file for global variables. */

#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_

#include <random>

extern double PI;
extern double KB;
extern std::mt19937_64 Gen;

#endif /* GLOBALS_HPP_ */
