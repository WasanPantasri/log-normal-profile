/* Voronoi.cpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

// System header files
#include "math.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <iomanip>

// M.A.R.S. specific header files
#include "../hdr/RNG/Random.hpp"
#include "../hdr/Structures.hpp"

// Load all header files required for voro++.
#include "voro++.hh"

int Voronoi(const Structure_t Sys,const double Interaction_radius,Voronoi_t*Data){

    std::cout << "Performing Voronoi Construction...\n" << std::flush;

    // Temporary values
    double Local_Grain_width = Sys.Grain_width/(sqrt(Sys.Packing_fraction));
    double  chi, x,y,z, Area_temp, Grain_x, Grain_y, min_angle, Angle_temp, Angle_Vertex1,length_X, length_Y, length_HYP, Avg_X_vert, Avg_Y_vert,
          Distance, Radius=Interaction_radius;
    int cell_id, min_pos, ID_temp, count=0;

    // Temporary Holders
    std::vector<double> vertices, angle_hold, x_hold, y_hold, vx_hold, vy_hold;
    std::vector<std::vector<double>> Vertex_X_temp, Vertex_Y_temp;
    std::vector<std::vector<int>> Neighbour_temp;
    std::vector<int> Grain_ID, Grain_neighbours;

    const int Blocks_x = int(Sys.Dim_x/(3.0*(Local_Grain_width)/sqrt(3.0)));
    const int Blocks_y = int(Sys.Dim_y/(Local_Grain_width));
    const int Blocks_z = 1;

    Data->x_max=1.5*Local_Grain_width/sqrt(3)+Local_Grain_width*(Blocks_x-0.5)*sqrt(3);
    Data->y_max=Blocks_y*Local_Grain_width;

    std::cout << std::setprecision(15) << "\tx_max: " << Data->x_max << " y_max: " << Data->y_max << std::endl;

    chi = -0.00558 + 2.5497*Sys.StdDev_grain_pos - 5.24005*(Sys.StdDev_grain_pos*Sys.StdDev_grain_pos) + 9.28643*(Sys.StdDev_grain_pos*Sys.StdDev_grain_pos*Sys.StdDev_grain_pos);
    if(Sys.StdDev_grain_pos==0.0){chi=0.0;}
    chi *= Local_Grain_width;

    // Create the container with the desired geometry.
    voro::container container(0.0,Data->x_max,0.0,Data->y_max,-1.0,1.0,Blocks_x,Blocks_y,Blocks_z,true,true,false,8);
    voro::c_loop_all Container_loop(container);
    voro::voronoicell_neighbor Cell_with_neighbour_info;

//##############################GENERATE GRAIN POSITIONS###############################//
    for(int j=0;j<Blocks_y;++j){
        for(int i=0;i<Blocks_x;++i){
            x=0.5*Local_Grain_width/sqrt(3) + Local_Grain_width*double(i)*sqrt(3)+(2.0*mtrandom::grnd()-1.0)*chi;
            y=0.5*Local_Grain_width+Local_Grain_width*double(j)+(2.0*mtrandom::grnd()-1.0)*chi;
            z=0.0;
            ++count;
            container.put(count,x,y,z);
            x=0.5*Local_Grain_width/sqrt(3) + Local_Grain_width*(double(i)+0.5)*sqrt(3)+(2.0*mtrandom::grnd()-1.0)*chi;
            y=0.5*Local_Grain_width+Local_Grain_width*(double(j)+0.5)+(2.0*mtrandom::grnd()-1.0)*chi;
            z=0.0;
            ++count;
            container.put(count,x,y,z);
    }    }
    count = 0;

//#########################OBTAIN CELL ID; X,Y COORD; VERTICES#########################//
    if(Container_loop.start()){
        do if(container.compute_cell(Cell_with_neighbour_info,Container_loop)){
            Container_loop.pos(x,y,z);
            cell_id = Container_loop.pid();
            Grain_ID.push_back(cell_id);
            x_hold.push_back(x);y_hold.push_back(y);
            Cell_with_neighbour_info.vertices(x,y,z,vertices);
            for(size_t i=0; i<vertices.size(); i+=3){
                if(vertices[i+2] == 1){
                    vx_hold.push_back(vertices[i]);
                    vy_hold.push_back(vertices[i+1]);
            }    }
            Vertex_X_temp.push_back(vx_hold);
            Vertex_Y_temp.push_back(vy_hold);
            vx_hold.clear();
            vy_hold.clear();
        }
        while (Container_loop.inc());
    }

    Data->Pos_X_final.resize(x_hold.size());
    Data->Pos_Y_final.resize(y_hold.size());
    Data->Vertex_X_final.resize(x_hold.size());
    Data->Vertex_Y_final.resize(y_hold.size());
    Neighbour_temp.resize(Data->Pos_X_final.size());

    Data->Num_Grains = Data->Pos_X_final.size();
    std::cout << "\tNumber of Grains per layer: " << Data->Num_Grains << std::endl;

//################################OBTAIN NEIGHBOUR INFO################################//
    if(Container_loop.start()){
        do if(container.compute_cell(Cell_with_neighbour_info,Container_loop)){
            Cell_with_neighbour_info.neighbors(Grain_neighbours);
            for(size_t i=0; i<Grain_neighbours.size();++i){
                if(Grain_neighbours[i]>0){
                    Neighbour_temp[count].push_back(Grain_neighbours[i]-1);
            }    }
            ++count;
        }
        while (Container_loop.inc());
    }

//########################REORDER LISTS SO INDEX MATCHES GRAIN ID######################//
    Data->Neighbour_final.resize(Data->Pos_X_final.size());
    for(size_t i=0;i<Data->Pos_X_final.size();++i){
        ID_temp = Grain_ID[i]-1;            // Adjusts ID system to start from 0 instead of 1.
        Data->Pos_X_final[ID_temp] = x_hold[i];        // Place grain info into position=NewID.
        Data->Pos_Y_final[ID_temp] = y_hold[i];
        Data->Vertex_X_final[ID_temp].resize(Vertex_X_temp[i].size());
        Data->Vertex_Y_final[ID_temp].resize(Vertex_Y_temp[i].size());
        Data->Neighbour_final[ID_temp].resize(Neighbour_temp[i].size());
        for(size_t j=0;j<Vertex_X_temp[i].size();++j){
            Data->Vertex_X_final[ID_temp][j]=Vertex_X_temp[i][j];
            Data->Vertex_Y_final[ID_temp][j]=Vertex_Y_temp[i][j];
        }
        for(size_t j=0;j<Vertex_X_temp[i].size();++j){
            Data->Neighbour_final[ID_temp][j] = Neighbour_temp[i][j];
    }    }
    x_hold.clear();
    y_hold.clear();

//################################REORDER VERTEX LIST #################################//
    for(unsigned int i=0;i<Data->Num_Grains;++i){
      Grain_x=Data->Pos_X_final[i];
      Grain_y=Data->Pos_Y_final[i];
      vx_hold.resize(Data->Vertex_X_final[i].size());
      vy_hold.resize(Data->Vertex_Y_final[i].size());
      for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
          x_hold.push_back(Data->Vertex_X_final[i][j]);
          y_hold.push_back(Data->Vertex_Y_final[i][j]);
      }
      for(size_t j=0;j<x_hold.size();++j){
          Angle_temp = atan2((y_hold[j]-Grain_y),(x_hold[j]-Grain_x));
          if(Angle_temp<0){Angle_temp+=6.283185307179586;}
          angle_hold.push_back(Angle_temp);
      }
      min_pos=0;
      for(size_t j=0;j<angle_hold.size();++j){
          min_angle=7.0;
          for(size_t k=0;k<angle_hold.size();++k){
              if(angle_hold[k]<min_angle){
                  min_angle=angle_hold[k];
                  min_pos=k;
          }    }
          vx_hold[j]=x_hold[min_pos];
          vy_hold[j]=y_hold[min_pos];
          angle_hold[min_pos]=7.0;
      }
      for(size_t j=0;j<vx_hold.size();++j){
          Data->Vertex_X_final[i][j]=vx_hold[j];
          Data->Vertex_Y_final[i][j]=vy_hold[j];
      }
        x_hold.clear();
        y_hold.clear();
        vx_hold.clear();
        vy_hold.clear();
        angle_hold.clear();
    }

//#############################DETERMINE SYSTEM CENTRE#################################//
    double Vx, Vy;
    for(unsigned int grain=0;grain<Data->Num_Grains;++grain){
      for(size_t vertex=0;vertex<Data->Vertex_X_final[grain].size();++vertex){
          Vx = Data->Vertex_X_final[grain][vertex];
          Vy = Data->Vertex_Y_final[grain][vertex];
          if(Vx>Data->Vx_MAX){Data->Vx_MAX=Vx;}
          else if(Vx<Data->Vx_MIN){Data->Vx_MIN=Vx;}
          if(Vy>Data->Vy_MAX){Data->Vy_MAX=Vy;}
          else if(Vy<Data->Vy_MIN){Data->Vy_MIN=Vy;}
    }    }
    Data->Centre_X = (Data->Vx_MAX-Data->Vx_MIN)/2.0+Data->Vx_MIN;
    Data->Centre_Y = (Data->Vy_MAX-Data->Vy_MIN)/2.0+Data->Vy_MIN;

    std::cout << "\n\tSystem dimensions:\n\t\tX: " << Data->Vx_MIN << " to " << Data->Vx_MAX
            << "\n\t\tY: " << Data->Vy_MIN << " to " << Data->Vy_MAX
            << "\n\t\tCentre: " << Data->Centre_X << " , " << Data->Centre_Y << "\n" << std::endl;

//################################APPLY GRAIN SPACING##################################//
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        double local_Px=Data->Pos_X_final[grain_num], local_Py=Data->Pos_Y_final[grain_num];
        for(size_t vert=0;vert<Data->Vertex_X_final[grain_num].size();++vert){
            double local_Vx, local_Vy;
            local_Vx = Data->Vertex_X_final[grain_num][vert];
            local_Vy = Data->Vertex_Y_final[grain_num][vert];
            Data->Vertex_X_final[grain_num][vert] = local_Px+(local_Vx-local_Px)*sqrt(Sys.Packing_fraction);
            Data->Vertex_Y_final[grain_num][vert] = local_Py+(local_Vy-local_Py)*sqrt(Sys.Packing_fraction);
    }    }

//#################################DETERMINE GRAIN AREA################################//
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        Area_temp=0.0;
        for(size_t j=0;j<Data->Vertex_X_final[grain_num].size();++j){
            int k=j+1;
            if(j==Data->Vertex_X_final[grain_num].size()-1){k=0;}
                Area_temp += (Data->Vertex_X_final[grain_num][j]*Data->Vertex_Y_final[grain_num][k]);
                 Area_temp -= (Data->Vertex_Y_final[grain_num][j]*Data->Vertex_X_final[grain_num][k]);
            }
        Data->Grain_Area.push_back(Area_temp*0.5);
        Data->Grain_diameter.push_back(2*sqrt((Area_temp*0.5)/PI)); // circle assumption
    }

//###################REORDER NEIGHBOUR LIST TO MATCH VERTEX ORDERING###################//
    /* Here the Neighbours list is reordered, this is done by determining the angle (anti-clockwise)
     * from the grain and its neighbour. In order to match the vertices ordering, all determined
     * angles are shifted by the angle between the grain and the first vertex.
     */
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        vx_hold.resize(Data->Neighbour_final[grain_num].size());
        Grain_x=Data->Pos_X_final[grain_num];
        Grain_y=Data->Pos_Y_final[grain_num];
        Angle_Vertex1 = atan2((Data->Vertex_Y_final[grain_num][0]-Grain_y),(Data->Vertex_X_final[grain_num][0]-Grain_x));
      if(Angle_Vertex1<0){Angle_Vertex1+=6.283185307179586;}
        for(size_t j=0;j<Data->Neighbour_final[grain_num].size();++j){
            // Store Neighbour Coords
            x_hold.push_back(Data->Pos_X_final[Data->Neighbour_final[grain_num][j]]);
            y_hold.push_back(Data->Pos_Y_final[Data->Neighbour_final[grain_num][j]]);
        }
        for(size_t j=0;j<x_hold.size();++j){
            if((Grain_x-x_hold[j]) > (Data->Centre_X)){x_hold[j] += Data->x_max;}        // LEFT
            else if((Grain_x-x_hold[j]) < (-Data->Centre_X)){x_hold[j] -= Data->x_max;}    //RIGHT
            if((Grain_y-y_hold[j]) < (-Data->Centre_Y)){y_hold[j] -= Data->y_max;}        //TOP
            else if((Grain_y-y_hold[j]) > (Data->Centre_Y)){y_hold[j] += Data->y_max;}    //BOT
        }

          for(size_t j=0;j<x_hold.size();++j){
          Angle_temp = atan2((y_hold[j]-Grain_y),(x_hold[j]-Grain_x));
          if(Angle_temp<0){Angle_temp+=6.283185307179586;}
          Angle_temp -= Angle_Vertex1;
          if(Angle_temp<0){Angle_temp+=6.283185307179586;}
          angle_hold.push_back(Angle_temp);
        }
        min_pos=0;
        for(size_t j=0;j<angle_hold.size();++j){
             min_angle=7.0;
             for(size_t k=0;k<angle_hold.size();++k){
                 if(angle_hold[k]<min_angle){
                     min_angle=angle_hold[k];
                     min_pos=k;
             }    }
             vx_hold[j]=Data->Neighbour_final[grain_num][min_pos];
             angle_hold[min_pos]=7.0;
        }
        for(size_t j=0;j<x_hold.size();++j){
          Data->Neighbour_final[grain_num][j]=vx_hold[j];
        }
        x_hold.clear();
        y_hold.clear();
        vx_hold.clear();
        vy_hold.clear();
        angle_hold.clear();
    }

//##############################DETERMINE CONTACT LENGTHS##############################//
    Data->Contact_lengths.resize(Data->Num_Grains);
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        for(size_t i=0;i<Data->Vertex_X_final[grain_num].size();++i){
            int k=i+1;
            if(i==Data->Vertex_X_final[grain_num].size()-1){k=0;}
            length_X = fabs(Data->Vertex_X_final[grain_num][i]-Data->Vertex_X_final[grain_num][k]);
            length_Y = fabs(Data->Vertex_Y_final[grain_num][i]-Data->Vertex_Y_final[grain_num][k]);
            length_HYP = sqrt(length_X*length_X+length_Y*length_Y);
            Data->Contact_lengths[grain_num].push_back(length_HYP);
    }    }

//########################DETERMINE AVG CONTACT LENGTH AND AREA########################//
    int counter=0;
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        Data->Average_area += Data->Grain_Area[grain_num];
    }
    Data->Average_area /= Data->Num_Grains;
    std::cout << "\tAverage grain area: " << Data->Average_area << " nm^2" << std::endl;

    // DETERMINE STANDARD DEVIATION IN GRAIN AREA
    double VARIANCE=0.0;
    for(unsigned int grain=0;grain<Data->Num_Grains;++grain){
        VARIANCE += pow(Data->Grain_Area[grain]-Data->Average_area,2.0);
    }
    VARIANCE/=Data->Num_Grains;
    double STDDEV = sqrt(VARIANCE);
    std::cout << "\tAREA Std Dev : " << STDDEV << " nm^2" << std::endl;

    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        for(size_t neigh_num=0;neigh_num<Data->Contact_lengths[grain_num].size();++neigh_num){
            Data->Average_contact_length += Data->Contact_lengths[grain_num][neigh_num];
            ++counter;
    }    }
    Data->Average_contact_length /= counter;
    std::cout << "\tAverage grain contact length: " << Data->Average_contact_length << " nm" << std::endl;

//##########################DETERMINE GEOMETRIC GRAIN CENTRES##########################//
    Data->Geo_grain_centre_X.resize(Data->Pos_X_final.size());
    Data->Geo_grain_centre_Y.resize(Data->Pos_Y_final.size());

    for(unsigned int i=0;i<Data->Num_Grains;++i){
        Avg_X_vert = Avg_Y_vert = 0.0;
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            Avg_X_vert += Data->Vertex_X_final[i][j];
            Avg_Y_vert += Data->Vertex_Y_final[i][j];
        }
        Data->Geo_grain_centre_X[i] = Avg_X_vert/Data->Vertex_X_final[i].size();
        Data->Geo_grain_centre_Y[i] = Avg_Y_vert/Data->Vertex_X_final[i].size();
    }

//####################DETERMINE NEIGHBOURS FOR MAGNETOSTATIC MATRIX####################//
    if(Sys.Magnetostatics_gen_type=="dipole"){
        // Ensure the interaction radius is no larger then half the system size
        if (Radius>Data->Centre_X || Radius>Data->Centre_Y){
            std::cout << "\n\tInteraction radius too large" << std::endl;
            std::cout << "\t\tAdjusting radius..." << std::endl;
            if(Data->Centre_X>Data->Centre_Y){Radius=Data->Centre_Y/2.0;}
            else{Radius=Data->Centre_X/2.0;}
            std::cout << "\t\t    New Radius = " << Radius << "nm" << std::endl;
        }
        // Save interaction radius
        Data->Int_Rad = Radius;
        Data->Magnetostatic_neighbours.resize(Data->Num_Grains);
        double Neigh_x, Neigh_y;

        for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
            Grain_x=Data->Pos_X_final[grain_num];            // DETERMINE GRAIN POSITION
            Grain_y=Data->Pos_Y_final[grain_num];
            for(unsigned int neigh_num=0;neigh_num<Data->Num_Grains;++neigh_num){
                if(neigh_num!=grain_num){            // PREVENT SELF-COUNTING
                    Neigh_x=Data->Pos_X_final[neigh_num];
                    Neigh_y=Data->Pos_Y_final[neigh_num];
                    if((Grain_x-Neigh_x) > (Data->Centre_X/2.0)){Neigh_x += Data->x_max;}                // LHS
                    else if((Grain_x-Neigh_x) < (-Data->Centre_X)){Neigh_x -= Data->x_max;}            // RHS
                    if((Grain_y-Neigh_y) < (-Data->Centre_Y)){Neigh_y -= Data->y_max;}                // TOP
                    else if((Grain_y-Neigh_y) > (Data->Centre_Y)){Neigh_y += Data->y_max;}            // BOT

                    Distance = sqrt((Grain_x-Neigh_x)*(Grain_x-Neigh_x) + (Grain_y-Neigh_y)*(Grain_y-Neigh_y));
                    if(Distance < Radius){Data->Magnetostatic_neighbours[grain_num].push_back(neigh_num);}
        }    }    }

        //---------------------------REORDER LIST    ---    Not sure if required.
        for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
            vx_hold.resize(Data->Magnetostatic_neighbours[grain_num].size());
            Grain_x=Data->Pos_X_final[grain_num];
            Grain_y=Data->Pos_Y_final[grain_num];
            Angle_Vertex1 = atan2((Data->Vertex_Y_final[grain_num][0]-Grain_y),(Data->Vertex_X_final[grain_num][0]-Grain_x));
            if(Angle_Vertex1<0){Angle_Vertex1+=6.283185307179586;}
            for(size_t j=0;j<Data->Magnetostatic_neighbours[grain_num].size();++j){
                // Store Neighbour Coords
                x_hold.push_back(Data->Pos_X_final[Data->Magnetostatic_neighbours[grain_num][j]]);
                y_hold.push_back(Data->Pos_Y_final[Data->Magnetostatic_neighbours[grain_num][j]]);
            }
            for(size_t j=0;j<x_hold.size();++j){
                if((Grain_x-x_hold[j]) > (Data->Centre_X)){x_hold[j] += Data->x_max;}        // LEFT
                else if((Grain_x-x_hold[j]) < (-Data->Centre_X)){x_hold[j] -= Data->x_max;}    // RIGHT
                if((Grain_y-y_hold[j]) < (-Data->Centre_Y)){y_hold[j] -= Data->y_max;}        // TOP
                else if((Grain_y-y_hold[j]) > (Data->Centre_Y)){y_hold[j] += Data->y_max;}    // BOT
            }

            for(size_t j=0;j<x_hold.size();++j){
                Angle_temp = atan2((y_hold[j]-Grain_y),(x_hold[j]-Grain_x));
                if(Angle_temp<0){Angle_temp+=6.283185307179586;}
                Angle_temp -= Angle_Vertex1;
                if(Angle_temp<0){Angle_temp+=6.283185307179586;}
                angle_hold.push_back(Angle_temp);
            }
            min_pos=0;
            for(size_t j=0;j<angle_hold.size();++j){
                min_angle=7.0;
                for(size_t k=0;k<angle_hold.size();++k){
                    if(angle_hold[k]<min_angle){
                        min_angle=angle_hold[k];
                        min_pos=k;
                }    }
                vx_hold[j]=Data->Magnetostatic_neighbours[grain_num][min_pos];
                angle_hold[min_pos]=7.0;
            }
            for(size_t j=0;j<x_hold.size();++j){
                Data->Magnetostatic_neighbours[grain_num][j]=vx_hold[j];
            }
            x_hold.clear();
            y_hold.clear();
            vx_hold.clear();
            vy_hold.clear();
            angle_hold.clear();
        }

        double Avg_Mneigh=0.0;
        for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){Avg_Mneigh += Data->Magnetostatic_neighbours[grain_num].size();}
        Avg_Mneigh /= Data->Num_Grains;
        std::cout << "\tAverage Magnetostatic neighbours: " << Avg_Mneigh << std::endl;
    }
    double Avg_Neigh=0.0;
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){Avg_Neigh += Data->Neighbour_final[grain_num].size();}
    Avg_Neigh /= Data->Num_Grains;
    std::cout << "\tAverage Exchange Neighbours: " << Avg_Neigh << std::endl;
//#################################WRITE DATA TO FILES#################################//
    // POSITIONS
    std::ofstream POS_FILE("Output/pos_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        POS_FILE << Data->Pos_X_final[i] << " " << Data->Pos_Y_final[i] << "\n";
    }
    POS_FILE.flush();
    POS_FILE.close();
    // GEOMETRIC CENTRES
    std::ofstream GEO_FILE("Output/geo_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        GEO_FILE << Data->Geo_grain_centre_X[i] << " " << Data->Geo_grain_centre_Y[i] << "\n";
    }
    GEO_FILE.flush();
    GEO_FILE.close();
    // VERTICES
    std::ofstream VERT_FILE("Output/vert_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        VERT_FILE << i << " " << Data->Vertex_X_final[i].size() << "\n";
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            VERT_FILE << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << "\n";
        }
        VERT_FILE << Data->Vertex_X_final[i][0] << " " << Data->Vertex_Y_final[i][0] << "\n";
        VERT_FILE << "\n";
    }
    VERT_FILE.flush();
    VERT_FILE.close();

    std::ofstream VERT_GNU_FILE("Output/gnuplot_vert_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            VERT_GNU_FILE << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << "\n";
        }
        VERT_GNU_FILE << Data->Vertex_X_final[i][0] << " " << Data->Vertex_Y_final[i][0] << "\n";
        VERT_GNU_FILE << "\n\n";
    }
    VERT_GNU_FILE.flush();
    VERT_GNU_FILE.close();

    // NEIGHBOURS
    std::ofstream NEIGH_FILE("Output/neigh_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        NEIGH_FILE << i << " " << Data->Neighbour_final[i].size() << "\n";
        for(size_t j=0;j<Data->Neighbour_final[i].size();++j){
            NEIGH_FILE << Data->Neighbour_final[i][j] << " ";
        }
        NEIGH_FILE << "\n";
    }
    NEIGH_FILE.flush();
    NEIGH_FILE.close();
    if(Sys.Magnetostatics_gen_type=="dipole"){
        // MAGNETOSTATIC NEIGHBOURS
        std::ofstream MAG_NEIGH_FILE("Output/mag_neigh_file.dat");
        for(unsigned int i=0;i<Data->Num_Grains;++i){
            MAG_NEIGH_FILE << i << " " << Data->Magnetostatic_neighbours[i].size() << "\n";
            for(size_t j=0;j<Data->Magnetostatic_neighbours[i].size();++j){
                MAG_NEIGH_FILE << Data->Magnetostatic_neighbours[i][j] << " ";
            }
            MAG_NEIGH_FILE << "\n";
        }
        MAG_NEIGH_FILE.flush();
        MAG_NEIGH_FILE.close();
    }
    // AREAS
    std::ofstream AREA_FILE("Output/area_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        AREA_FILE << Data->Grain_Area[i] << "\n";
    }
    AREA_FILE.flush();
    AREA_FILE.close();
    // DIAMETERS
    std::ofstream DIAMETER_FILE("Output/diameter_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        DIAMETER_FILE << Data->Grain_diameter[i] << "\n";
    }
    DIAMETER_FILE.flush();
    DIAMETER_FILE.close();
    // CONTACT LENGTHS
    std::ofstream CONTACT_FILE("Output/contact_file.dat");
    for(size_t i=0;i<Data->Pos_X_final.size();++i){
      CONTACT_FILE << i << " " << Data->Contact_lengths[i].size() << "\n";
      for(size_t j=0;j<Data->Contact_lengths[i].size();++j){
          CONTACT_FILE << Data->Contact_lengths[i][j] << " ";
      }
      CONTACT_FILE << "\n";
    }
    CONTACT_FILE.flush();
    CONTACT_FILE.close();
    // OUTPUT FOR IMPROVED DETERMINATION OF MAGNETOSTATICS VIA SEPARATE CODE
    std::ofstream MAGNETO_OUT_1("Output/pc.dat");
    MAGNETO_OUT_1 << Data->Num_Grains << " " << Data->x_max << " " << Data->y_max << " " << Data->Average_area << std::endl;
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        MAGNETO_OUT_1 << i << " " << Data->Pos_X_final[i] << " " << Data->Pos_Y_final[i] << " " << Data->Neighbour_final[i].size() << std::endl;
    }
    MAGNETO_OUT_1.flush();
    MAGNETO_OUT_1.close();
    std::ofstream MAGNETO_OUT_2("Output/neighcheck.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        MAGNETO_OUT_2 << i << "\n" << Data->Neighbour_final[i].size() << std::endl;
        for(size_t j=0;j<Data->Neighbour_final[i].size();++j){
            int k;
            if(j==Data->Neighbour_final[i].size()-1){k=0;}
            else{k=j+1;}

            MAGNETO_OUT_2 << Data->Neighbour_final[i][j] << " "
                          << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << " "
                          << Data->Vertex_X_final[i][k] << " " << Data->Vertex_Y_final[i][k] << " "
                          << std::endl;
    }    }
    MAGNETO_OUT_2.flush();
    MAGNETO_OUT_2.close();
    std::ofstream MAGNETO_OUT_3("Output/initgs.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        MAGNETO_OUT_3 << "# " << i << " " << Data->Vertex_X_final[i].size() << std::endl;
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            MAGNETO_OUT_3 << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << "\n";
        }
        MAGNETO_OUT_3 << Data->Vertex_X_final[i][0] << " " << Data->Vertex_Y_final[i][0] << "\n";
    }
    MAGNETO_OUT_3.flush();
    MAGNETO_OUT_3.close();
    std::cout << std::endl;
    return 0;
}
