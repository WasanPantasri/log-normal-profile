/*
 * Thermoremanence.cpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include "math.h"

#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/Structures.hpp"
#include "../hdr/Importers/Structure_import.hpp"
#include "../hdr/Importers/Grain_setup.hpp"
#include "../hdr/Importers/Materials_import.hpp"
#include "../hdr/Voronoi.hpp"
#include "../hdr/Data_output/HAMR_grain_output.hpp"
#include "../hdr/Importers/Experiments/Experiment_long_timescale_import.hpp"
#include "../hdr/Interactions/Generate_interactions.hpp"
#include "../hdr/Solver.h"

static int Determine_averages(unsigned int Num_layers, unsigned int Num_Grains, Grain_t*Grain,
        std::vector<double>*Average_Mx,std::vector<double>*Average_My,std::vector<double>*Average_Mz,std::vector<double>*Probability){

    std::vector<double> SUM_MsV(Num_layers+1,0.0);
    double SUM_MsV_all_layers=0.0, Mx_all_layers=0.0, My_all_layers=0.0, Mz_all_layers=0.0, Prob_all_layers=0.0;
    double Tot_grains = Num_Grains*Num_layers;

    Average_Mx->resize(Num_layers+1,0.0);
    Average_My->resize(Num_layers+1,0.0);
    Average_Mz->resize(Num_layers+1,0.0);
    Probability->resize(Num_layers+1,0.0);

    //##############################DETERMINE AVERAGE MAGNETISATIONS##############################//
    // Experimentally they measure Ms*V, thus the output needs to match that.
    // Determine normalisation values.
    for(unsigned int layer=0;layer<Num_layers;++layer){
        double dummy_SUM_MsV=0.0, offset=Num_Grains*layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            int grain_in_system = grain_in_layer+offset;
            dummy_SUM_MsV += Grain->Ms[grain_in_system]*Grain->Vol[grain_in_system];
        }
        SUM_MsV[layer]=dummy_SUM_MsV;
        SUM_MsV_all_layers+=SUM_MsV[layer];
    }
    SUM_MsV.back() = SUM_MsV_all_layers;

    // Determine average magnetisations.
    for(unsigned int layer=0;layer<Num_layers;++layer){
        double offset=Num_Grains*layer;
        Average_Mx->at(layer)=Average_My->at(layer)=Average_Mz->at(layer)=Probability->at(layer)=0.0;
        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            int grain_in_system = grain_in_layer+offset;
            Average_Mx->at(layer)  += Grain->Ms[grain_in_system]*Grain->Vol[grain_in_system]*Grain->m[grain_in_system].x;
            Average_My->at(layer)  += Grain->Ms[grain_in_system]*Grain->Vol[grain_in_system]*Grain->m[grain_in_system].y;
            Average_Mz->at(layer)  += Grain->Ms[grain_in_system]*Grain->Vol[grain_in_system]*Grain->m[grain_in_system].z;
            double Pol = (Grain->m[grain_in_system].z/fabs(Grain->m[grain_in_system].z));
            if(Pol>0){++Probability->at(layer);}
        }
        Mx_all_layers      += Average_Mx->at(layer);
        My_all_layers      += Average_My->at(layer);
        Mz_all_layers      += Average_Mz->at(layer);
        Prob_all_layers    += Probability->at(layer);
        Average_Mx->at(layer)  /= SUM_MsV[layer];
        Average_My->at(layer)  /= SUM_MsV[layer];
        Average_Mz->at(layer)  /= SUM_MsV[layer];
        Probability->at(layer) /= Num_Grains;

    }
    // Full system values
    Average_Mx->back() = Mx_all_layers/SUM_MsV.back();
    Average_My->back() = My_all_layers/SUM_MsV.back();
    Average_Mz->back() = Mz_all_layers/SUM_MsV.back();
    Probability->back() = Prob_all_layers/Tot_grains;

    return 0;
}

int Thermoremanence(const ConfigFile cfg){
    std::cout << "Thermoremanence simulation" << std::endl;

    // TODO set simulation dependent inputs to use SImulation name as prefix

    // Required input
    double Environ_Temp = cfg.getValueOfKey<double>("Sim:Environment_Temp");
    double Laser_Temp_MIN = cfg.getValueOfKey<double>("Thermoremanence:Laser_min");
    double Laser_Temp_MAX = cfg.getValueOfKey<double>("Thermoremanence:Laser_max");
    double Laser_Temp_interval = cfg.getValueOfKey<double>("Thermoremanence:Laser_interval");

    double Equilibration_time = cfg.getValueOfKey<double>("Timings:Initialisation_time");
    double Application_time = cfg.getValueOfKey<double>("Timings:Application_time");

    bool OUTPUT_GRAINS = cfg.getValueOfKey<bool>("Sim:Output_individual_Grain_data");
    unsigned int RES_grain=0;
    if(OUTPUT_GRAINS){
        RES_grain = cfg.getValueOfKey<unsigned int>("Sim:Output_every_x_grain");
    }

    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t Int_system;
    Grain_t Grain, Grain_BACKUP;

    double Time=0.0;
    std::string FILENAME_idv;

    std::ofstream OUTPUT_FILE_in_time, OUTPUT_FILE_final_values, MAG_DIST_OUT, TC_DIST_CHECK_FILE("Output/TC_dist_check.dat");
    TC_DIST_CHECK_FILE << "Grain Tc tot_grains" << std::endl;

//##########################################IMPORT ALL REQUIRED DATA#########################################//
    Structure_import(cfg, &Structure);
    ExpHdc FieldDC(cfg);
    BasicLaser LaserPulse(cfg,Environ_Temp);
    Materials_import(cfg,Structure.Num_layers, &Materials_Config, &Materials);
    Voronoi(Structure,Structure.Magneto_Interaction_Radius,&Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grain);
    Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);
    Solver_t Solver(Structure.Num_layers,cfg,Materials_Config);
    Solver.Force_specific_solver_construction(Structure.Num_layers,cfg,Materials_Config,Solvers::kMC); // Ensure kMC data is imported from configuration file
    Solver.disableExclusion();
    unsigned int Output_Steps = Solver.getOutputSteps();
    unsigned int Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;

    for(unsigned int grain_in_system=0;grain_in_system<Tot_grains;++grain_in_system){
        Grain.H_appl[grain_in_system] = FieldDC.getMax()*FieldDC.getDirn();
    }
    int Iterations = (Laser_Temp_MAX - Laser_Temp_MIN)/Laser_Temp_interval;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###########Make a copy of every grain in order to reset the system after each laser pulse##################//
    Grain_BACKUP = Grain;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################################PERFORM LASER PULSES#########################################//
    Solvers input_Solver=Solver.get_Solver();
    double input_dt=Solver.get_dt();
    double reduced_dt=input_dt;

    for(int runs=0;runs<Iterations;++runs){
        unsigned int steps=0;
        LaserPulse.setTempMax(Laser_Temp_MIN+runs*Laser_Temp_interval);
        // Ensure at least 100 steps are performed during the heating/cooling phases
        if(LaserPulse.getRate()>0.0){
            if(LaserPulse.getCoolingTime()<100.0*Solver.get_dt()){
                reduced_dt = LaserPulse.getCoolingTime()*0.01;
                Solver.set_dt(reduced_dt);
                std::cout << "Reduced dt to " << reduced_dt << " for heating and cooling phases." << std::endl;
            }
        }
        //#########################################SET UP OUTPUT FILES#########################################//
        std::vector<double> Average_Mx, Average_My,Average_Mz, Probability;
        std::string FILENAME_pre_T = "Temp_" + std::to_string(LaserPulse.getTempMax()) + "_";
        std::cout << std::setprecision(5) << "Pulse temperature = " << LaserPulse.getTempMax() << std::endl;

        for(unsigned int layer=0;layer<=Structure.Num_layers;++layer){
            std::string FILENAME_pre_1 = "Layer_" + std::to_string(layer) + "_";
            OUTPUT_FILE_in_time.open(("Output/" + FILENAME_pre_1 + FILENAME_pre_T + "Average_M.dat").c_str());
            OUTPUT_FILE_in_time << "Mx My Mz Prob Temp time" << std::endl;
            OUTPUT_FILE_in_time.close();

            if(layer==0){
                if(OUTPUT_GRAINS==true){
                    for(unsigned int OUT_grain=0;OUT_grain<Voronoi_data.Num_Grains;OUT_grain+=RES_grain){
                        std::string FILENAME_pre_1b = "Grain_" + std::to_string(OUT_grain) + "_";
                        OUTPUT_FILE_in_time.open(("Output/GRAINS/" + FILENAME_pre_1 + FILENAME_pre_1b + FILENAME_pre_T + "_M.dat").c_str());
                        OUTPUT_FILE_in_time << "Mx My Mz Temp time" << std::endl;
                        OUTPUT_FILE_in_time.close();
        }   }   }   }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        //#########################################Equilibration phase#########################################//
        while(Time<Equilibration_time){
            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
            Time += Solver.get_dt();
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        //##########################################Apply Laser pulse##########################################//
        LaserPulse.turnOn();
        Time = 0.0;
        double Time_dur_app=0.0;
        do{
            std::cout << std::setprecision(5) << Time << " | | " << LaserPulse.getTemp() << "\r" << std::flush; //std::endl;
            // Perform simulation
            LaserPulse.updateLaser(Solver.get_dt());
            for(unsigned int grain=0;grain<Tot_grains;++grain){Grain.Temp[grain] = LaserPulse.getTemp();}
            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
            ++steps;
            Time += Solver.get_dt();

            if(LaserPulse.isMax()){
                Time_dur_app+=Solver.get_dt();
                // Reset timesteps for application phase
                Solver.set_dt(input_dt);
                // Utilise kMC solver if application time exceeds 1 microsecond
                if(Application_time>1.0e-6){Solver.set_Solver(Solvers::kMC);}
                // Revert solver once application is completed & set reduced timesteps for cooling phase
                if(Time_dur_app>=Application_time){
                    Solver.set_Solver(input_Solver);
                    Solver.set_dt(reduced_dt);
                    LaserPulse.turnOff();
                }
            }
            //##########################################Apply Laser pulse##########################################//
            if(steps==Output_Steps){
                steps=0;
                Determine_averages(Structure.Num_layers,Voronoi_data.Num_Grains,&Grain,&Average_Mx,&Average_My,&Average_Mz,&Probability);
                // OUTPUT for time resolved data
                for(unsigned int layer=0;layer<=Structure.Num_layers;++layer){
                    std::string FILENAME_pre_1 = "Layer_" + std::to_string(layer) + "_";
                    OUTPUT_FILE_in_time.open(("Output/" + FILENAME_pre_1 + FILENAME_pre_T + "Average_M.dat").c_str(),std::ofstream::app);
                    OUTPUT_FILE_in_time <<std::setprecision(15) << Average_Mx[layer] << " " << Average_My[layer]  << " " << Average_Mz[layer]  << " " << Probability[layer]  << " " << Grain.H_appl[0].z << " " << Grain.Temp[0] << " " << Time << std::endl;
                    OUTPUT_FILE_in_time.close();

                    int Offset=layer*Voronoi_data.Num_Grains;
                    if(layer==0){
                        if(OUTPUT_GRAINS==true){
                            for(unsigned int OUT_grain=0;OUT_grain<Voronoi_data.Num_Grains;OUT_grain+=RES_grain){
                                int grain_in_system = OUT_grain+Offset;
                                std::string FILENAME_pre_1b = "Grain_" + std::to_string(OUT_grain) + "_";
                                OUTPUT_FILE_in_time.open(("Output/GRAINS/" + FILENAME_pre_1 + FILENAME_pre_1b + FILENAME_pre_T + "_M.dat").c_str(),std::ofstream::app);
                                OUTPUT_FILE_in_time << std::setprecision(15) << Grain.m[grain_in_system] << " " << " " << Grain.Temp[grain_in_system] << " " << Time << std::endl;
                                OUTPUT_FILE_in_time.close();
                }   }   }   }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        }
        while(LaserPulse.isOn() || !LaserPulse.isMin());
        std::cout << std::endl;
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        //################################ OUTPUT MAGNETISATION DISTRIBUTION ##################################//

        for(unsigned int layer=0;layer<Structure.Num_layers;++layer){
            std::string FILENAME_pre_L = "Layer_" + std::to_string(layer) + "_";
            MAG_DIST_OUT.open(("Output/"+FILENAME_pre_L+FILENAME_pre_T+"Mag_dist.dat").c_str());
            double offset=Voronoi_data.Num_Grains*layer;
            for(unsigned int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
                int grain_in_system = grain_in_layer+offset;
                MAG_DIST_OUT << Voronoi_data.Num_Grains << " " << grain_in_system << " "
                             << Grain.m[grain_in_system].x << " " << Grain.m[grain_in_system].y << " "
                             << Grain.m[grain_in_system].z << " " << LaserPulse.getTempMax() << " " << Time << std::endl;
            }
            MAG_DIST_OUT.close();
        }
        // Output for thermo-remanence data
        Determine_averages(Structure.Num_layers,Voronoi_data.Num_Grains,&Grain,&Average_Mx,&Average_My,&Average_Mz,&Probability);
        for(unsigned int layer=0;layer<=Structure.Num_layers;++layer){
            std::string FILENAME_pre_L = "Layer_" + std::to_string(layer) + "_";
            OUTPUT_FILE_final_values.open(("Output/" + FILENAME_pre_L + "Final_magnetisation.dat").c_str(),std::ofstream::app);
            OUTPUT_FILE_final_values << Average_Mx[layer] << " " << Average_My[layer]  << " " << Average_Mz[layer]  << " " << Probability[layer]  << " " <<  LaserPulse.getTempMax() << " " << Time << std::endl;
            OUTPUT_FILE_final_values.close();
        }
        // Output final system configuration
        Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,".dat",FILENAME_pre_T+"Final_config_");
        Grain = Grain_BACKUP;
        Time = 0.0;
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    }
    return 0;
}
