/*
 * FMR.cpp
 *
 *  Created on: 20 May 2020
 *      Author: Ewan Rannala
 */

#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/Structures.hpp"
#include "../hdr/Interactions/Generate_interactions.hpp"
#include "../hdr/Solver.h"

#include "../hdr/Voronoi.hpp"
#include "../hdr/Importers/Grain_setup.hpp"
#include "../hdr/Importers/Materials_import.hpp"
#include "../hdr/Importers/Structure_import.hpp"


#include <iostream>
#include <fstream>

void updateGrainField(Vec3 Happ, std::vector<Vec3>*GrainH){

    for(unsigned int grain=0;grain<GrainH->size();++grain){
        GrainH->at(grain) = Happ;
    }

}

int FMR(const ConfigFile cfg){

    double Run_time = cfg.getValueOfKey<double>("FMR:Run_time");
    double Environ_Temp = cfg.getValueOfKey<double>("FMR:Environment_Temperature");

    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t Int_system;
    Grain_t Grains;

    Structure_import(cfg,&Structure);
    Materials_import(cfg,Structure.Num_layers,&Materials_Config,&Materials);
    Voronoi(Structure,Structure.Magneto_Interaction_Radius,&Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grains);
    Generate_interactions(Structure.Num_layers,Grains.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);

    Solver_t Solver(Structure.Num_layers,cfg,Materials_Config);
    ExpHac AC(cfg);
    ExpHdc DC(cfg);

    std::ofstream OUTPUT("Output/FMR.dat");
    OUTPUT << "mx my mz ex ey ez DCx DCy DCz ACx ACy ACz time" << std::endl;

    updateGrainField(AC.getField()+DC.getField(),&Grains.H_appl);

    double Time=0.0;
    unsigned int steps=0;
    while(Time<Run_time){


        Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grains);
        Time += Solver.get_dt();

        Vec3 m,e;
        for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
            m += Grains.m[grain];
            e += Grains.Easy_axis[grain];
        }
        m /= Voronoi_data.Num_Grains;
        e /= Voronoi_data.Num_Grains;

        if(steps==Solver.getOutputSteps()){
            OUTPUT << m << " " << e << " " << DC.getField() << " "
                    << AC.getField() << " " << Time << std::endl;
            steps=0;
        }
        // Update field - only AC field
        AC.updateField(Time);
        updateGrainField(AC.getField()+DC.getField(),&Grains.H_appl);
        ++steps;

    }
    OUTPUT.close();

    return 0;
}


