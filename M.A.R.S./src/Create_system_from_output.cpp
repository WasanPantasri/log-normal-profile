/*
 * Create_system_from_output.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Create_system_from_output.cpp
 * \brief Function used to read in the required files to generated a complete granular system. */

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <istream>

#include "../hdr/Structures.hpp"
#include "../hdr/Importers/Read_in_import.hpp"

/** Functions reads in all required data files and fills the required storage vectors.
 * \param[in] IN_data Struct containing filenames for all required input files.
 * \param[out] Structure Pointer to a structure data type.
 * \param[out] VORO Pointer to Voronoi data type.
 * \param[out] Materials Pointer to Materials data type.
 * \param[out] Grain Pointer to Grain data type.
 * \param[out] Int Pointer to Interaction data type
 *  */
int Read_in_system(const Data_input_t IN_data, Structure_t*Structure, Voronoi_t*VORO, Material_t*Materials, Grain_t*Grain, Interaction_t*Int){

	int Num_Grains, Vert_num, Neigh_num,
	    Mag_Neigh_num, CL_Num, Num_Layers,
	    W_num, H_num;
	int grain, total_grains;
	std::string placeholder_str;
	double placeholder_dbl;

	// INPUTS
	//
	std::ifstream INPUT_VORO_FILE     (("Input/System/"+IN_data.Voro_file).c_str(),      std::ifstream::in);
	std::ifstream INPUT_ST_MAT_FILE   (("Input/System/"+IN_data.St_Mat_file).c_str(),    std::ifstream::in);
	// Structure
	std::ifstream INPUT_POS_FILE      (("Input/System/"+IN_data.Pos_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_VERT_FILE     (("Input/System/"+IN_data.Vert_file).c_str(),      std::ifstream::in);
	std::ifstream INPUT_GEO_FILE      (("Input/System/"+IN_data.Geo_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_NEIGH_FILE    (("Input/System/"+IN_data.Neigh_file).c_str(),     std::ifstream::in);
	std::ifstream INPUT_MAGNEIGH_FILE (("Input/System/"+IN_data.Mag_neigh_file).c_str(), std::ifstream::in);
	std::ifstream INPUT_AREA_FILE     (("Input/System/"+IN_data.Area_file).c_str(),      std::ifstream::in);
	std::ifstream INPUT_CL_FILE       (("Input/System/"+IN_data.CL_file).c_str(),        std::ifstream::in);
	// Grain parameters
	std::ifstream INPUT_GPV_FILE      (("Input/System/"+IN_data.GpV_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_GPC_FILE      (("Input/System/"+IN_data.GpC_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_GP_FILE       (("Input/System/"+IN_data.Gp_file).c_str(),        std::ifstream::in);
	// Interaction parameters
	std::ifstream INPUT_INT_MN_FILE   (("Input/System/"+IN_data.IpM_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_INT_EN_FILE   (("Input/System/"+IN_data.IpE_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_INT_W_FILE    (("Input/System/"+IN_data.IpW_file).c_str(),       std::ifstream::in);
	std::ifstream INPUT_INT_HEXC_FILE (("Input/System/"+IN_data.IpH_file).c_str(),       std::ifstream::in);


	// Check that all the required files have been opened
	if(!INPUT_VORO_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.Voro_file      + " system input files\n");}
	if(!INPUT_ST_MAT_FILE){  throw std::runtime_error("ERROR: Missing " + IN_data.St_Mat_file    + " system input files\n");}
	if(!INPUT_POS_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.Pos_file       + " system input files\n");}
	if(!INPUT_VERT_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.Vert_file      + " system input files\n");}
	if(!INPUT_GEO_FILE){     throw std::runtime_error("ERROR: Missing " + IN_data.Geo_file       + " system input files\n");}
	if(!INPUT_NEIGH_FILE){   throw std::runtime_error("ERROR: Missing " + IN_data.Neigh_file     + " system input files\n");}
	if(!INPUT_MAGNEIGH_FILE){throw std::runtime_error("ERROR: Missing " + IN_data.Mag_neigh_file + " system input files\n");}
	if(!INPUT_AREA_FILE){    throw std::runtime_error("ERROR: Missing " + IN_data.Area_file      + " system input files\n");}
	if(!INPUT_CL_FILE){		 throw std::runtime_error("ERROR: Missing " + IN_data.CL_file        + " system input files\n");}
	if(!INPUT_GPV_FILE){	 throw std::runtime_error("ERROR: Missing " + IN_data.GpV_file       + " system input files\n");}
	if(!INPUT_GPC_FILE){	 throw std::runtime_error("ERROR: Missing " + IN_data.GpC_file       + " system input files\n");}
	if(!INPUT_GP_FILE){		 throw std::runtime_error("ERROR: Missing " + IN_data.Gp_file        + " system input files\n");}
	if(!INPUT_INT_MN_FILE){	 throw std::runtime_error("ERROR: Missing " + IN_data.IpM_file       + " system input files\n");}
	if(!INPUT_INT_EN_FILE){	 throw std::runtime_error("ERROR: Missing " + IN_data.IpE_file       + " system input files\n");}
	if(!INPUT_INT_W_FILE){	 throw std::runtime_error("ERROR: Missing " + IN_data.IpW_file       + " system input files\n");}
	if(!INPUT_INT_HEXC_FILE){throw std::runtime_error("ERROR: Missing " + IN_data.IpH_file       + " system input files\n");}

//#####################################################################################//

	INPUT_VORO_FILE >> VORO->Centre_X >> VORO->Centre_Y
					>> VORO->Vx_MAX >> VORO->Vy_MAX
					>> VORO->Vx_MIN >> VORO->Vy_MIN;

	INPUT_ST_MAT_FILE >> VORO->Num_Grains >> Structure->Num_layers;
	Num_Grains = VORO->Num_Grains;
	Num_Layers = Structure->Num_layers;
	Materials->dz.resize(Num_Layers);
	for(int LAYER=0;LAYER<Num_Layers;++LAYER){
		INPUT_ST_MAT_FILE >> Materials->dz[LAYER];
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#################################IMPORTING STRUCTURE#################################//
	// Positions
	VORO->Pos_X_final.resize(Num_Grains);
	VORO->Pos_Y_final.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_POS_FILE >> VORO->Pos_X_final[grain_in_layer] >> VORO->Pos_Y_final[grain_in_layer];
	}
	// Vertices
	VORO->Vertex_X_final.resize(Num_Grains);
	VORO->Vertex_Y_final.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_VERT_FILE >> grain >> Vert_num;
		VORO->Vertex_X_final[grain_in_layer].resize(Vert_num);
		VORO->Vertex_Y_final[grain_in_layer].resize(Vert_num);
		for(int vert=0;vert<Vert_num;++vert){
			INPUT_VERT_FILE >> VORO->Vertex_X_final[grain_in_layer][vert] >> VORO->Vertex_Y_final[grain_in_layer][vert];
		}
		// Clear line with repeated initial vertex
		INPUT_VERT_FILE >> placeholder_dbl >> placeholder_dbl;
	}
	// Geo centre
	VORO->Geo_grain_centre_X.resize(Num_Grains);
	VORO->Geo_grain_centre_Y.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_GEO_FILE >> VORO->Geo_grain_centre_X[grain_in_layer] >> VORO->Geo_grain_centre_Y[grain_in_layer];
	}
	// Neighbours
	VORO->Neighbour_final.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_NEIGH_FILE >> grain >> Neigh_num;
		VORO->Neighbour_final[grain_in_layer].resize(Neigh_num);
		for(int neigh=0;neigh<Neigh_num;++neigh){
			INPUT_NEIGH_FILE >> VORO->Neighbour_final[grain_in_layer][neigh];
		}
	}
	// Magnetostatic neighbours
	VORO->Magnetostatic_neighbours.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_MAGNEIGH_FILE >> grain >> Mag_Neigh_num;
		VORO->Magnetostatic_neighbours[grain_in_layer].resize(Mag_Neigh_num);
		for(int Mneigh=0;Mneigh<Mag_Neigh_num;++Mneigh){
			INPUT_MAGNEIGH_FILE >> VORO->Magnetostatic_neighbours[grain_in_layer][Mneigh];
		}
	}
	// Areas
	VORO->Grain_Area.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_AREA_FILE >> VORO->Grain_Area[grain_in_layer];
	}
	// Contact length
	VORO->Contact_lengths.resize(Num_Grains);
	for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
		INPUT_CL_FILE >> grain >> CL_Num;
		VORO->Contact_lengths[grain_in_layer].resize(CL_Num);
		for(int CL=0;CL<CL_Num;++CL){
			INPUT_CL_FILE >> VORO->Contact_lengths[grain_in_layer][CL];
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################################INTERACTIONS#####################################//

	total_grains = Num_Grains*Num_Layers;
	Int->Magneto_neigh_list.resize(total_grains);
	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		INPUT_INT_MN_FILE >> grain >> Mag_Neigh_num;
		Int->Magneto_neigh_list[grain_in_system].resize(Mag_Neigh_num);
		for(int Mneigh=0;Mneigh<Mag_Neigh_num;++Mneigh){
			INPUT_INT_MN_FILE >> Int->Magneto_neigh_list[grain_in_system][Mneigh];
		}
	}
	Int->Exchange_neigh_list.resize(total_grains);
	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		INPUT_INT_EN_FILE >> grain >> Neigh_num;
		Int->Exchange_neigh_list[grain_in_system].resize(Neigh_num);
		for(int neigh=0;neigh<Neigh_num;++neigh){
			INPUT_INT_EN_FILE >> Int->Exchange_neigh_list[grain_in_system][neigh];
		}
	}
	Int->Wxx.resize(total_grains); Int->Wxy.resize(total_grains);
	Int->Wxz.resize(total_grains); Int->Wyy.resize(total_grains);
	Int->Wyz.resize(total_grains); Int->Wzz.resize(total_grains);
	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		INPUT_INT_W_FILE >> grain >> W_num;
		Int->Wxx[grain_in_system].resize(W_num); Int->Wxy[grain_in_system].resize(W_num);
		Int->Wxz[grain_in_system].resize(W_num); Int->Wyy[grain_in_system].resize(W_num);
		Int->Wyz[grain_in_system].resize(W_num); Int->Wzz[grain_in_system].resize(W_num);
		for(int W=0;W<W_num;++W){
			INPUT_INT_W_FILE >> Int->Wxx[grain_in_system][W] >> Int->Wxy[grain_in_system][W]
			                 >> Int->Wxz[grain_in_system][W] >> Int->Wyy[grain_in_system][W]
			                 >> Int->Wyz[grain_in_system][W] >> Int->Wzz[grain_in_system][W];
		}
	}
	Int->H_exch_str.resize(total_grains);
	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		INPUT_INT_HEXC_FILE >> grain >> H_num;
		Int->H_exch_str[grain_in_system].resize(H_num);
		for(int H=0;H<H_num;++H){
			INPUT_INT_HEXC_FILE >> Int->H_exch_str[grain_in_system][H];
		}
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################READ IN GRAIN PARAMETERS###############################//

	total_grains = Num_Grains*Num_Layers;
	// Magnetisation and Easy axis
	Grain->m.resize(total_grains);
	Grain->Easy_axis.resize(total_grains);
	Grain->H_appl.resize(total_grains);
	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		INPUT_GPV_FILE >> Grain->m[grain_in_system].x >> Grain->m[grain_in_system].y >> Grain->m[grain_in_system].z
					  >> Grain->Easy_axis[grain_in_system].x >> Grain->Easy_axis[grain_in_system].y >> Grain->Easy_axis[grain_in_system].z
					  >> Grain->H_appl[grain_in_system].x >> Grain->H_appl[grain_in_system].y >> Grain->H_appl[grain_in_system].z;
	}
	// Temp, Vol, K, Tc, Ms, Crit exp
	Grain->Temp.resize(total_grains); Grain->Vol.resize(total_grains);
	Grain->K.resize(total_grains);    Grain->Tc.resize(total_grains);
	Grain->Ms.resize(total_grains);   Grain->Crit_exp.resize(total_grains);
	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		INPUT_GP_FILE >> Grain->Temp[grain_in_system] >> Grain->Vol[grain_in_system] >> Grain->K[grain_in_system]
		              >> Grain->Tc[grain_in_system] >> Grain->Ms[grain_in_system] >> Grain->Crit_exp[grain_in_system];
	}
	// Callen_powers
	Grain->Callen_factor_lowT.resize(total_grains);  Grain->Callen_factor_midT.resize(total_grains);
	Grain->Callen_factor_highT.resize(total_grains); Grain->Callen_power_lowT.resize(total_grains);
	Grain->Callen_power_midT.resize(total_grains);   Grain->Callen_power_highT.resize(total_grains);
	Grain->Callen_range_lowT.resize(total_grains);   Grain->Callen_range_midT.resize(total_grains);
	Grain->Callen_power_range.resize(total_grains);
	Grain->Callen_power.resize(total_grains);        Grain->Callen_power_range.resize(total_grains);

	for(int LAYER=0;LAYER<Num_Layers;++LAYER){
		int Offset = Num_Grains*LAYER;
		std::string Callen_power_range;
		INPUT_GPC_FILE >> Callen_power_range;

		if(Callen_power_range=="single"){
			for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
				int grain_in_system = Offset+grain_in_layer;
				INPUT_GPC_FILE >> Grain->Callen_power[grain_in_system];
				Grain->Callen_factor_lowT[grain_in_system] =(1.000);
				Grain->Callen_factor_midT[grain_in_system] =(1.000);
				Grain->Callen_factor_highT[grain_in_system]=(1.000);
				Grain->Callen_power_lowT[grain_in_system]  =Grain->Callen_power[grain_in_system];
				Grain->Callen_power_midT[grain_in_system]  =Grain->Callen_power[grain_in_system];
				Grain->Callen_power_highT[grain_in_system] =Grain->Callen_power[grain_in_system];
				Grain->Callen_range_lowT[grain_in_system]  =(1600.0);
				Grain->Callen_range_midT[grain_in_system]  =(2000.0);
				Grain->Callen_power_range[grain_in_system] = Callen_power_range;
			}
		}
		else if (Callen_power_range=="multiple"){
			for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
				int grain_in_system = Offset+grain_in_layer;
				INPUT_GPC_FILE >> Grain->Callen_factor_lowT[grain_in_system]  >> Grain->Callen_power_lowT[grain_in_system]
							   >> Grain->Callen_range_lowT[grain_in_system]   >> Grain->Callen_factor_midT[grain_in_system]
							   >> Grain->Callen_power_midT[grain_in_system]   >> Grain->Callen_range_midT[grain_in_system]
							   >> Grain->Callen_factor_highT[grain_in_system] >> Grain->Callen_power_highT[grain_in_system];
				Grain->Callen_power[grain_in_system]=0.0;
				Grain->Callen_power_range[grain_in_system] = Callen_power_range;
			}
		}
		else{
			std::cout << "Unknown Callen power_range" << std::endl;
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	INPUT_VORO_FILE.close();		INPUT_ST_MAT_FILE.close();
	INPUT_POS_FILE.close();		    INPUT_VERT_FILE.close();
	INPUT_GEO_FILE.close();	    	INPUT_NEIGH_FILE.close();
	INPUT_MAGNEIGH_FILE.close();	INPUT_AREA_FILE.close();
	INPUT_CL_FILE.close();			INPUT_GPV_FILE.close();
	INPUT_GPC_FILE.close();			INPUT_GP_FILE.close();
	INPUT_INT_MN_FILE.close();		INPUT_INT_EN_FILE.close();
	INPUT_INT_W_FILE.close();		INPUT_INT_HEXC_FILE.close();

	return 0;
}
