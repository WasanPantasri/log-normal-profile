/*
 * Read_back.cpp
 *
 *  Created on: 30 Jul 2019
 *      Author: Ewan Rannala
 */

/** \file Read_back.cpp
 * \brief Function used for read back simulations.*/

// TODO Test these functions

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>

#include "../hdr/Structures.hpp"
#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/Importers/Experiments/Read_back_import.hpp"

#include "../hdr/Importers/Read_in_import.hpp"
#include "../hdr/Create_system_from_output.hpp"

#include "../hdr/Data_output/HAMR_grain_output.hpp"


#include "../hdr/PixelMap.hpp"

/** Function to determine the signal obtained from reading the magnetisation of the granular system using the discretised grid system.
 * \param[in] Dx Size of read head in x-dimension.
 * \param[in] Dy Size of read head in y-dimension.
 * \param[in] Cx Centre of read head x-coordinate.
 * \param[in] Cy Centre of read head y-coordinate.
 * \param[out] Cells Pointer to 2D vector of cells that make up the grid.
 */
static double Determine_signal(const ReadHead ReadingHead,PixelMap&PixMap){

    /* First we must determine which pixels lie within the read head.
     * The method used here always determines when you have passed the boundary
     * of the read head so the previous/next pixel is the one that needs storing.
    */
    double Signal=0.0;
    unsigned int x_MIN=0, x_MAX=PixMap.getCols()-1, y_MIN=0, y_MAX=PixMap.getRows()-1;
    // Y (row) MIN
    for(unsigned int y=0;y<PixMap.getRows()-1;++y){
        if(PixMap.Access(0, y).get_Coords().second < (ReadingHead.getPosY()-ReadingHead.getSizeY()*0.5)){y_MIN = (y+1);}
        else{break;}
    }
    // Y (row) MAX
    for(unsigned int y=PixMap.getRows()-1;y>0;--y){
        if(PixMap.Access(0, y).get_Coords().second > (ReadingHead.getPosY()+ReadingHead.getSizeY()*0.5)){y_MAX = (y-1);}
        else{break;}
    }
    // x (col) MIN
    for(unsigned int x=0;x<PixMap.getCols()-1;++x){
        if(PixMap.Access(x, 0).get_Coords().first < (ReadingHead.getPosX()-ReadingHead.getSizeX()*0.5)){x_MIN=(x+1);}
        else {break;}
    }
    // X (col) MAX
    for(unsigned int x=PixMap.getCols()-1;x>0;--x){
        if(PixMap.Access(x, 0).get_Coords().first > (ReadingHead.getPosX()+ReadingHead.getSizeX()*0.5)){x_MAX = (x-1);}
        else{break;}
    }
    unsigned int Tot_cells=0;
    for(unsigned int y=y_MIN;y<=y_MAX;++y){
        for(unsigned int x=x_MIN;x<=x_MAX;++x){
            // Get grain ID
            if(PixMap.Access(x, y).get_Grain()>=0){ // Ignore any cells outside of a grain
                Signal += PixMap.Access(x, y).get_M().z;
                ++Tot_cells;
            }
        }
    }
    Signal /= Tot_cells;
    return Signal;
}

/** Function performs read back of magnetisation of a system. First a Pixel map of the system is generated
 * and then a read head is scanned along the tracks and the magnetisation is recorded. A rolling 5-point average of
 * the generated signal is created to reduce noise expected in the true signal.
 *
 * \param[in] Grain Pointer to grain data.
 * \param[in] VORO Pointer to Voronoi data.
 * \param[in] Read_data Struct containing required read head parameters.
 * \param[in] FILENAME String used as the output files name.
 */
int data_read_back(const Grain_t&Grains, const Voronoi_t&VORO, const ReadLayer ReadingLayer, const std::string FILENAME, ReadHead ReadingHead){
//######################################CREATE DISCRETISED SYSTEM##########################//
    double Cell_size=ReadingHead.getSizeX();
    PixelMap PixMap(VORO.Vx_MIN, VORO.Vy_MIN, VORO.Vx_MAX, VORO.Vy_MAX, Cell_size);
    PixMap.Discretise(VORO, Grains);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###################################READ BACK MAGNETISATION###############################//
    int Read_track=1;
    ReadingHead.setPosX(VORO.Vx_MIN+PixMap.getCellsize()*0.5);
    ReadingHead.setPosY(ReadingHead.getSizeY()*0.5+ReadingLayer.getBitSpacingY());

    std::ofstream READ_OUTPUT(("Output/" + FILENAME).c_str());

    std::vector<double> x, y, Signals;
    std::vector<int> track, Index_at_new_track;
    while(Read_track<=ReadingLayer.getBitsY()){
        while(ReadingHead.getPosX()<=VORO.Vx_MAX){
            x.push_back(ReadingHead.getPosX());
            y.push_back(ReadingHead.getPosY());
            track.push_back(Read_track);
            Signals.push_back(Determine_signal(ReadingHead, PixMap));
            ReadingHead.setPosX(ReadingHead.getPosX()+Cell_size);
        }
        ReadingHead.setPosY(ReadingHead.getPosY()+ReadingLayer.getBitSpacingY());
        ++Read_track;
        Index_at_new_track.push_back(Signals.size());
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################DETEMRINE ROLLING AVERAGE OF THE MAGNETISATION###################//
    std::vector<double> Signals_AVG_ROLL, x_AVG_ROLL;
    double AVG=0.0, AVG_x=0.0;
    int AVG_over=5;
    int sig=0;
    std::vector<int> track_index;
    for(int t=0;t<Read_track-1;++t){
        while(sig<(Index_at_new_track[t]-AVG_over)){
            int step=0;
            while(step<AVG_over){
                AVG_x += x[(sig+step)];
                AVG += Signals[(sig+step)];
                ++step;
            }
            x_AVG_ROLL.push_back(AVG_x/double(AVG_over));
            Signals_AVG_ROLL.push_back(AVG/double(AVG_over));
            AVG=0.0;AVG_x=0.0;
            ++sig;
        }
        track_index.push_back(Signals_AVG_ROLL.size());
        sig=Index_at_new_track[t];
    }
    int i=0;
    READ_OUTPUT << "Track x_avg y signal_avg x signal" << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#######################DIFFERENTIATE THE MAGNETISATION DATA##############################//
    // Differentiate the Signal data
    double SignalA=0.0, SignalB=Signals_AVG_ROLL[0];
    std::vector<double> Diff_AVG_ROLL;
    Diff_AVG_ROLL.push_back(0.0);
    for(size_t i=1;i<Signals_AVG_ROLL.size();++i){
        SignalA = Signals_AVG_ROLL[i];
        Diff_AVG_ROLL.push_back(SignalA-SignalB);
        SignalB = SignalA;
    }
    std::vector<double> Diff;
    SignalA=0.0, SignalB=Signals[0];
    for(size_t i=1;i<Signals.size();++i){
        SignalA = Signals[i];
        Diff.push_back(SignalA-SignalB);
        SignalB = SignalA;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################PRINT DATA TO OUTPUT FILE##################################//
    for(size_t t=0;t<track_index.size();++t){
        while(i<track_index[t]){
            READ_OUTPUT << std::setprecision(5) << track[i] << " "
                        << x_AVG_ROLL[i] << " " << y[i] << " "
                        << Signals_AVG_ROLL[i] << " "
                        << Diff_AVG_ROLL[i] << " "
                        << x[i] << " " << Signals[i] << " "
                        << Diff[i] << std::endl;
            ++i;
        }
        READ_OUTPUT << std::endl << std::endl;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    return 0;
}

/** Function performs read back simulation on a previously generated system using the
 *Read_in_system(const Data_input_t IN_data, Structure_t*Structure, Voronoi_t*VORO, Material_t*Materials, Grain_t*Grain, Interaction_t*Int) function.
 */
int data_read_back_from_import(ConfigFile cfg){

    std::cout << "Read head simulation using imported system" << std::endl;

    Data_input_t IN_data;
    Structure_t Structure;
    Voronoi_t VORO;
    Material_t Materials;
    Grain_t Grain;
    Interaction_t Int;
    std::string OUTPUT_FILE = "Read_back_from_input.dat";

    Read_in_import(cfg, &IN_data);
    ReadHead ReadingHead(cfg);
    ReadLayer ReadingLayer(cfg);
    Read_in_system(IN_data,&Structure,&VORO,&Materials,&Grain,&Int);
    data_read_back(Grain,VORO,ReadingLayer,OUTPUT_FILE,ReadingHead);

    return 0;
}
