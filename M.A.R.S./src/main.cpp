/* M.A.R.S. - Models of Advanced Recording Systems
 *  Created on: 23 Feb 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file main.cpp
 * \brief Prepares required values and invokes desired simulation. */

// System header files
#include <iostream>
#include <fstream>
#include <random>

// M.A.R.S specific header files.
#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/RNG/Random.hpp"

#include "../hdr/Tests/Test_analytical.hpp"
#include "../hdr/Tests/Test_hyst.hpp"
#include "../hdr/Tests/Test_boltz.hpp"

#include "../hdr/Tests/Test_LLG_timestep.hpp"
#include "../hdr/Tests/Test_LLB_mVt_multilayer.hpp"
#include "../hdr/Tests/Test_LLB_hyst_temperature_multilayer.hpp"
#include "../hdr/Tests/Test_LLB_timestep.hpp"
#include "../hdr/Tests/Test_LLB_transverse_relaxation.hpp"
#include "../hdr/Tests/Test_KMC_Rate.hpp"
#include "../hdr/Globals.hpp"

#include  "../hdr/HAMR/HAMR.hpp"
#include "../hdr/HAMR/HAMR_localised_write.hpp"
#include "../hdr/HAMR/HAMR_Write_Data.hpp"
#include "../hdr/HAMR/HAMR_Write_Data_continuous.hpp"
#include "../hdr/System_generate.hpp"
#include "../hdr/Thermoremanence.hpp"
#include "../hdr/Read_back.hpp"
#include "../hdr/Data_longevity.hpp"
#include "../hdr/FMR.hpp"

double PI, KB;
std::mt19937_64 Gen;

int main(int argc, char *argv[]){

    std::string InputcfgFile = "MARS_input.cfg";

    if(argc>1 && argc<4){
        std::string argument = argv[1];
        if(argument=="--h" || argument=="--help"){
            std::cout << "See Readme.md for configuration file details.\n";
            std::cout << "-f <input filename> To select a custom input configuration file.\n\n";
            std::cout << "Available simulations types are:\n";
            std::cout << "\tHAMR\n";
            std::cout << "\tHAMR_WRITE_LOCAL\n";
            std::cout << "\tHAMR_WRITE_DATA\n";
            std::cout << "\tHAMR_WRITE_DATA_CONT\n";
            std::cout << "\tSYSTEM_GEN\n";
            std::cout << "\tTHERMOREMANENCE\n";
            std::cout << "\tREAD_BACK\n";
            std::cout << "\tDATA_LONGEVITY\n";
            std::cout << "\tFMR\n";
            std::cout << std::flush;
            exit(0);
        }
        else if(argument=="--f"){
            if(argc<3){
                std::cout << "Missing Configuration filename." << std::endl;
                exit(0);
            } else {
                InputcfgFile=argv[2];
            }
        }
        else{
            std::cout << "Unknown argument use --h for help." << std::endl;
            std::cout << "Usage: ./MARS [--help] [--f <input filename>]" << std::endl;
            exit(0);
        }
    }
    else if(argc>3){
            std::cout << "Too many arguments.\n";
            std::cout << "Usage: ./MARS [--help] [--f <input filename>]" << std::endl;
            exit(0);
    }

	// Define constants.
	PI = 3.141592653589;
	KB = 1.3806485279e-16;

	// Read in data
	ConfigFile cfg(("Input/"+InputcfgFile).c_str());

	// Obtain desired SEED and apply it to global generators
	int SEED = cfg.getValueOfKey<int>("sim:seed");
	Gen.seed(SEED);
	mtrandom::grnd.seed(SEED);

	// Determine desired simulation
	std::string Sim_Type = cfg.getValueOfKey<std::string>("sim:type");
	if(Sim_Type=="test"){
		std::cout << "Testing... \n" << std::flush;
		std::string Test_type = cfg.getValueOfKey<std::string>("test:type");

		if(Test_type=="analytical"){analytic_test();}
        else if(Test_type=="hysteresis"){Test_hyst();}
		else if(Test_type=="boltzmann"){Test_boltz();}
		else if(Test_type=="llg-timestep"){LLG_timestep_test();}
		else if(Test_type=="llb-mvt-multilayer"){LLB_mVt_multilayer_test();}
		else if(Test_type=="llb-hysteresis-temperature-multilayer"){LLB_hyst_temperature_multilayer_test();}
		else if(Test_type=="llb-timestep"){LLB_timestep_test();}
		else if(Test_type=="llb-transverse"){LLB_trans();}
		else if(Test_type=="kmc-rate"){KMC_Rate_test();}
		else{std::cout << "Unidentified test type" << std::endl;}
	}
	else{
		if(Sim_Type=="hamr"){HAMR(cfg);}
		else if(Sim_Type=="hamr_write_local"){HAMR_localised_write(cfg);}
		else if(Sim_Type=="hamr_write_data"){HAMR_write_data(cfg);}
		else if(Sim_Type=="hamr_write_data_cont"){HAMR_write_data_cont(cfg);}
		else if(Sim_Type=="system_gen"){Gen_sys(cfg);}
		else if(Sim_Type=="thermoremanence"){Thermoremanence(cfg);}
		else if(Sim_Type=="read_back"){data_read_back_from_import(cfg);}
		else if(Sim_Type=="data_longevity"){Data_longevity(cfg);}
		else if(Sim_Type=="fmr"){FMR(cfg);}
        else{std::cout << "Unidentified simulation type" << std::endl;}
	}
	return 0;
}

