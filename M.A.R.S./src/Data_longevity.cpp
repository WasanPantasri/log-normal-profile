/*
 * Data_longevity.cpp
 *
 *  Created on: 14 Nov 2019
 *      Author: Ewan Rannala
 */

#include <string>
#include <iostream>

#include "../hdr/Structures.hpp"

#include "../hdr/Create_system_from_output.hpp"
#include "../hdr/Read_back.hpp"
#include "../hdr/Data_output/HAMR_grain_output.hpp"
#include "../hdr/Solver.h"
#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/Importers/Read_in_import.hpp"
#include "../hdr/Importers/Experiments/Read_back_import.hpp"
#include "../hdr/Importers/Experiments/Experiment_time_evolution_import.hpp"


// TODO set up the SNR simulation!


int Data_longevity(const ConfigFile cfg){
	Data_input_t IN_DATA;
	Expt_data_read_t Read_data;
	Structure_t Structure;
	Voronoi_t VORO;
	Material_t Materials;
	Grain_t Grain;
	Interaction_t INT;
	SNR_t SNR_data;

//#########################################################################
// WHAT INPUTS ARE NEEDED FOR THIS SIMULATION???
	/*	Total_snaps
	 * 	Snap times
	 *
	 *
	 *
	 *
	 *
	 */
	int Total_snaps=5;
	std::vector<double> Time_limit;
//#########################################################################
	double Time=0.0;

	Time_limit.resize(Total_snaps);
	double temp=3600;	// Temporary value for setting time_limits
	for(size_t i=0;i<Time_limit.size();++i){
		Time_limit.at(i)=temp;
		temp *= 10;
	}
	// 1H, 10H, 4D4H, 41D16H, 416D16H

	std::string FILENAME_idv = "0.dat";
	std::string Read_back_FILENAME = "Read_back_Img_0.dat";

	// Importers
	Time_evol_import(cfg, &SNR_data);
	Read_in_import(cfg, &IN_DATA);
	Read_Back_import(cfg, &Read_data);

	// Want to read in a system
	Read_in_system(IN_DATA,&Structure,&VORO,&Materials,&Grain,&INT);

	Solver_t Solver(cfg);

	// Perform data read back
//	data_read_back(&Grain,&VORO,Read_data,Read_back_FILENAME);
	// Output system image
	Idv_Grain_output(Structure.Num_layers,VORO,Grain,Time,FILENAME_idv);



	// Loop over each required snapshot
	for(int snapshot=1;snapshot<=Total_snaps;++snapshot){
		// Select time_limit from input
		int snap_time = Time_limit.at(snapshot-1);
		Solver.set_dt(snap_time/100); // Ensure 100 steps - automatically set timestep

		std::cout << "Evolving system over " << snap_time << " seconds." << std::endl;
		Read_back_FILENAME = "Read_back_Img_" + std::to_string(snapshot) + ".dat";
		FILENAME_idv = std::to_string(snapshot) + ".dat";
		// Perform kMC until reach limit time
		while(Time<snap_time){
			Solver.Integrate(&VORO,Structure.Num_layers,&INT,0.0,0.0,&Grain);
			Time += Solver.get_dt();
		}
//		data_read_back(&Grain,&VORO,Read_data,Read_back_FILENAME);
		Idv_Grain_output(Structure.Num_layers,VORO,Grain,Time,FILENAME_idv);
		Time = 0.0; // Reset Time
/*
 *
 * Need to remove time reset and just set time to continue
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
	}
	return 0;
}
