/*
 * Test_KMC_HYST.cpp
 *
 *  Created on: 27 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file Test_KMC_HYST.cpp
 * \brief Test simulation for kMC solver. Determines coercivity as a function of sweep rate. */

#include <iostream>
#include <fstream>
#include "math.h"

#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/KMC_Solver.hpp"
#include "../../hdr/Globals.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"

// Used to determine sign of value.
template <typename T>
int sign(T Val){
	if(Val < 0){return -1;}
	else if(Val > 0){return 1;}
	else{return 0;}
}

template <typename Q>
bool Sign_change(Q Val1, Q Val2){
	if(sign(Val1)!=0 || sign(Val2)!=0){
		if(sign(Val1)*sign(Val2)==-1){return true;}
		else{return false;}
	}
	else{return false;}
}

double DotP(const Vec3 VectorA, const Vec3 VectorB){
	double DOT;
	DOT = VectorA.x*VectorB.x + VectorA.y*VectorB.y + VectorA.z*VectorB.z;
	return DOT;
}

int KMC_Rate_test(){
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Materials;
	Interaction_t Int_system;
	KMC_t KMC_data;
	Grain_t Grain;
	int Inter_steps = 100;
	double Phi_H=0.0, Theta_H=1e-3, time=0.0, H_delta = 50, Avg_K=0.0, Avg_Ms=0.0, Avg_Vol=0.0, mDOTh_A=0.0, mDOTh_B=0.0;
	Vec3 H_appl_unit, Avg_m, Avg_e;
	std::ofstream KMC_Output("Tests/KMC_Rate/Output/KMC_Hyst.dat");
	std::ofstream KMC_Hc_Output("Tests/KMC_Rate/Output/KMC_Hc.dat");

	KMC_Output << "mx my mz Hx Hy Hz Ex Ey Ez H_MAG Hk m.H Temp Rate time\n";
	KMC_Hc_Output << "Rate Temp Hc_left Hc_right error\n";

	std::cout << "Performing Hyst(R,T) test for KMC..." << std::endl;

//###########TEST PARAMETERS###########//

	Structure.Dim_x = 150.0;
	Structure.Dim_y = 150.0;
	Structure.Num_layers= 1;
	Structure.Grain_width = 8.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Magneto_Interaction_Radius = 0.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Materials.Mag_Type.push_back("assigned");
	Materials.Initial_mag.resize(Structure.Num_layers);
	Materials.z.push_back(0.0);
	for(unsigned int i=0;i<Structure.Num_layers;++i){
		Materials.Initial_mag[i].x=0.0;
		Materials.Initial_mag[i].y=0.0;
		Materials.Initial_mag[i].z=1.0;

		Materials.Easy_axis_polar.push_back(0.0);
		Materials.Easy_axis_azimuth.push_back(0.0);
		Materials.Anis_angle.push_back(0.0);
		Materials.dz.push_back(10);
 		if(i>0){Materials.z.push_back(Materials.z[i-1]+(Materials.dz[i-1]+Materials.dz[i])*0.5);}
		Materials.Ms.push_back(800.0);
		Materials.Tc_Dist_Type.push_back("normal");
		Materials.Avg_Tc.push_back(750);
		Materials.StdDev_Tc.push_back(0.0);
		Materials.K_Dist_Type.push_back("normal");
		Materials.Avg_K.push_back(5.0e+6);
		Materials.StdDev_K.push_back(0.0);
		Materials.J_Dist_Type.push_back("normal");
		Materials.StdDev_J.push_back(0.0);
		Materials.H_sat.push_back(0.0);
		Materials.Ani_method.push_back("callen");
		Materials.Callen_power_range.push_back("single");
		Materials.Callen_power.push_back(3.0);
		Materials.Callen_factor_lowT.push_back (1.000);
		Materials.Callen_factor_midT.push_back (1.000);
		Materials.Callen_factor_highT.push_back(1.000);
		Materials.Callen_power_lowT.push_back  (3.0);
		Materials.Callen_power_midT.push_back  (3.0);
		Materials.Callen_power_highT.push_back (3.0);
		Materials.Callen_range_lowT.push_back  (1600.0);
		Materials.Callen_range_midT.push_back  (2000.0);
		Materials.mEQ_Type.push_back("bulk");
		Materials.a0_mEQ.push_back(0.0);
		Materials.a1_mEQ.push_back(0.0);
		Materials.a2_mEQ.push_back(0.0);
		Materials.a3_mEQ.push_back(0.0);
		Materials.a4_mEQ.push_back(0.0);
		Materials.a5_mEQ.push_back(0.0);
		Materials.a6_mEQ.push_back(0.0);
		Materials.a7_mEQ.push_back(0.0);
		Materials.a8_mEQ.push_back(0.0);
		Materials.a9_mEQ.push_back(0.0);
		Materials.a1_2_mEQ.push_back(0.0);
		Materials.b1_mEQ.push_back(0.0);
		Materials.b2_mEQ.push_back(0.0);
		Materials.Crit_exp.push_back(0.0);
	}

	KMC_data.ZeroKInputs = false;

	double Temp=300.0;

//#####################################//
	Voronoi(Structure,Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temp,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);
	// Save initial mag for backup
	std::vector<Vec3> MAG_BACK(Voronoi_data.Num_Grains);
	for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
		MAG_BACK[grain].x = Grain.m[grain].x;
		MAG_BACK[grain].y = Grain.m[grain].y;
		MAG_BACK[grain].z = Grain.m[grain].z;
	}

	H_appl_unit.x = sin(Theta_H*PI/180.0)*cos(Phi_H*PI/180.0);
	H_appl_unit.y = sin(Theta_H*PI/180.0)*sin(Phi_H*PI/180.0);
	H_appl_unit.z = cos(Theta_H*PI/180.0);

	for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
		Avg_K  += Grain.K[grain];
		Avg_Ms += Grain.Ms[grain];
		Avg_Vol += Grain.Vol[grain];  // Volume is in nm^3
	}
	Avg_K /= Voronoi_data.Num_Grains;
	Avg_Ms /= Voronoi_data.Num_Grains;
	Avg_Vol /= Voronoi_data.Num_Grains;

	//double H_k_MAG = ((2.0*Grain.K[0])/Grain.Ms[0]);
	double H_k_MAG = (2.0*Avg_K)/Avg_Ms;
	double H_appl_MAG = 1.5*H_k_MAG;
	std::cout << "\nVol: " << Avg_Vol << " K: " << Avg_K << " erg/cc\nMs: " << Avg_Ms << " emu/cc\nHk: " << H_k_MAG << " Oe\nHappl: " << H_appl_MAG << " Oe" << std::endl;


	for(double Temp=100;Temp<800;Temp+=100){
		H_appl_MAG = 1.5*H_k_MAG;
		std::cout << "Temperature = " << Temp
				  << "\tKV/kT = " << (Avg_K*Avg_Vol*1e-21)/(Temp*KB) << std::endl;
			for(double Rate=1;Rate<1e+15;Rate*=10){
				std::cout << "Rate = " << Rate << std::endl;
				KMC_data.time_step = H_delta/(Rate*Inter_steps);

			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Grain.Temp[grain]=Temp;
				Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG;
				Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG;
				Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG;
			}

			// INITIAL OUTPUT TO FILE
			Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Avg_m.x += Grain.m[grain].x/Voronoi_data.Num_Grains;
				Avg_m.y += Grain.m[grain].y/Voronoi_data.Num_Grains;
				Avg_m.z += Grain.m[grain].z/Voronoi_data.Num_Grains;
				Avg_e.x += Grain.Easy_axis[grain].x/Voronoi_data.Num_Grains;
				Avg_e.y += Grain.Easy_axis[grain].y/Voronoi_data.Num_Grains;
				Avg_e.z += Grain.Easy_axis[grain].z/Voronoi_data.Num_Grains;
			}
			KMC_Output << Avg_m << " " << H_appl_unit << " " << Avg_e << " "
					   << H_appl_MAG << " " << H_k_MAG << " "
					   << DotP(Avg_m,H_appl_unit) << " " << Temp << " "
					   << Rate << " " << time << "\n" << std::flush;

			while(H_appl_MAG>=(-1.5*H_k_MAG)){	// First half of loop
				H_appl_MAG-=(H_delta/Inter_steps);
				std::cout << H_appl_MAG << "\r";
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG;
					Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG;
					Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG;
				}
				KMC_solver(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,KMC_data.time_step,KMC_data.ZeroKInputs,false,1.0e9,&Grain);
				time += KMC_data.time_step;

				// Average over the entire system then write out
				Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Avg_m.x += Grain.m[grain].x/Voronoi_data.Num_Grains;
					Avg_m.y += Grain.m[grain].y/Voronoi_data.Num_Grains;
					Avg_m.z += Grain.m[grain].z/Voronoi_data.Num_Grains;
					Avg_e.x += Grain.Easy_axis[grain].x/Voronoi_data.Num_Grains;
					Avg_e.y += Grain.Easy_axis[grain].y/Voronoi_data.Num_Grains;
					Avg_e.z += Grain.Easy_axis[grain].z/Voronoi_data.Num_Grains;
				}
				mDOTh_A = DotP(Avg_m,H_appl_unit);
				// OUTPUT TO FILE
				KMC_Output << Avg_m << " " << H_appl_unit << " "<< Avg_e << " "
						   << H_appl_MAG << " " << H_k_MAG << " " << mDOTh_A << " "
						   << Temp << " " << Rate << " " << time << "\n" << std::flush;
				if(Sign_change(mDOTh_A, mDOTh_B)){
					KMC_Hc_Output << Rate << " " << Temp << " " << H_appl_MAG << " ";
				}
				mDOTh_B = mDOTh_A;
			}
			while(H_appl_MAG<=(1.5*H_k_MAG)){	// Second half of loop
				std::cout << H_appl_MAG << "\r";
				H_appl_MAG+=(H_delta/Inter_steps);
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG;
					Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG;
					Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG;
				}

				KMC_solver(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,KMC_data.time_step,KMC_data.ZeroKInputs,false,1.0e9,&Grain);
				time += KMC_data.time_step;
				// Average over the entire system then write out
				Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Avg_m.x += Grain.m[grain].x/Voronoi_data.Num_Grains;
					Avg_m.y += Grain.m[grain].y/Voronoi_data.Num_Grains;
					Avg_m.z += Grain.m[grain].z/Voronoi_data.Num_Grains;
					Avg_e.x += Grain.Easy_axis[grain].x/Voronoi_data.Num_Grains;
					Avg_e.y += Grain.Easy_axis[grain].y/Voronoi_data.Num_Grains;
					Avg_e.z += Grain.Easy_axis[grain].z/Voronoi_data.Num_Grains;
				}
				mDOTh_A = DotP(Avg_m,H_appl_unit);
				// OUTPUT TO FILE
				KMC_Output << Avg_m << " " << H_appl_unit << " " << Avg_e << " "
					   	  	  << H_appl_MAG << " " << H_k_MAG << " "
					   	  	  << DotP(Avg_m,H_appl_unit) << " " << Temp << " "
					   	  	  << Rate << " " << time << "\n" << std::flush;
				if(Sign_change(mDOTh_A, mDOTh_B)){
					KMC_Hc_Output << H_appl_MAG << " " << (H_delta/Inter_steps) << std::endl;
				}
				mDOTh_B = mDOTh_A;
			}
			time = 0.0;
			KMC_Output << "\n\n";
			Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temp,&Grain);
			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Grain.m[grain].x = MAG_BACK[grain].x;
				Grain.m[grain].y = MAG_BACK[grain].y;
				Grain.m[grain].z = MAG_BACK[grain].z;
		}	}
		KMC_Hc_Output << "\n\n";
	}
	KMC_Output.close();
	KMC_Hc_Output.close();
	return 0;
}
