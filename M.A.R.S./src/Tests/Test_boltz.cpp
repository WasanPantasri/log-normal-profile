/* Test_boltz.cpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Test_boltz.cpp
 * \brief Test simulation for LLB. Compares Boltzmann distributions with analytical solutions. */

// System header files
#include <fstream>
#include <iostream>
#include "math.h"
#include <chrono>

// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/Materials_import_Test_layers.hpp"
#include "../../hdr/Solver.h"

int Test_boltz(){
	Structure_t Structure;
	Material_t Material;
	Voronoi_t Voronoi_data;
	Interaction_t Int_Mat;
	Grain_t Grain, Grain_BACKUP;
    std::vector<ConfigFile> Materials_Config;
    std::fstream OUTPUT;
    std::string OUTPUT_pre;
    double run_time=0.0;
    int Num_temps=0;

	const ConfigFile TEST_CFG_FILE("Tests/Boltzmann/test_parameters.cfg");
    const std::string MAT_FILE_LOC("Tests/Boltzmann");
	Structure_import(TEST_CFG_FILE, &Structure);
	Structure.Num_layers=1;
	Materials_import_Test_layers(MAT_FILE_LOC,Structure.Num_layers,&Materials_Config,&Material);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,&Grain);
    Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);
    for(unsigned int grains=0;grains<Voronoi_data.Num_Grains;++grains){
        Grain.H_appl[grains]=0.0;
    }
    Solver_t Solver(Structure.Num_layers,TEST_CFG_FILE,Materials_Config);


    std::cout << "Performing Boltzmann test using the " << std::flush;

    // Set up output file
    switch(Solver.get_Solver())
    {
    case Solvers::kMC:
        std::cout << "kMC solver." << std::endl;
        std::cout << "Skipping test." << std::endl;
        return 0;
    case Solvers::LLG:
        std::cout << "LLG solver." << std::endl;
        Material.Ani_method[0]="callen";
        OUTPUT_pre = "Tests/Boltzmann/Output/LLG_";
        run_time=1.0e-6;
        Num_temps=4;
        for(unsigned int grains=0;grains<Voronoi_data.Num_Grains;++grains){
            Grain.m[grains].x=0.0;
            Grain.m[grains].y=0.0;
            Grain.m[grains].z=1.0;
        }
        break;
    case Solvers::LLB:
        std::cout << "LLB solver." << std::endl;
        OUTPUT_pre = "Tests/Boltzmann/Output/LLB_";
        run_time = 1.0e-9;
        Num_temps=6;
        for(unsigned int grains=0;grains<Voronoi_data.Num_Grains;++grains){
            Grain.m[grains].x=1.0;
            Grain.m[grains].y=0.0;
            Grain.m[grains].z=0.0;
        }
        break;
    }
    Grain_BACKUP = Grain;
	for(int Temps=1;Temps<=Num_temps;++Temps){
		double Temperature;
		std::vector<double> mx, my, mz;
		double time=0.0, m_theta=0.0, Mag=0.0, Mag_Avg=0.0;

		if(Temps==1){Temperature=900;}
		else if(Temps==2){Temperature=500;}
		else if(Temps==3){Temperature=300;}
        else if(Temps==4){Temperature=50;}
		else if(Temps==5){Temperature=800;}
		else if(Temps==6){Temperature=750;}

		std::cout << "Running for " << Temperature << " K...\nUsing " << Material.Ani_method[0] << " method." << std::endl;

		OUTPUT.open ((OUTPUT_pre+std::to_string(Temperature)+"K.dat").c_str(), std::fstream::out);
		OUTPUT << "theta_m frequency bin_width total_values Boltzmann Normaliser\n";

		int steps=0,rep=0;
		auto start = std::chrono::system_clock::now();
		unsigned int N = 2000, Bin_pos;
		double dL = PI/N;
		std::vector<unsigned int> Bin(N,0);

        for(unsigned int grains=0;grains<Voronoi_data.Num_Grains;++grains){
            Grain.Temp[grains] = Temperature;
        }

		while(time<run_time){
			Mag_Avg = 0.0;
			std::cout << "Time: " << time << "\r";
			Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_Mat,0.0,0.0,&Grain);
			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Mag = sqrt(Grain.m[grain]*Grain.m[grain]);
				m_theta = acos(Grain.m[grain].z/Mag);
				Bin_pos = static_cast<int>(m_theta/dL);
				++Bin[Bin_pos];
				Mag_Avg += Mag;
			}
			// Divide average by grains
			Mag_Avg /= Voronoi_data.Num_Grains;
			time += Solver.get_dt();
			++steps; ++rep;
			if(rep==1e6){	// Check time taken for 1e6 steps in order to predict total time required per temperature.
				auto end = std::chrono::system_clock::now();
				std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
				long int Iterations = run_time/Solver.get_dt();
				double predicted_Sim_time = Iterations*elapsed.count()/1e6;
				std::cout << "\n###############################################################################" << std::endl;
				std::cout << "Predicted run time per temperature: " << predicted_Sim_time/60 << " min + Binning time" << std::endl;
				std::cout << "###############################################################################" << std::endl;

			}
		}

        // Determine Callen-Callen region for K(T) determination
        double K_as_T=0.0;
        if (Temperature <= Grain.Callen_range_lowT[0]){
            K_as_T=Grain.K[0]*Grain.Callen_factor_lowT[0] * pow(Solver.get_mEQ(0),Grain.Callen_power_lowT[0]);
        }
        else if (Temperature > Grain.Callen_range_midT[0]){
            K_as_T=Grain.K[0]*Grain.Callen_factor_highT[0] * pow(Solver.get_mEQ(0),Grain.Callen_power_highT[0]);
        } else {
            K_as_T=Grain.K[0]*Grain.Callen_factor_midT[0] * pow(Solver.get_mEQ(0),Grain.Callen_power_midT[0]);
        }

        double Boltz_param=0.0, m_sq=0.0;
        // Determine FREE ENERGY
        m_sq = Mag_Avg*Mag_Avg;
        double F_ani = m_sq/(2.0*Solver.get_ChiPerp(0));
        Boltz_param  = F_ani*Grain.Ms[0]*Grain.Vol[0]*1e-21;
        Boltz_param /= KB*Temperature;
        Boltz_param *= -1.0;


		// Generate Boltzmann and determine maximums for data and Boltzmann.
		std::vector<double> Boltz_data;
		double MAX_BOLTZ=0.0;
		unsigned int MAX_HIST=0;
		for(unsigned int count=0;count<N;count++){
			double Boltz=0.0;
	        switch(Solver.get_Solver())
	        {
	        case Solvers::LLG:
                Boltz = sin(count*dL)*exp(-(K_as_T*Grain.Vol[0]*1.0e-21*sin(count*dL)*sin(count*dL))/(1.38e-16*Temperature));
	            break;
	        case Solvers::LLB:
	            if(Material.Ani_method[0]=="callen"){
	                Boltz = sin(count*dL)*exp(-(K_as_T*Grain.Vol[0]*1.0e-21*sin(count*dL)*sin(count*dL))/(1.38e-16*Temperature));
	            }
	            else{Boltz = sin(count*dL)*m_sq*exp(Boltz_param*sin(count*dL)*sin(count*dL));}
	            break;
	        default:
	            return 0;
	        }
			Boltz_data.push_back(Boltz);
			if(Boltz>MAX_BOLTZ){MAX_BOLTZ=Boltz;}
			unsigned int Hist = Bin[count];
			if(Hist>MAX_HIST){MAX_HIST=Hist;}
		}
		// Normalise to ensure MAX(Boltzmann)==Maximum probability density.
		double Nomraliser = MAX_BOLTZ/(MAX_HIST/(steps*dL));
		for(unsigned int count=0;count<N;count++){
			OUTPUT << count*dL*180.0/PI << " " << Bin[count] << " " << dL*180.0/PI << " " << steps*dL << " " << Boltz_data[count] << " " << Nomraliser << std::endl;
		}
		OUTPUT.close();
		std::cout << "done." << std::endl;
		// Reset grains to initial state
        Grain = Grain_BACKUP;
	}
	return 0;
}


