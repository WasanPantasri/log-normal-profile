/* Test_LLG_timestep.cpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Test_LLG_timestep.cpp
 * \brief Test simulation for LLG. Determines maximum viable timestep by generating Boltzmann distributions for multiple values of dt. */


// System header files
#include <fstream>
#include <iostream>
#include "math.h"

// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/LLG.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"


int LLG_timestep_test(){
	Structure_t Structure;
	Material_t Material;
	Voronoi_t Voronoi_data;
	Interaction_t Int_Mat;
	LLG_t LLG_data;
	Grain_t Grain;

	std::cout << "Performing LLG time-step test... \n" << std::flush;

// Generate system parameters
	Structure.Dim_x = 50.0;
	Structure.Dim_y = 50.0;
	Structure.Grain_width = 8.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Num_layers = 1;
	Structure.Magneto_Interaction_Radius = 0.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Material.Mag_Type.push_back("assigned");
	Material.Initial_mag.resize(1);
	Material.Initial_mag[0].x=0.0;
	Material.Initial_mag[0].y=0.0;
	Material.Initial_mag[0].z=1.0;
	Material.Easy_axis_polar.push_back(0.0);
	Material.Easy_axis_azimuth.push_back(0.0);
	Material.Anis_angle.push_back(0.0);
	Material.z.push_back(0.0);
	Material.dz.push_back(10);
	Material.Ms.push_back(1200);
	Material.K_Dist_Type.push_back("normal");
	Material.Avg_K.push_back(4.2e+6);
	Material.StdDev_K.push_back(0.0);
	Material.Tc_Dist_Type.push_back("normal");
	Material.Avg_Tc.push_back(700);
	Material.StdDev_Tc.push_back(0.0);
	Material.J_Dist_Type.push_back("normal");
	Material.StdDev_J.push_back(0.0);
	Material.H_sat.push_back(0);
	Material.Ani_method.push_back("callen");
	Material.Callen_power_range.push_back("single");
	Material.Callen_power.push_back(3.0);
	Material.Callen_factor_lowT.push_back (1.000);
	Material.Callen_factor_midT.push_back (1.000);
	Material.Callen_factor_highT.push_back(1.000);
	Material.Callen_power_lowT.push_back  (3.0);
	Material.Callen_power_midT.push_back  (3.0);
	Material.Callen_power_highT.push_back (3.0);
	Material.Callen_range_lowT.push_back  (1600.0);
	Material.Callen_range_midT.push_back  (2000.0);
	Material.mEQ_Type.push_back("bulk");
	Material.a0_mEQ.push_back(0.0);
	Material.a1_mEQ.push_back(0.0);
	Material.a2_mEQ.push_back(0.0);
	Material.a3_mEQ.push_back(0.0);
	Material.a4_mEQ.push_back(0.0);
	Material.a5_mEQ.push_back(0.0);
	Material.a6_mEQ.push_back(0.0);
	Material.a7_mEQ.push_back(0.0);
	Material.a8_mEQ.push_back(0.0);
	Material.a9_mEQ.push_back(0.0);
	Material.a1_2_mEQ.push_back(0.0);
	Material.b1_mEQ.push_back(0.0);
	Material.b2_mEQ.push_back(0.0);
	Material.Crit_exp.push_back(0.0);

	LLG_data.Alpha.resize(1);
	LLG_data.Alpha[0] = 0.1;
	LLG_data.dt=1.0e-13;
	LLG_data.Gamma=1.7e+7;

	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);

	for(int Temps=1;Temps<=10;++Temps){
		if(Temps==1){LLG_data.dt=1e-11;}
		else if(Temps==2){LLG_data.dt=9e-12;}
		else if(Temps==3){LLG_data.dt=8e-12;}
		else if(Temps==4){LLG_data.dt=7e-12;}
		else if(Temps==5){LLG_data.dt=6e-12;}
		else if(Temps==6){LLG_data.dt=5e-12;}
		else if(Temps==7){LLG_data.dt=4e-12;}
		else if(Temps==8){LLG_data.dt=3e-12;}
		else if(Temps==9){LLG_data.dt=2e-12;}
		else if(Temps==10){LLG_data.dt=1e-12;}

		double Temperature=300.0;
		std::vector<double> mx, my, mz;
		std::vector<double> m_theta;
		std::vector<double> Mag (Voronoi_data.Num_Grains);
		std::vector<double> m_theta_AVG, Mag_Avg;
		double time=0.0;

		std::cout << "Running for " << LLG_data.dt*1e12 << "ps..." << std::endl;

		std::ofstream LLG_OUTPUT("Tests/LLG_timestep/Output/LLG"+std::to_string(Temperature)+"K_"+std::to_string(LLG_data.dt*1e12)+"ps.dat");
		LLG_OUTPUT << "theta_m frequency bin_width total_values Boltzmann Normaliser\n";

		Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temperature,&Grain);
		Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);

		// SETUP APPLIED FIELD
		for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
			Grain.H_appl[grain].x=0.0;
			Grain.H_appl[grain].y=0.0;
			Grain.H_appl[grain].z=0.0;
		}
		// RUN LLG
		int steps=0;
		double m_theta_MAX=0.0;
		while(time<1e-6){
			m_theta_AVG.push_back(0.0); // Set average to zero
			Mag_Avg.push_back(0.0);
			std::cout << "Time: " << time << "\r";
			LL_G(Voronoi_data.Num_Grains,Structure.Num_layers,Int_Mat,Voronoi_data,LLG_data,false,&Grain);

			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Mag[grain] = sqrt(Grain.m[grain].x*Grain.m[grain].x + Grain.m[grain].y*Grain.m[grain].y + Grain.m[grain].z*Grain.m[grain].z);
				m_theta.push_back(acos(Grain.m[grain].z/Mag[grain]));
				Mag_Avg.back() += Mag[grain]; // Add m_theta to average
				if(m_theta.back() > m_theta_MAX){m_theta_MAX=m_theta.back();}
			}
			Mag_Avg.back() /= Voronoi_data.Num_Grains;
			time += LLG_data.dt;
			steps++;
		}
		// BIN MAGNETISATION
		double L = m_theta_MAX;
		int N = 2000;
		double dL = L/N;
		std::vector<int> Bin(N);
		for(int i=0;i<N;i++){
			Bin[i] = 0;
		}
		int Bin_pos=0;
		unsigned int i=0;
		while(i<steps*Voronoi_data.Num_Grains){
			Bin_pos = (m_theta[i]/dL);
			Bin[Bin_pos]++;
			i++;
		}
		std::vector<double> Boltz_data;
		double MAX_BOLTZ=0.0, MAX_HIST=0.0;
		// Generate Boltzmann and determine maximums for data and Boltzmann.
		for(int count=0;count<N;count++){
			//boltz(x,K,V,T) =sin(x)*exp(-(K*V*sin(x)*sin(x))/(1.38e-16*T))
			double Boltz = sin(count*dL)*exp(-(Material.Avg_K[0]*Grain.Vol[0]*1.0e-21*sin(count*dL)*sin(count*dL))/(1.38e-16*Temperature));
			Boltz_data.push_back(Boltz);
			if(Boltz>MAX_BOLTZ){MAX_BOLTZ=Boltz;}
			double Hist = Bin[count];
			if(Hist>MAX_HIST){MAX_HIST=Hist;}
		}
		// Normalise to ensure MAX(Boltzmann)==Maximum probability density.
		double Nomraliser = MAX_BOLTZ/(MAX_HIST/(steps*dL));
		for(int count=0;count<N;count++){
			LLG_OUTPUT << count*dL << " " << Bin[count] << " " << dL << " " << steps*dL << " " << Boltz_data[count] << " " << Nomraliser << std::endl;
		}
		LLG_OUTPUT.close();
		std::cout << std::endl << std::endl;
	}
	return 0;
}


