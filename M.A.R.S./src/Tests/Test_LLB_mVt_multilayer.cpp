/* Test_LLB_mVt_multilayer.cpp
 *  Created on: 2 Feb 2020
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

// System header files
#include <fstream>
#include <iostream>
#include "math.h"

// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/LLB.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/LLB_import.hpp"
#include "../../hdr/Importers/Materials_import_Test_layers.hpp"


int LLB_mVt_multilayer_test(){
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Material;
	Interaction_t Int_Mat;
	LLB_t LLB_data;
	Grain_t Grain;
	std::vector<Vec3> Magnetisation;

	std::cout << "Performing LLB mVt multilayer test... \n" << std::flush;

	std::ofstream LLB_OUTPUT("Tests/LLB_mVt_multilayer/Output/LLB_mVt.dat");
	const ConfigFile CONFIG_FILE("Tests/LLB_mVt_multilayer/LLB_test_parameters.cfg");
	std::vector<ConfigFile> Materials_Config;
    const std::string MAT_FILE_LOC="Tests/LLB_mVt_multilayer";


	// Generate system parameters
	double Temperature=0.0;
	// Import structure parameters
	Structure_import(CONFIG_FILE, &Structure);
	// Import material parameters
	Materials_import_Test_layers(MAT_FILE_LOC, Structure.Num_layers, &Materials_Config, &Material);
	// Import time step and damping
	LLB_import(CONFIG_FILE, Materials_Config, Structure.Num_layers, &LLB_data);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temperature,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);

	//-------- Start writing heading of output file ----------------//
	LLB_OUTPUT << "mx my mz ml <mx> <my> <mz> <ml> Temp> <Tc>";
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){LLB_OUTPUT << " Xpara" << layer+1 << " Xperp" << layer+1;}
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){LLB_OUTPUT << " meq" << layer+1;}
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){LLB_OUTPUT << " mx" << layer+1 << " my" << layer+1 << " mz" << layer+1 << " ml" << layer+1 << "";}
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){LLB_OUTPUT << " <mx" << layer+1 << "> <my" << layer+1 << "> <mz" << layer+1 << "> <ml" << layer+1 << ">";}
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){LLB_OUTPUT << " <Xx" << layer+1 << "> <Xy" << layer+1 << "> <Xz" << layer+1 << "> <Xl" << layer+1 << ">";}
	LLB_OUTPUT << "\n";
	//-------- Finish writing heading of output file ----------------//

	double Mag_length_thresh=1e-9;
	double Mag_length_equil_thresh=1e-8;
	double Temp_incr = 10.0;
	double Mx,My,Mz,Xpara,Xperp=0.0;
	double Mx_time,My_time,Mz_time;
//	double Xpara_time,Xperp_time=0.0;
	Vec3 Applied_Field;
	// Create structures to store layer magnetisation components and set them to zero
	std::vector<double> Magnetisation_layers_x;
	std::vector<double> Magnetisation_layers_y;
	std::vector<double> Magnetisation_layers_z;
	std::vector<double> Magnetisation_layers_l;
	Magnetisation_layers_x.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_y.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_z.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_l.resize(Structure.Num_layers,0.0);
	std::vector<double> Ms_layers;
	Ms_layers.resize(Structure.Num_layers,0.0);
	// Time average values
	std::vector<double> Magnetisation_layers_time_x;
	std::vector<double> Magnetisation_layers_time_y;
	std::vector<double> Magnetisation_layers_time_z;
	std::vector<double> Magnetisation_layers_time_l;
	Magnetisation_layers_time_x.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_time_y.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_time_z.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_time_l.resize(Structure.Num_layers,0.0);
	std::vector<double> Ms_layers_time;
	Ms_layers_time.resize(Structure.Num_layers,0.0);
	// Create layer susceptibility vectors and set them to zero
	std::vector<double> Chi_layers_x;
	std::vector<double> Chi_layers_y;
	std::vector<double> Chi_layers_z;
	std::vector<double> Chi_layers_l;
	Chi_layers_x.resize(Structure.Num_layers,0.0);
	Chi_layers_y.resize(Structure.Num_layers,0.0);
	Chi_layers_z.resize(Structure.Num_layers,0.0);
	Chi_layers_l.resize(Structure.Num_layers,0.0);
	std::vector<double> Chi_prefactor_layers;
	Chi_prefactor_layers.resize(Structure.Num_layers,0.0);


	std::cout << "\n\n\nPerforming LLB MvsT test for multilayer structure ... \n" << std::flush;

	// Set applied field to zero
	Applied_Field.x = 0.0; //Expt_field.H_appl_unit.x*Expt_field.H_appl_MAG_max;
	Applied_Field.y = 0.0; //Expt_field.H_appl_unit.y*Expt_field.H_appl_MAG_max;
	Applied_Field.z = 0.0; //Expt_field.H_appl_unit.z*Expt_field.H_appl_MAG_max;

	for(unsigned int j=0;j<Structure.Num_layers;++j){
		const int offset = j*Voronoi_data.Num_Grains;
		for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
			Grain.H_appl[offset+i].x = Applied_Field.x;
			Grain.H_appl[offset+i].y = Applied_Field.y;
			Grain.H_appl[offset+i].z = Applied_Field.z;
		}
	}

	// Loop over grains to determine max Tc
	double Tc_max = Grain.Tc[0];
	double Tc_min = Grain.Tc[0];
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
		const int Offset = Voronoi_data.Num_Grains*layer;
		for (unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
			if(Grain.Tc[i+Offset] > Tc_max){
				Tc_max = Grain.Tc[i+Offset];
			}
			else if(Grain.Tc[i+Offset] < Tc_min){
				Tc_min = Grain.Tc[i+Offset];
			}
		}
	}
	std::cout << "\n\t***\tMax Tc = " << Tc_max << std::endl;
	std::cout << "\n\t***\tMin Tc = " << Tc_min << std::endl;

	std::cout << "Performing M vs. T test for multilayer system..." << std::endl;
	double Mag_length_time_AVG=0.0;
	double Mag_length = 0.0, Mag_length_AVG = 0.0, Mag_length_1 = 0.0, Mag_length_1AVG = 0.0, Mag_length_2AVG = 0.0;
	double Ms_AVG = 0.0, Vol_AVG = 0.0, Temp_AVG = 0.0, Tc_AVG = 0.0;

	//---------------------------------//
	//----- Loop over temperature -----//
	//---------------------------------//
	int Rep_counter=0;
	for(int Temp=0;Temp<Tc_max+200;Temp+=Temp_incr){
		Temp_incr=10;
		std::cout << Temp << "K\n";

		// Set mag vector to zero
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			const int Offset = Voronoi_data.Num_Grains*layer;
			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Grain.Temp[grain+Offset] = double(Temp);
			}
			Magnetisation_layers_x[layer] = 0.0;
			Magnetisation_layers_y[layer] = 0.0;
			Magnetisation_layers_z[layer] = 0.0;
			Magnetisation_layers_l[layer] = 0.0;
			Ms_layers[layer] = 0.0;
			Magnetisation_layers_time_x[layer] = 0.0;
			Magnetisation_layers_time_y[layer] = 0.0;
			Magnetisation_layers_time_z[layer] = 0.0;
			Magnetisation_layers_time_l[layer] = 0.0;
			Ms_layers_time[layer] = 0.0;
			Chi_layers_x[layer] = 0.0;
			Chi_layers_y[layer] = 0.0;
			Chi_layers_z[layer] = 0.0;
			Chi_layers_l[layer] = 0.0;
		}
		// Reset total quantities
		Mag_length_1=Mag_length_1AVG=0.0;
		Mx=My=Mz=0.0;
		Mag_length_AVG=0.0;
		Ms_AVG=Vol_AVG=Temp_AVG=Tc_AVG=0.0;
		Mx_time=My_time=Mz_time=0.0;
		Mag_length_time_AVG = 0.0;
		//---------------------------------//
		//------- Equilibration -----------//
		//---------------------------------//
		// Reset time average counter
		Rep_counter=0;
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			const int Offset = Voronoi_data.Num_Grains*layer;
			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Mag_length_1 = sqrt(Grain.m[grain+Offset].x*Grain.m[grain+Offset].x+Grain.m[grain+Offset].y*Grain.m[grain+Offset].y+Grain.m[grain+Offset].z*Grain.m[grain+Offset].z);
				Mag_length_1AVG += Mag_length_1;
			}
		}
		Mag_length_1AVG /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Mag_length_2AVG = 0.0;

		while( (fabs(Mag_length_1AVG/double(Rep_counter)-Mag_length_2AVG/double(Rep_counter+1))>Mag_length_equil_thresh || Rep_counter<50000) && Rep_counter<10000000 ){

//			std::cout << Rep_counter <<  "\t|\t" << fabs(Mag_length_1AVG/double(Rep_counter)-Mag_length_2AVG/double(Rep_counter+1)) << "\r";

			// Update old magnetisation
			Mag_length_1AVG = Mag_length_2AVG;

			// Reset to zero
			Mag_length_AVG=0.0;

			// Perform LLB integration
			LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_Mat,Voronoi_data,0.0,0.0,false,&LLB_data,&Grain);

			// Loop over each layer
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				const int Offset = Voronoi_data.Num_Grains*layer;
				// Dummy variables for calculation of magnetisation
				double Ml_dummy = 0.0;
				// Loop over grains in each layer
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Mag_length = sqrt(Grain.m[grain+Offset].x*Grain.m[grain+Offset].x+Grain.m[grain+Offset].y*Grain.m[grain+Offset].y+Grain.m[grain+Offset].z*Grain.m[grain+Offset].z);
					Ml_dummy += Mag_length;
				}
				Magnetisation_layers_l[layer] = Ml_dummy;
			}
			//Calculate per grains average values
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				Magnetisation_layers_l[layer] /= double(Voronoi_data.Num_Grains);
			}
			//Calculate total system properties
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				Mag_length_AVG += Magnetisation_layers_l[layer];
			}
			Mag_length_AVG /= double(Structure.Num_layers);

			Mag_length_time_AVG += Mag_length_AVG;

			Mag_length_2AVG = Mag_length_time_AVG;

			// Increase counter
			++Rep_counter;
		}

//		std::cout << "\nTime average " << Temp << "K\n";
		//----------------------------------------------------//
		//----------------- Time average ---------------------//
		//----------------------------------------------------//
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			Magnetisation_layers_x[layer] = 0.0;
			Magnetisation_layers_y[layer] = 0.0;
			Magnetisation_layers_z[layer] = 0.0;
			Magnetisation_layers_l[layer] = 0.0;
			Ms_layers[layer] = 0.0;
			Magnetisation_layers_time_x[layer] = 0.0;
			Magnetisation_layers_time_y[layer] = 0.0;
			Magnetisation_layers_time_z[layer] = 0.0;
			Magnetisation_layers_time_l[layer] = 0.0;
			Ms_layers_time[layer] = 0.0;
			Chi_layers_x[layer] = 0.0;
			Chi_layers_y[layer] = 0.0;
			Chi_layers_z[layer] = 0.0;
			Chi_layers_l[layer] = 0.0;
		}
		// Reset total quantities
		Mag_length_1=Mag_length_1AVG=0.0;
		Mx=My=Mz=0.0;
		Mag_length_AVG=0.0;
		Ms_AVG=Vol_AVG=Temp_AVG=Tc_AVG=0.0;
		Mx_time=My_time=Mz_time=0.0;
		Mag_length_time_AVG = 0.0;
		// Reset counter and calculate time average of magnetisation
		Rep_counter=0;
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			const int Offset = Voronoi_data.Num_Grains*layer;
			for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
				Mag_length_1 = sqrt(Grain.m[grain+Offset].x*Grain.m[grain+Offset].x+Grain.m[grain+Offset].y*Grain.m[grain+Offset].y+Grain.m[grain+Offset].z*Grain.m[grain+Offset].z);
				Mag_length_1AVG += Mag_length_1;
			}
		}
		Mag_length_1AVG /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Mag_length_2AVG = 0.0;

		while( (fabs(Mag_length_1AVG/double(Rep_counter)-Mag_length_2AVG/double(Rep_counter+1))>Mag_length_thresh || Rep_counter<10000) && Rep_counter<10000000 ){

			std::cout << Rep_counter <<  "\t|\t" << fabs(Mag_length_1AVG/double(Rep_counter)-Mag_length_2AVG/double(Rep_counter+1)) << "\r";

			// Update old magnetisation
			Mag_length_1AVG = Mag_length_2AVG;

			// Reset to zero
			Mx=My=Mz=0.0;
			Xpara=Xperp=0.0;
			Mag_length_AVG=Ms_AVG=0.0;

			// Perform LLB integration
			LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_Mat,Voronoi_data,0.0,0.0,false,&LLB_data,&Grain);

			// Loop over each layer
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				const int Offset = Voronoi_data.Num_Grains*layer;
				// Dummy variables for calculation of magnetisation
				double Mx_dummy = 0.0;
				double My_dummy = 0.0;
				double Mz_dummy = 0.0;
				double Ml_dummy = 0.0;
				double Ms_dummy = 0.0;
				// Loop over grains in each layer
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Mag_length = sqrt(Grain.m[grain+Offset].x*Grain.m[grain+Offset].x+Grain.m[grain+Offset].y*Grain.m[grain+Offset].y+Grain.m[grain+Offset].z*Grain.m[grain+Offset].z);
					Mx_dummy += Grain.m[grain+Offset].x;
					My_dummy += Grain.m[grain+Offset].y;
					Mz_dummy += Grain.m[grain+Offset].z;
					Ml_dummy += Mag_length;
					Ms_dummy += Grain.Ms[grain+Offset];
				}
				Magnetisation_layers_x[layer] = Mx_dummy;
				Magnetisation_layers_y[layer] = My_dummy;
				Magnetisation_layers_z[layer] = Mz_dummy;
				Magnetisation_layers_l[layer] = Ml_dummy;
				Ms_layers[layer]              = Ms_dummy;
			}
			//Calculate per grains average values
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				Magnetisation_layers_x[layer] /= double(Voronoi_data.Num_Grains);
				Magnetisation_layers_y[layer] /= double(Voronoi_data.Num_Grains);
				Magnetisation_layers_z[layer] /= double(Voronoi_data.Num_Grains);
				Magnetisation_layers_l[layer] /= double(Voronoi_data.Num_Grains);
				Ms_layers[layer]              /= double(Voronoi_data.Num_Grains);
			}
			//Calculate total system properties
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				Mx             += Magnetisation_layers_x[layer];
				My             += Magnetisation_layers_y[layer];
				Mz             += Magnetisation_layers_z[layer];
				Mag_length_AVG += Magnetisation_layers_l[layer];
				Ms_AVG         += Ms_layers[layer]             ;
			}
			Mx /= double(Structure.Num_layers);
			My /= double(Structure.Num_layers);
			Mz /= double(Structure.Num_layers);
			Mag_length_AVG /= double(Structure.Num_layers);
			Ms_AVG /= double(Structure.Num_layers);

			// Calculate grain variance for grain susceptibility
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				const int Offset = Voronoi_data.Num_Grains*layer;
				for(unsigned int grain=0; grain<Voronoi_data.Num_Grains; ++grain){
					double Mag_length = sqrt(Grain.m[grain+Offset].x*Grain.m[grain+Offset].x+Grain.m[grain+Offset].y*Grain.m[grain+Offset].y+Grain.m[grain+Offset].z*Grain.m[grain+Offset].z);
					Chi_layers_x[layer] += pow(Grain.m[grain+Offset].x - Magnetisation_layers_x[layer],2);
					Chi_layers_y[layer] += pow(Grain.m[grain+Offset].y - Magnetisation_layers_y[layer],2);
					Chi_layers_z[layer] += pow(Grain.m[grain+Offset].z - Magnetisation_layers_z[layer],2);
					Chi_layers_l[layer] += pow(Mag_length               - Magnetisation_layers_l[layer],2);
				}
			}
			// Add magnetisation values and Ms to respective time averages
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				Magnetisation_layers_time_x[layer] += Magnetisation_layers_x[layer];
				Magnetisation_layers_time_y[layer] += Magnetisation_layers_y[layer];
				Magnetisation_layers_time_z[layer] += Magnetisation_layers_z[layer];
				Magnetisation_layers_time_l[layer] += Magnetisation_layers_l[layer];
				Ms_layers_time[layer]              += Ms_layers[layer]             ;
			}
			Mx_time += Mx;
			My_time += My;
			Mz_time += Mz;
			Mag_length_time_AVG += Mag_length_AVG;

			Mag_length_2AVG = Mag_length_time_AVG;

			// Increase counter
			++Rep_counter;
		}

		// Calculate time averages by dividing by the number of steps
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			Magnetisation_layers_time_x[layer] /= double(Rep_counter);
			Magnetisation_layers_time_y[layer] /= double(Rep_counter);
			Magnetisation_layers_time_z[layer] /= double(Rep_counter);
			Magnetisation_layers_time_l[layer] /= double(Rep_counter);
			Ms_layers_time[layer]              /= double(Rep_counter);
			// Susceptibility
			Chi_layers_x[layer] /= double(Rep_counter);
			Chi_layers_y[layer] /= double(Rep_counter);
			Chi_layers_z[layer] /= double(Rep_counter);
			Chi_layers_l[layer] /= double(Rep_counter);
		}
		Mx_time 					/= double(Rep_counter);
		My_time 					/= double(Rep_counter);
		Mz_time 					/= double(Rep_counter);
		Mag_length_time_AVG 	/= double(Rep_counter);

		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			const int Offset = Voronoi_data.Num_Grains*layer;
			for(unsigned int grain=0; grain<Voronoi_data.Num_Grains; ++grain){
				Temp_AVG += Grain.Temp[grain+Offset];
				Tc_AVG   += Grain.Tc[grain+Offset];
				Vol_AVG  += Grain.Vol[grain+Offset];
				Xpara    += LLB_data.Chi_para[grain+Offset];
				Xperp    += LLB_data.Chi_perp[grain+Offset];
			}
		}
		// Compute per layer average quantities
		Xpara /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Xperp /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Tc_AVG /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Temp_AVG /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Vol_AVG /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
		Vol_AVG *= 1e-21; //Convert from nm3 to cc
		const double i_kBT = Temp_AVG <1 ? 0.0 : 1.0/(1.381e-16 * Temp_AVG);

		// Calculate susceptibility prefactor for each material
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			Chi_prefactor_layers[layer] = Ms_layers_time[layer]*Vol_AVG*i_kBT;
		}

		//---------------------------------//
		//----- Output results to file ----//
		//---------------------------------//
		LLB_OUTPUT << Mx << " " << My << " " << Mz << " " << Mag_length_AVG << " ";
		LLB_OUTPUT << Mx_time << " " << My_time << " " << Mz_time << " " << Mag_length_time_AVG << " ";
		LLB_OUTPUT << Temp_AVG << " " << Tc_AVG;
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			const int Offset = Voronoi_data.Num_Grains*layer;
			LLB_OUTPUT << " " << LLB_data.Chi_para[0+Offset] << " " << LLB_data.Chi_perp[0+Offset];
		}
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << " " << LLB_data.m_EQ[layer*Voronoi_data.Num_Grains];
		}
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << " ";
			LLB_OUTPUT << Magnetisation_layers_x[layer] << " " << Magnetisation_layers_y[layer] << " " << Magnetisation_layers_z[layer] << " " << Magnetisation_layers_l[layer];
		}
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << " ";
			LLB_OUTPUT << Magnetisation_layers_time_x[layer] << " " << Magnetisation_layers_time_y[layer] << " " << Magnetisation_layers_time_z[layer] << " " << Magnetisation_layers_time_l[layer];
		}
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << " ";
			LLB_OUTPUT << Chi_prefactor_layers[layer] * Chi_layers_x[layer] << " ";
			LLB_OUTPUT << Chi_prefactor_layers[layer] * Chi_layers_y[layer] << " ";
			LLB_OUTPUT << Chi_prefactor_layers[layer] * Chi_layers_z[layer] << " ";
			LLB_OUTPUT << Chi_prefactor_layers[layer] * Chi_layers_l[layer];
		}
		LLB_OUTPUT << std::endl;
		LLB_OUTPUT << std::flush;

	}
	LLB_OUTPUT.close();


	return 0;
}
