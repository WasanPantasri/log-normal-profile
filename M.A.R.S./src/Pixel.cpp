/*
 * Pixel.cpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

#include "../hdr/Pixel.hpp"

Pixel::Pixel(unsigned int id, double X, double Y)
    : ID(id)
    , centre(std::make_pair(X,Y))
    , Grain(-1)
    , Temperature(0.0)
    , Magnetisation(0.0,0.0,0.0)
    , Field(0.0,0.0,0.0)
    , Easy_Axis(0.0,0.0,0.0)
{}

void Pixel::set_M(Vec3 M){Magnetisation=M;}
void Pixel::set_M(double M){Magnetisation=M;}
void Pixel::set_EA(Vec3 EA){Easy_Axis=EA;}
void Pixel::set_EA(double EA){Easy_Axis=EA;}
void Pixel::set_Temp(double T){Temperature=T;}
void Pixel::set_H(Vec3 H){Field=H;}
void Pixel::set_Grain(unsigned int G){Grain=G;}
unsigned int Pixel::get_ID() const {return ID;}
int Pixel::get_Grain() const {return Grain;}
Vec3 Pixel::get_M() const {return Magnetisation;}
Vec3 Pixel::get_EA() const {return Easy_Axis;}
double Pixel::get_Mx() const {return Magnetisation.x;}
double Pixel::get_My() const {return Magnetisation.y;}
double Pixel::get_Mz() const {return Magnetisation.z;}
double Pixel::get_Temp() const {return Temperature;}
Vec3 Pixel::get_H() const {return Field;}
std::pair<double,double> Pixel::get_Coords() const {return centre;}

