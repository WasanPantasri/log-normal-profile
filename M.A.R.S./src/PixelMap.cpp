/*
 * PixelMap.cpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

#include "../hdr/PixelMap.hpp"

PixelMap::PixelMap(const double Xmin, const double Ymin, const double Xmax, const double Ymax, const double cell_size)
    : Cells(0)
    , cellsize(cell_size)
{
    // Create PixelMap
    unsigned int ID=0,IDy=0;

    double y=Ymin;
    while(y<Ymax){
        double x=Xmin;
        Cells.push_back(std::vector<Pixel>());
        while(x<Xmax){
            // Want to save centre of cells
            Cells.at(IDy).push_back(Pixel(ID,x+cellsize*0.5,y+cellsize*0.5));
            ++ID;
            x += cellsize;
        }
        y += cellsize;
        ++IDy;
    }
}

PixelMap::~PixelMap() {}

static inline double isLEFT(double P0x,double P0y,double P1x,double P1y,double P2x,double P2y){
    return ((P1x-P0x)*(P2y-P0y)-(P2x-P0x)*(P1y-P0y));
}

/** Determines of a point lies within a polygon.
 * \param[in] double Px X-coordinate of point.
 * \param[in] double Py Y-coordinate of point.
 * \param[in] std::vector<double> Vx X-coordinates of polygon vertex points with V[n+1].
 * \param[in] std::vector<double> Vy Y-coordinates of polygon vertex points with V[n+1].
 * \param[in] int n Number of vertices.
 * \return Winding number (wn=0 when P is outside the polygon)
 */
static int wn_PnPoly(double Px,double Py,std::vector<double>Vx,std::vector<double>Vy,int n){

    int wn=0;
    for(int edge=0;edge<n;++edge){
        if(Vy[edge]<=Py){if(Vy[edge+1]>Py){ if(isLEFT(Vx[edge],Vy[edge],Vx[edge+1],Vy[edge+1],Px,Py)>0){++wn;} }  }// Upwards crossing
        else{ if(Vy[edge+1]<=Py){if(isLEFT(Vx[edge],Vy[edge],Vx[edge+1],Vy[edge+1],Px,Py)<0){--wn;} } } // Downwards crossing
    }
    // Special case for final edge
    if(Vy[n]<=Py){if(Vy[0]>Py){if(isLEFT(Vx[n],Vy[n],Vx[0],Vy[0],Px,Py)>0){++wn;} } } // Upwards crossing
    else{ if(Vy[0]<=Py){if(isLEFT(Vx[n],Vy[n],Vx[0],Vy[0],Px,Py)<0){--wn;} } } // Downwards crossing
    return wn;
}

void PixelMap::Discretise(const Voronoi_t&VORO, const Grain_t&Grains){

    bool in_grain=false;
    std::cout << "Discretising system" << std::endl;
    for(size_t cell_Y=0;cell_Y<Cells.size();++cell_Y){
        for(size_t cell_X=0;cell_X<Cells[cell_Y].size();++cell_X){
            in_grain=false;
            double Px = Cells[cell_Y][cell_X].get_Coords().first;
            double Py = Cells[cell_Y][cell_X].get_Coords().second;
            for(unsigned int grain=0;grain<VORO.Num_Grains;++grain){
                if(wn_PnPoly(Px,Py,VORO.Vertex_X_final[grain],VORO.Vertex_Y_final[grain],VORO.Vertex_X_final[grain].size()-1)!=0){
                    Cells[cell_Y][cell_X].set_Grain(grain);
                    in_grain=true;
                    break;
                }
            }
            if(!in_grain){Cells[cell_Y][cell_X].set_Grain(-1);} // Not in any grain
        }
    }

    MapVal(Grains, DataType::ALL);

}

void PixelMap::MapVal(const Grain_t&Grains, const DataType type){

    for(size_t y=0;y<Cells.size();++y){
        for(size_t x=0;x<Cells[y].size();++x){
            int Grain = Cells[y][x].get_Grain();
            switch(type)
            {
            case DataType::M:
                if(Grain>=0){
                    Cells[y][x].set_M(Grains.m[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_M(Vec3(0.0,0.0,0.0));
                }
                break;
            case DataType::EA:
                if(Grain>=0){
                    Cells[y][x].set_EA(Grains.Easy_axis[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_EA(Vec3(0.0,0.0,0.0));
                }
                break;
            case DataType::H:
                if(Grain>=0){
                    Cells[y][x].set_H(Grains.H_appl[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_H(Vec3(0.0,0.0,0.0));
                }
                break;
            case DataType::T:
                if(Grain>=0){
                    Cells[y][x].set_Temp(Grains.Temp[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_Temp(0.0);
                }
                break;
            case DataType::ALL:
                if(Grain>=0){
                    Cells[y][x].set_M(Grains.m[Cells[y][x].get_Grain()]);
                    Cells[y][x].set_EA(Grains.Easy_axis[Cells[y][x].get_Grain()]);
                    Cells[y][x].set_H(Grains.H_appl[Cells[y][x].get_Grain()]);
                    Cells[y][x].set_Temp(Grains.Temp[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_M(Vec3(0.0,0.0,0.0));
                    Cells[y][x].set_EA(Vec3(0.0,0.0,0.0));
                    Cells[y][x].set_H(Vec3(0.0,0.0,0.0));
                    Cells[y][x].set_Temp(0.0);
                }
            }
        }
    }
}

void PixelMap::Print(const std::string filename){

    std::ofstream FILE(("Output/"+filename).c_str());

    for(size_t y=0;y<Cells.size();++y){
        for(size_t x=0;x<Cells[y].size();++x){

            FILE << Cells[y][x].get_Coords().second << " " << Cells[y][x].get_Coords().first << " "
                 << Cells[y][x].get_Grain() << " " << Cells[y][x].get_M() << " "
                 << Cells[y][x].get_EA() << " " << Cells[y][x].get_H() << " "
                 << Cells[y][x].get_Temp() << "\n";
        }
        FILE << "\n";
    }
    FILE << std::flush;
    FILE.close();
}

void PixelMap::PrintwUpdate(const std::string filename, const Grain_t&Grains, const DataType type){

    // Want to print out the most up-to-date values for the grid
    MapVal(Grains, type);
    Print(filename);
}

void PixelMap::StoreinGrain(Grain_t&Grains){

    for(unsigned int y=0;y<Cells.size();++y){
        for(unsigned int x=0;x<Cells[0].size();++x){
            if(Cells[y][x].get_Grain()>=0){
                Grains.Pixel[Cells[y][x].get_Grain()].push_back(Cells[y][x].get_ID());
            }
        }
    }
}

/* WIP
std::vector<unsigned int> PixelMap::InRect(const double xMin, const double yMin, const double xMax, const double yMax) const {

    std::vector<unsigned int> Pixel_list(0);

    unsigned int x_MIN=0, x_MAX=getCols()-1, y_MIN=0, y_MAX=getRows()-1;
    // Y (row) MIN
    for(unsigned int y=0;y<getRows()-1;++y){
        if(Access(0, y).get_Coords().second < yMin){y_MIN = (y+1);}
        else{break;}
    }
    // Y (row) MAX
    for(unsigned int y=getRows()-1;y>0;--y){
        if(Access(0, y).get_Coords().second > yMax){y_MAX = (y-1);}
        else{break;}
    }
    // x (col) MIN
    for(unsigned int x=0;x<getCols()-1;++x){
        if(Access(x, 0).get_Coords().first < xMin){x_MIN=(x+1);}
        else {break;}
    }
    // X (col) MAX
    for(unsigned int x=getCols()-1;x>0;--x){
        if(Access(x, 0).get_Coords().first > xMax){x_MAX = (x-1);}
        else{break;}
    }

    for(unsigned int y=y_MIN;y<=y_MAX;++y){
        for(unsigned int x=x_MIN;x<=x_MAX;++x){
            Pixel_list.push_back(Access(x, y).get_ID());
        }
    }
    return Pixel_list;
}

void PixelMap::ClearH(){
    for(unsigned int y=0;y<getRows();++y){
        for(unsigned int x=0;x<getCols();++x){
            Cells[y][x].set_H(Vec3(0.0,0.0,0.0));
        }
    }
}

void PixelMap::ClearT(){
    for(unsigned int y=0;y<getRows();++y){
        for(unsigned int x=0;x<getCols();++x){
            Cells[y][x].set_Temp(0.0);
        }
    }
}
*/
Pixel PixelMap::Access(const unsigned int X, const unsigned int Y) const {
    return Cells[Y][X];
}

Pixel PixelMap::Access(const unsigned int ID) const {
    return Cells[(ID/Cells[0].size())][(ID%Cells[0].size())];
}

unsigned int PixelMap::getRows() const {
    return Cells.size();
}

unsigned int PixelMap::getCols() const {
    return Cells[0].size();
}

double PixelMap::getCellsize() const{
    return cellsize;
}
