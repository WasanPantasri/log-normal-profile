/*
 * Generate_interactions.cpp
 *
 *  Created on: 9 Feb 2019
 *      Author: Ewan Rannala
 */

/** \file Generate_interactions.cpp
 * \brief Function for generation of interaction data.
 *
 * All interaction data is handled by this function. This function only needs to be
 * performed once per simulation, after the system has been generated. There are two
 * forms of interactions which are accounted for magnetostatics and exchange.
 *
 * <P> Magnetostatic interactions: <BR>
 * The magnetostatics are determined by the dipole approximation.
 * \f[
 * 		W = \frac{V}{4\pi r^3}
 *      \begin{bmatrix}
 *          \frac{3r_{x}^2}{r^2}-1 & \frac{3r_{x}r_{y}}{r^2} & \frac{3r_{x}r_{z}}{r^2} \\
 *          \frac{3r_{y}r_{x}}{r^2} & \frac{3r_{y}^2}{r^2}-1 & \frac{3r_{y}r_{z}}{r^2} \\
 *          \frac{3r_{z}r_{x}}{r^2} & \frac{3r_{z}r_{y}}{r^2} & \frac{3r_{z}^2}{r^2}-1
 *      \end{bmatrix}
 * \f]
 * \f[
 * 		\vec{H_{dip}} = W\vec{m}
 * \f]
 * \f$V\f$ is the grain volume. <BR>
 * \f$r\f$ is the displacement between the target and source grains, with the subscript denoting the
 * component.<BR>
 * The geometrical centres for the grains are used to determine the grain positions, while the lattice
 * sites can be used, doing so produces significantly poorer results.<BR>
 * The computational efficiency of this method is improved by taking advantage of the symmetry in the
 * W-matrix (W=W<SUP>T</SUP>). Only six of the nine terms required calculation, specifically only
 * \f$W_{11}\f$, \f$W_{12}\f$, \f$W_{13}\f$, \f$W_{22}\f$, \f$W_{23}\f$, \f$W_{33}\f$ are
 * calculated.<BR>
 * While the dipole approximation provides a fast solution it is not the best solution available. For
 * grains with a large aspect ratio the accuracy is poor, this is due to the approximation that all
 * interactions are driven from the centre of the grain. Due to this MARS currently offers a method
 * to import externally an determined W-matrix. To use this method Structure_t.Magnetostatics_gen_type
 * must be set to <EM>import</EM>
 *
 * An issue with this method is its poor accuracy for grains with large aspect ratios. This poor
 * accuracy is due to the approximation that the interactions are driven from the centre of the
 * grain. <BR>
 * If a different method is desired then the w-matrix can be imported from an input file when
 * Struct_t.Magnetostatics_gen_type is set to <EM>import</EM> and a file named
 * <EM>W_matrix_MARS.dat</EM> must be placed in the Input directory.
 *

 * </P>
 * <P> Exchange interactions: <BR>
 * \f[
 * 		H_{exch\,strength} = H_{sat}J_{ij}\frac{<A>}{A_i}\frac{L_{ij}}{<L>}
 * \f]
 * \f[
 * 		\vec{H_{exch}} = H_{exch\,strength}\vec{m}
 * \f]
 * \f$< >\f$ denotes an average value.<BR>
 * \f$J_{ij}\f$ is the exchange constant between i<SUP>th</SUP> and j<SUP>th</SUP>.<BR>
 * \f$L_{ij}\f$ is the contact length between grain i<SUP>th</SUP> and j<SUP>th</SUP>.<BR>
 * \f$A_i\f$ is the area of the i<SUP>th</SUP> grain.<BR>
 * \f$H_{sat}\f$ is the exchange field strength at saturation.<BR>
 * \f$\vec{m}\f$ is the reduced magnetisation \f$(\frac{\Vec{M}}{M_s})\f$.<BR>
 * The exchange field strength has a maximum value of \f$H_{sat}\f$, to ensure this
 * \f$j_{ij}\f$ is always distributed about unity and
 * \f$<J_{ij}\frac{<A>}{A_i}\frac{L_{ij}}{<L>}>\f$ is set to equal unity via normalisation.<BR>
 *
 * When determining the exchange between separate layers a different much simpler
 * approach is applied. Here a new neighbour is added to the exchange array
 * accounting for the presence of a neighbour above and/or below the grain. The
 * exchange field strength is then assigned per material.
 * </P> */
#include <vector>
#include "math.h"
#include <iostream>
#include <fstream>
#include <sstream>

#include "../../hdr/Globals.hpp"
#include "../../hdr/Distributions_generator.hpp"
#include "../../hdr/Structures.hpp"

int Generate_interactions(const int Num_layers,const std::vector<double> Grain_Vol,
		const double Magneto_CUT_OFF,const std::string Magneto_type, const Material_t MAT,const Voronoi_t VORO,Interaction_t*Int_system){

	std::cout << "Generating interactions..." << std::endl;
	int Tot_grains = VORO.Num_Grains*Num_layers;
	// Set size of interaction vectors
	Int_system->Magneto_neigh_list.resize(Tot_grains);
	Int_system->Exchange_neigh_list.resize(Tot_grains);
	Int_system->Wxx.resize(Tot_grains); Int_system->Wxy.resize(Tot_grains);
	Int_system->Wxz.resize(Tot_grains);	Int_system->Wyy.resize(Tot_grains);
	Int_system->Wyz.resize(Tot_grains);	Int_system->Wzz.resize(Tot_grains);
	Int_system->H_exch_str.resize(Tot_grains);


//################### MAGNETOSTATIC ########################//
     if(Magneto_type=="import"){
    	 std::cout << "\tImporting..." << std::endl;
       	 double tempA,tempB;
         double Wxx,Wxy,Wxz,Wyy,Wyz,Wzz;
       	 int Import_grains, grain_ID, Neigh_size;
         std::ifstream INPUT_FILE("Input/W_matrix_MARS.dat");
         std::string line;
         std::getline(INPUT_FILE,line);
       	 std::stringstream ss(line);

         if(ss >> tempA >> Import_grains >> tempB){
        	 if(Import_grains!=Tot_grains){
        		std::cout << "Mismatch between grains in magnetostatics input and total system.\n"
        				  << "Check grains per layer and total number of layers.\n"
        				  << "Have you included interactions between layers?" << std::endl;
        		exit (EXIT_FAILURE);
        	 }
        	 else{
                 Int_system->Wxx.resize(Import_grains);
                 Int_system->Wxy.resize(Import_grains);
                 Int_system->Wxz.resize(Import_grains);
                 Int_system->Wyy.resize(Import_grains);
                 Int_system->Wyz.resize(Import_grains);
                 Int_system->Wzz.resize(Import_grains);
        	 }
         }
         else{
        	 std::cout << "Error failed to read interactions file." << std::endl;
        	 exit (EXIT_FAILURE);
         }
         // Read in rest of the data file
         for(int grain=0;grain<Import_grains;++grain){
        	 INPUT_FILE >> grain_ID >> Neigh_size;
             // Read in W-matrix for each neighbour.
             for(int neigh=0;neigh<Neigh_size;++neigh){
            	 INPUT_FILE >> tempA >> tempB >> Wxx >> Wxy >> Wxz >> Wyy >> Wyz >> Wzz;
                 // Need to use grain_ID-1 as input file counts from 1.
                 Int_system->Wxx[grain_ID-1].push_back(Wxx);
                 Int_system->Wxy[grain_ID-1].push_back(Wxy);
                 Int_system->Wxz[grain_ID-1].push_back(Wxz);
                 Int_system->Wyy[grain_ID-1].push_back(Wyy);
                 Int_system->Wyz[grain_ID-1].push_back(Wyz);
                 Int_system->Wzz[grain_ID-1].push_back(Wzz);
             }
         }
     }
	else{
		// look through all grains in the ENTIRE system
		for(int i=0;i<Tot_grains;++i){
			int grain_in_layer_i = i % VORO.Num_Grains,
					Z_layer_i = int(i/VORO.Num_Grains);
			double Grain_x=VORO.Geo_grain_centre_X[grain_in_layer_i];
			double Grain_y=VORO.Geo_grain_centre_Y[grain_in_layer_i];
			// Loop over grains is split into two to avoid i==j
			for(int j=0;j<i;++j){
				int grain_in_layer_j = j % VORO.Num_Grains,
						Z_layer_j = int(j/VORO.Num_Grains);
				double Neigh_x=VORO.Geo_grain_centre_X[grain_in_layer_j];
				double Neigh_y=VORO.Geo_grain_centre_Y[grain_in_layer_j];
				double x_diff, y_diff, z_diff, Separation;
				// Account for the periodic boundaries of the system.
				if((Grain_x-Neigh_x) > (VORO.Centre_X)){Neigh_x += VORO.x_max;}					// LHS
				else if((Grain_x-Neigh_x) < (-VORO.Centre_X)){Neigh_x -= VORO.x_max;}			// RHS
				if((Grain_y-Neigh_y) < (-VORO.Centre_Y)){Neigh_y -= VORO.y_max;}				// TOP
				else if((Grain_y-Neigh_y) > (VORO.Centre_Y)){Neigh_y += VORO.y_max;}			// BOT

				x_diff = (Neigh_x-Grain_x);
				y_diff = (Neigh_y-Grain_y);
				z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
				Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);

				if(Separation<=Magneto_CUT_OFF){
					Int_system->Magneto_neigh_list[i].push_back(j);
					double Wprefactor = (Grain_Vol[grain_in_layer_j]*1e-21)/(4*PI*pow(Separation,3));
					// Symmetric matrix, thus only 6/9 elements are determined.
					Int_system->Wxx[i].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
					Int_system->Wxy[i].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
					Int_system->Wxz[i].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
					Int_system->Wyy[i].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
					Int_system->Wyz[i].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
					Int_system->Wzz[i].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
				}
			}
			for(int j=i+1;j<Tot_grains;++j){
				int grain_in_layer_j = j % VORO.Num_Grains,
						Z_layer_j = int(j/VORO.Num_Grains);
				double Neigh_x=VORO.Geo_grain_centre_X[grain_in_layer_j];
				double Neigh_y=VORO.Geo_grain_centre_Y[grain_in_layer_j];
				double x_diff, y_diff, z_diff, Separation;
				// Account for the periodic boundaries of the system.
				if((Grain_x-Neigh_x) > (VORO.Centre_X)){Neigh_x += VORO.x_max;}					// LHS
				else if((Grain_x-Neigh_x) < (-VORO.Centre_X)){Neigh_x -= VORO.x_max;}			// RHS
				if((Grain_y-Neigh_y) < (-VORO.Centre_Y)){Neigh_y -= VORO.y_max;}				// TOP
				else if((Grain_y-Neigh_y) > (VORO.Centre_Y)){Neigh_y += VORO.y_max;}			// BOT

				x_diff = (Neigh_x-Grain_x);
				y_diff = (Neigh_y-Grain_y);
				z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
				Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);

				if(Separation<=Magneto_CUT_OFF){
					Int_system->Magneto_neigh_list[i].push_back(j);
					double Wprefactor = (Grain_Vol[grain_in_layer_j]*1e-21)/(4*PI*pow(Separation,3));
					// Symmetric matrix, thus only 6/9 elements are determined.
					Int_system->Wxx[i].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
					Int_system->Wxy[i].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
					Int_system->Wxz[i].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
					Int_system->Wyy[i].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
					Int_system->Wyz[i].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
					Int_system->Wzz[i].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
				}
			}
		}
	}
//################### EXCHANGE ########################//
	int counter=0;
	double Average=0.0, Normaliser;
	std::vector<std::vector<double>> Product(Tot_grains),exchange_constants(Tot_grains);
	// PERFORM NORMALISATION OF THE EXCHANGE DISTRIBUTION
	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = i % VORO.Num_Grains,
		    Z_layer_i = int(i/VORO.Num_Grains);
		// Generate random numbers for neighbours of grain i.
		Dist_gen(VORO.Neighbour_final[grain_in_layer_i].size(),MAT.J_Dist_Type[Z_layer_i],1.0,MAT.StdDev_J[Z_layer_i],&exchange_constants[i]);
		for(unsigned  j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
			double Avg_A_o_A = (VORO.Average_area/VORO.Grain_Area[grain_in_layer_i]);
			double L_o_Avg_L = (VORO.Contact_lengths[grain_in_layer_i][j]/VORO.Average_contact_length);
			Product[i].push_back(exchange_constants[i][j]*Avg_A_o_A*L_o_Avg_L);
	}	}

	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = i % VORO.Num_Grains;
		for(size_t j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
			counter++;
			Average += Product[i][j];
	}	}
	Average /= counter;
	Normaliser = 1.0/Average;

	// CREATE NEIGHBOUR LIST PLUS EXCHANGE VALUES IN-PLANE AND OUT-PLANE
	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = i % VORO.Num_Grains,
			Z_layer_i = int(i/VORO.Num_Grains),
			Layer_offset = Z_layer_i*VORO.Num_Grains; // Enables use of single layer vector for all layers
		for(size_t j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
			Int_system->Exchange_neigh_list[i].push_back(VORO.Neighbour_final[grain_in_layer_i][j]+Layer_offset);
			Int_system->H_exch_str[i].push_back(Product[i][j]*Normaliser*MAT.H_sat[Z_layer_i]);
		}
		// Conditions to check for layers above and below
		if(Z_layer_i-1>=0){ // Add exchange from layer below
			Int_system->Exchange_neigh_list[i].push_back(i-VORO.Num_Grains); // Add ith grain from upper layer
			double EXCH_FROM_DN = MAT.Hexch_str_out_plane_UP[Z_layer_i-1]*VORO.Grain_Area[grain_in_layer_i]/VORO.Average_area;
			Int_system->H_exch_str[i].push_back(EXCH_FROM_DN);
		}
		if(Z_layer_i+1<=Num_layers-1){ // Add exchange from layer above
			Int_system->Exchange_neigh_list[i].push_back(i+VORO.Num_Grains); // Add ith grain from lower layer
			double EXCH_FROM_UP = MAT.Hexch_str_out_plane_DOWN[Z_layer_i+1]*VORO.Grain_Area[grain_in_layer_i]/VORO.Average_area;
			Int_system->H_exch_str[i].push_back(EXCH_FROM_UP);
	}	}
	return 0;
}


