/*
 * Structures.cpp
 *
 *  Created on: 14 Mar 2020
 *      Author: Ewan Rannala
 */

#include "../hdr/Structures.hpp"

#include <iostream>


Vec3::Vec3(){
    x=0.0;
    y=0.0;
    z=0.0;
}

Vec3::Vec3(double x,double y,double z){
    this->x = x;
    this->y = y;
    this->z = z;
}

Vec3::Vec3(const Vec3& vector){
    x=vector.x;
    y=vector.y;
    z=vector.z;
}

Vec3::~Vec3(){}

std::ostream& operator<<(std::ostream& os, const Vec3& vec){
	return os << vec.x << " " << vec.y << " " << vec.z;
}

Vec3 operator+(Vec3 const &lhs, Vec3 const &rhs){
	Vec3 res;
	res.x = lhs.x + rhs.x;
	res.y = lhs.y + rhs.y;
	res.z = lhs.z + rhs.z;
	return res;
}

Vec3 operator-(Vec3 const &lhs, Vec3 const &rhs){
	Vec3 res;
	res.x = lhs.x - rhs.x;
	res.y = lhs.y - rhs.y;
	res.z = lhs.z - rhs.z;
	return res;
}

double operator*(Vec3 const &lhs, Vec3 const &rhs){
	return lhs.x*rhs.x+lhs.y*rhs.y+lhs.z*rhs.z;
}

Vec3 operator*(Vec3 const &lhs, double const &rhs){
	Vec3 res;
	res.x = lhs.x*rhs;
	res.y = lhs.y*rhs;
	res.z = lhs.z*rhs;
	return res;
}
Vec3 operator*(double const &lhs, Vec3 const &rhs){
	Vec3 res;
	res.x = rhs.x*lhs;
	res.y = rhs.y*lhs;
	res.z = rhs.z*lhs;
	return res;
}

Vec3 operator/(Vec3 const &lhs, double const &rhs){
	Vec3 res;
	res.x = lhs.x/rhs;
	res.y = lhs.y/rhs;
	res.z = lhs.z/rhs;
	return res;
}

Vec3 operator%(Vec3 const &lhs, Vec3 const &rhs){
	Vec3 res;
	res.x = (lhs.y*rhs.z - lhs.z*rhs.y);
	res.y = (lhs.z*rhs.x - lhs.x*rhs.z);
	res.z = (lhs.x*rhs.y - lhs.y*rhs.x);
	return res;
}
