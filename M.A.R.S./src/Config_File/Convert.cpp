/* Convert.cpp
 *  Created on: 28 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Convert.cpp
 * \brief Function implementations for the Convert class. */

#include "../../hdr/Config_File/Convert.hpp"
#include <sstream>
#include <typeinfo>

const std::string Convert::BoldRedFont = "\033[1;4;31m";
const std::string Convert::ResetFont = "\033[0m";

/** Converts the input type to a std::string.
 * \tparam val `int`, `double` or `bool`.
 * \param[in] FileName std::string indicating the name of the input file.
 * \param[in] val variable to be converted.
 * \param[in] key std::string indicating the requested key.
 * \return val converted to a std::string. */
template <typename T>
std::string Convert::T_to_string(const std::string FileName, T const &val, std::string const &key){
    // Convert T, which should be a primitive, to a std::string.
	std::ostringstream outstring;
	outstring << val;
	return outstring.str();
}

/** Converts the input std::string to the declared type.
 * \tparam val `std::string`, `bool`, std::vector<double>` or `std::vector<int>`.
 * \param[in] FileName std::string indicating the name of the input file.
 * \param[in] val std::string variable to be converted.
 * \param[in] key std::string indicating the requested key.
 * \return val converted to a std::string. */
template <typename T>
T Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
    // Convert a std::string to T.
	std::istringstream istr(val);
	T returnVal;
	if (!(istr >> returnVal)){
		throw std::runtime_error(BoldRedFont + "CFG error: Invalid " + (std::string)typeid(T).name() + " received from " + key + " in " + FileName + ResetFont + "\n");
	}
	return returnVal;
}

template <>
std::string Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	return val;
}

template <>
std::vector<std::string> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	std::string temp=val;
	std::stringstream istr(temp);
	std::string string;
	std::vector<std::string> Strings;
	while (istr >> string || !istr.eof()){
		if (istr.fail()){
			throw std::runtime_error(BoldRedFont + "CFG error: Invalid int received from " + key + " in " + FileName + ResetFont + "\n");
		}
		Strings.push_back(string);
	}
	return Strings;
}

template <>
unsigned int Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
    // Convert a std::string to T.
	std::istringstream istr(val);
	int returnVal;
	if (!(istr >> returnVal) || returnVal<0){
		throw std::runtime_error(BoldRedFont + "CFG error: Invalid unsigned int received from " + key + " in " + FileName + ResetFont + "\n");
	}
	return static_cast<unsigned int>(returnVal);
}

template <>
bool Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	bool returnVal;
	if(val=="true"){returnVal=true;}
	else if(val=="false"){returnVal=false;}
	else{
		throw std::runtime_error(BoldRedFont + "CFG error: Invalid Boolean received from " + key + " in " + FileName + ResetFont + "\n");
	}
	return returnVal;
}

template <>
std::vector<double> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	std::string temp=val;
	std::stringstream istr(temp);
	double number;
	std::vector<double> Numbers;
	while (istr >> number || !istr.eof()){
		if (istr.fail()){
			throw std::runtime_error(BoldRedFont + "CFG error: Invalid double received from " + key + " in " + FileName + ResetFont + "\n");
		}
		Numbers.push_back(number);
	}
	return Numbers;
}

template <>
std::vector<int> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	std::string temp=val;
	std::stringstream istr(temp);
	int number;
	std::vector<int> Numbers;
	while (istr >> number || !istr.eof()){
		if (istr.fail()){
			throw std::runtime_error(BoldRedFont + "CFG error: Invalid int received from " + key + " in " + FileName + ResetFont + "\n");
		}
		Numbers.push_back(number);
	}
	return Numbers;
}

template <>
std::vector<unsigned int> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	std::string temp=val;
	std::stringstream istr(temp);
	int number;
	std::vector<unsigned int> Numbers;
	while (istr >> number || !istr.eof()){
		if (istr.fail() || number<0){
			throw std::runtime_error(BoldRedFont + "CFG error: Invalid unsigned int received from " + key + " in " + FileName + ResetFont + "\n");
		}
		Numbers.push_back(static_cast<unsigned int>(number));
	}
	return Numbers;
}

template std::string Convert::T_to_string(const std::string, std::string const&, std::string const &);
template std::string Convert::T_to_string(const std::string, double const&, std::string const &);

template double Convert::string_to_T(const std::string, std::string const&, std::string const &);
template unsigned int Convert::string_to_T(const std::string, std::string const&, std::string const &);
template int Convert::string_to_T(const std::string, std::string const&, std::string const &);
template bool Convert::string_to_T(const std::string, std::string const&, std::string const &);



