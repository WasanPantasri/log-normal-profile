/*
 * KMC.cpp
 *
 *  Created on: 1 Nov 2018
 *      Author: Sergiu Ruta
 *      Editions by: Ewan Rannala
 */

/** \file KMC_core.cpp
 * \brief kMC core function.
 *
 *  This function first finds two possible orientations of spins given the anisotropy vector and the
 *	external field, which both define the line along which the new moment orientation will lie.
 *  The energy barrier is then calculated, separating the minima for the up and down states. To calculate
 *  the energy barrier the following form of the Stoner-Wohlfarth energy is assumed:
 *  \f[
 *  	e = \frac{KV}{k_BT}\sin(\theta)^2 - \frac{M_sVH_{eff}}{k_BT}\cos(\theta-\theta_h);
 *  \f]
 *  The implementation uses the normalisation:
 *  \f[
 *  	e = \frac{KV}{k_BT}\sin(\theta)^2 - \frac{M_sVH_k}{k_BT}h_{eff}\cos(\theta-\theta_h);
 *  \f]
 */
//#define F0h 1.0e9
#define PP (1.0/3.0)
#define ANGTOL 1.0e-4
#define INFTY 1.0e10
#define FERRO_EBCUTOFF 50.0

#include <vector>
#include "math.h"
#include <random>
#include <iostream>

#include "../../hdr/Globals.hpp"
/**
 *
 *  \param[in] Easy_axis Anisotropy vector component of a grain. <BR>
 *  \param[in] h_eff effective field vector components (Normalised by \f$H_k = \frac{2K}{M_s}\f$) <BR>
 *  \param[in] KV_over_kBT \f$\frac{KV}{k_BT}\f$. <BR>
 *  \param[in] MsVHk_over_kBT \f$\frac{M_sVH_K}{k_BT}\f$. <BR>
 *  \param[in] tm Time constant. <BR>
 *  \param[in] F0h Attempt frequency. <BR>
 *  \param[out] Spin Grain's magnetisation vector component.<BR>
 */
int KMC_core(const std::vector<double> Easy_axis,const std::vector<double> h_eff,const double KV_over_kBT,const double MsVHk_over_kBT,const double tm,const double F0h, std::vector<double>*Spin){

	/*   Internal variables
	 *   eminust -> energy associated with the energy of the minus state
	 *   eplust -> energy associated with the energy of the plus state
	 *   de -> energy barrier - difference between max energy and the energy
	 *        of the closer of the two minima
	 *   dele -> difference between the energies of the minima
	 */
	std::uniform_real_distribution<double> Random(0.0,1.0);
    // Decide whether the initial state is "+" (diro = 1) or "-" (diro = -1)
    double vecmag = Easy_axis[0]*Spin->at(0)+Easy_axis[1]*Spin->at(1)+Easy_axis[2]*Spin->at(2);
    int diro = -1;
    if (vecmag >= 0.0){diro=1;}

    double err_abs;

    double ht;
    double htot[3] = {0.0, 0.0, 0.0};
    double snew[3] = {0.0, 0.0, 0.0};
    double sdn[3] = {0.0, 0.0, 0.0};
    double sup[3] = {0.0, 0.0, 0.0};

//------------------------------The "up" state------------------------------//
    for(int i=0;i<3;++i){Spin->at(i) = Easy_axis[i];}
    err_abs = INFTY;
    while(err_abs > ANGTOL){
        vecmag = Easy_axis[0]*Spin->at(0)+Easy_axis[1]*Spin->at(1)+Easy_axis[2]*Spin->at(2);
        for(int i=0; i<3; ++i){htot[i] = vecmag*Easy_axis[i]+h_eff[i];}
        ht = sqrt(htot[0]*htot[0]+htot[1]*htot[1]+htot[2]*htot[2]);
        if(ht > 0.0){for(int i=0; i<3; ++i){snew[i] = htot[i]/ht;}}
        else{for(int i=0; i<3; ++i){snew[i] = Spin->at(i);}}
        err_abs = sqrt((snew[0]-Spin->at(0))*(snew[0]-Spin->at(0))+(snew[1]-Spin->at(1))*(snew[1]-Spin->at(1))+(snew[2]-Spin->at(2))*(snew[2]-Spin->at(2)));
        for(int i=0; i<3; ++i){Spin->at(i) = snew[i];}
    }
    for(int i=0; i<3; ++i){sup[i] = Spin->at(i);}

//-----------------------------The "down" state-----------------------------//
    for(int i=0; i<3; ++i){Spin->at(i) = -Easy_axis[i];}
    err_abs = INFTY;
    while(err_abs > ANGTOL){
        vecmag = Easy_axis[0]*Spin->at(0)+Easy_axis[1]*Spin->at(1)+Easy_axis[2]*Spin->at(2);
        for(int i=0; i<3; ++i){htot[i] = vecmag*Easy_axis[i]+h_eff[i];}
        ht = sqrt(htot[0]*htot[0]+htot[1]*htot[1]+htot[2]*htot[2]);
        if(ht > 0.0){for(int i=0; i<3; ++i){snew[i] = htot[i]/ht;}}
        else{for(int i=0; i<3; ++i){snew[i] = Spin->at(i);}}
        err_abs = sqrt((snew[0]-Spin->at(0))*(snew[0]-Spin->at(0))+(snew[1]-Spin->at(1))*(snew[1]-Spin->at(1))+(snew[2]-Spin->at(2))*(snew[2]-Spin->at(2)));
        for(int i=0; i<3; ++i){Spin->at(i) = snew[i];}
    }
    for(int i=0; i<3; ++i){sdn[i] = Spin->at(i);}
//==========================================================================//

    // Calculate the energy barrier for reversal against the field.
    // First set the direction to the old value.
    double hnorm=sqrt(h_eff[0]*h_eff[0]+h_eff[1]*h_eff[1]+h_eff[2]*h_eff[2]);
    double hu[3] = {0.0, 0.0, 0.0};
    if(hnorm != 0.0){for(int i=0; i<3; ++i){hu[i] = h_eff[i]/hnorm;}}
    else{for(int i=0;i<3;++i){hu[i] = 0.0;}}

    int dirn = diro;
    double coshe = hu[0]*Easy_axis[0]+hu[1]*Easy_axis[1]+hu[2]*Easy_axis[2];
    double pp1 = (double) pow((double) coshe*coshe, (double) PP);
    double pp2 = (double) pow((double) (1.0-coshe*coshe), (double) PP);
    double hebsy = (double) pow((double) (pp1+pp2), (double) -1.5);
    double gebsy = 0.86+1.14*hebsy;
    double hcon = hnorm/hebsy;
    double de, eb, dele, ebdu, ebud, eplust, eminust, cosang, fielddirn;

    if(hcon >= 1.0){de = 0.0;}
    else{
        eb = (double) pow((double) (1.0-hcon), (double) gebsy);
        de = KV_over_kBT*eb;
    }

    // energy of up state
    eplust = -MsVHk_over_kBT*(sup[0]*h_eff[0]+sup[1]*h_eff[1]+sup[2]*h_eff[2]);
    cosang = (Easy_axis[0]*sup[0]+Easy_axis[1]*sup[1]+Easy_axis[2]*sup[2]);
    eplust = eplust+KV_over_kBT*(1.0-cosang*cosang);

    // energy of down state
    eminust = -MsVHk_over_kBT*(sdn[0]*h_eff[0]+sdn[1]*h_eff[1]+sdn[2]*h_eff[2]);
    cosang = (Easy_axis[0]*sdn[0]+Easy_axis[1]*sdn[1]+Easy_axis[2]*sdn[2]);
    eminust = eminust+KV_over_kBT*(1.0-cosang*cosang);

    dele = eminust-eplust;

    fielddirn = h_eff[0]*Easy_axis[0]+h_eff[1]*Easy_axis[1]+h_eff[2]*Easy_axis[2];
    if(fielddirn > 0.0){
        ebdu = de;
        ebud = de+dele;
    }
    else{
        ebud = de;
        ebdu = de-dele;
    }

    // If outside of the SW-asteroid...
    if(hcon >= 1.0){
        if(fielddirn > 0.0){dirn = 1;}
        else{dirn = -1;}

        // Assign new moment direction
        for(int i=0; i<3; ++i){Spin->at(i) = sup[i];}
        if(dirn < 0){
            for(int i=0; i<3; ++i){Spin->at(i) = sdn[i];}
    }	}
    else{
        // calculation of relaxation times
        double towiv, towud, ttot, xvar, prot, test = 0.0;
        if(sqrt(ebud*ebud) > FERRO_EBCUTOFF){
            towiv = 0.0;
            if(ebud < 0.0){test=1.0;}
        }
        else{towiv = F0h*exp(-ebud);}

        if(sqrt(ebdu*ebdu) > FERRO_EBCUTOFF){
            towud = 0.0;
            if(ebdu < 0.0){test = 1.0;}
        }
        else{towud = F0h*exp(-ebdu);}

        ttot = towiv+towud;
        prot = 1.0-exp(-tm*ttot);

        if(test > 0.5){prot = 1.0;}
        double ran = Random(Gen);
        if(prot > ran){
            dirn = 1;
            ran = Random(Gen);
            if(dele > FERRO_EBCUTOFF){xvar = 0.0;}
            else{
                if(dele < -FERRO_EBCUTOFF){xvar = 1.0;}
                else{
                    xvar = exp(-dele);
                    xvar = xvar/(xvar+1.0);
            }	}
            if(xvar > ran){dirn = -1;}
        }
        // Assign new moment direction
        if(dirn > 0){for(int i=0; i<3; ++i){Spin->at(i) = sup[i];}}
        else{for(int i=0; i<3; ++i){Spin->at(i) = sdn[i];}}
    }
    return 0;
}


