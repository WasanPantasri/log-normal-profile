/* LLB_Thermal_properties.cpp
 *  Created on: 13 Jul 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file LLB_Thermal_properties.cpp
 * \brief This function determines the susceptibility for use in the LLB solver. */
#include <iostream>
#include "math.h"
#include <string>
#include <vector>

#include "../../hdr/Structures.hpp"
/** Multiple fitting methods are available within this function. The desired fit is selected in
 *  the material configuration file with the <B>Mat:susceptibility_type</B> parameter. <BR>
 *  The available options are: "default", "kazantseva_susceptibility_fit",
 *  "vogler_scusceptibility_fit" or "inv_scusceptibility_fit".<BR>
 *	<B>If none of these options are found the function will always use the Kazantseva fitting method.</B><BR>
 *  <P>
 *  The kazantseva fitting method originates from "Dynamic response of the magnetisation to picosecond
 *  heat pulses" Natalia Kazantseva 2008 <BR>
 *  The Susceptibilities are given by:
 *  \f[
 *  	\tilde{\chi}_{\parallel} =\left \{
 *  	\begin{aligned}
 *      	a_0 + \sum_{i=1}^{9} &a_i(T_c-T)^i, && \text{if } T < T_c \\
 *         	&b_0\frac{1}{4\pi}\frac{T}{T-T_c}, && \text{if } T > T_c
 *	   	\end{aligned} \right.
 *	\f]
 *	\f[
 * 		\tilde{\chi}_{\bot} =\left \{
 *      \begin{aligned}
 *      	a_0 + \sum_{i=1}^{9} &a_i\bigg(1-\frac{T}{1.068T_c}\bigg)^i, && \text{if } T < 1.065T_c \\
 *          &b_0\frac{1}{4\pi}\frac{T}{T-T_c}, && \text{if } T > 1.065T_c
 * 		\end{aligned} \right.
 *  \f]
 * <B>If</B><EM> "Default"</EM> is used the following parameters are applied:
 * <center>
 * | Parameter | \f$\chi_{\parallel}\f$ | \f$\chi_{\bot}\f$        |
 * | :-------: | :--------------------: | :----------------------: |
 * |\f$a_0\f$  |\f$2.11\times10^4\f$    |\f$1.21\times10^4\f$      |
 * |\f$a_1\f$  |\f$1.10\times10^6\f$    |\f$-2.20\f$               |
 * |\f$a_2\f$  |\f$-8.55\times10^6\f$   |\f$0.0\f$                 |
 * |\f$a_3\f$  |\f$3.42\times10^6\f$    |\f$1.95\times10^{-6}\f$   |
 * |\f$a_4\f$  |\f$-7.85\times10^7\f$   |\f$-1.3\times10^{-10}\f$  |
 * |\f$a_5\f$  |\f$1.03\times10^8\f$    |\f$0.0\f$                 |
 * |\f$a_6\f$  |\f$-6.86\times10^6\f$   |\f$-4.00\times10^{-16}\f$ |
 * |\f$a_7\f$  |\f$7.97\times10^6\f$    |\f$0.0\f$                 |
 * |\f$a_8\f$  |\f$1.54\times10^7\f$    |\f$0.0\f$                 |
 * |\f$a_9\f$  |\f$-6.27\times10^6\f$   |\f$-6.51\times10^{-25}\f$ |
 * |\f$b_0\f$  |\f$4.85\times10^4\f$    |\f$2.12\times10^4\f$      |
 * </center>
 * <EM>"kazantseva_susceptibility_fit", "vogler_scusceptibility_fit" <EM>and</EM>
 * "inv_scusceptibility_fit"</EM> options use parameters defined in the material configuration files.
 * </P>
 *  The Vogler fitting method originates from Vogler et al, J. Appl. Phys. 119, 223903 (2016) https://doi.org/10.1063/1.4953390 <BR>
 *  The Susceptibilities are given by:
 *  \f[
 *  	\chi_{\parallel} =\left \{
 *  	\begin{aligned}
 *      	&\frac{a_0}{T_c-T}, && \text{if } T < T_c \\
 *         	&\frac{b_0}{T-T_c}, && \text{if } T \geq T_c
 *	   	\end{aligned} \right.
 *	\f]
 *	\f[
 * 		\chi_{\bot} =\left \{
 *      \begin{aligned}
 *      	&a_0, && \text{if } T < T_c \\
 *          &b_0, && \text{if } T \geq T_c
 * 		\end{aligned} \right.
 *  \f]
 * <P>
 *	The Inverse susceptibility fit originates from "Simulations of magnetic reversal properties in
 *	granular recording media" Matthew Ellis 2015<BR>
 *  The Susceptibilities are given by:
 *  \f[
 *  	\chi_{\parallel} =\left \{
 *  	\begin{aligned}
 *      	a_0 + a_{\frac{1}{2}}\bigg(\frac{T_c-T}{T_c}\bigg)^{\frac{1}{2}} &+ \sum_{i=1}^{9} a_i\bigg(\frac{T_c-T}{T_c}\bigg)^i, && \text{if } T < T_c \\
 *         	&b_1(T-T_c), && \text{if } T \geq T_c
 *	   	\end{aligned} \right.
 *	\f]
 *	\f[
 * 		\chi_{\bot} =\left \{
 *      \begin{aligned}
 *      	a_0 + a_{\frac{1}{2}}\bigg(\frac{|T_c-T|}{Tc}\bigg)^{\frac{1}{2}} &+ \sum_{i=1}^{9} a_i\bigg(\frac{T_c-T}{T_c}\bigg)^i, && \text{if } T < T_c \\
 *          &b_0 + \sum_{i=1}^{4} b_i\bigg(\frac{T-T_c}{T_c}\bigg)^i, && \text{if } T \geq T_c
 * 		\end{aligned} \right.
 *  \f]
 *	</P>
 *	<P>
 *	\param[in] Num_Grains Number of grains per layer.
 *	\param[in] Num_Layers Number of layers in the system.
 *	\param[in] Included_grains_in_system List of grains within the integration exclusion zone.
 *	\param[in] Grain Struct containing grain specific data
 *	\param[out] LLB Struct Containing LLB specific data
 */
int LLB_Thermal_Properties(const int Num_Grains, const int Num_Layers,const std::vector<unsigned int> Included_grains_in_system, const Grain_t Grain, LLB_t*LLB){

	for(int Layer=0;Layer<Num_Layers;++Layer){
		int Offset = Num_Grains*Layer; // Needs to be the number of grains per layer
		std::string Susceptibility_type_DUMMY = LLB->Susceptibility_Type[Layer];
		// Temporary variables to make code more clean
		double Chi_scaling_factor = LLB->Chi_scaling_factor[Layer];
		double a0_PARA = LLB->a0_PARA[Layer], a1_PARA = LLB->a1_PARA[Layer],
				a2_PARA = LLB->a2_PARA[Layer], a3_PARA = LLB->a3_PARA[Layer],
				a4_PARA = LLB->a4_PARA[Layer], a5_PARA = LLB->a5_PARA[Layer],
				a6_PARA = LLB->a6_PARA[Layer], a7_PARA = LLB->a7_PARA[Layer],
				a8_PARA = LLB->a8_PARA[Layer], a9_PARA = LLB->a9_PARA[Layer],
				a1_2_PARA = LLB->a1_2_PARA[Layer];
		double b0_PARA = LLB->b0_PARA[Layer], b1_PARA = LLB->b1_PARA[Layer],
			   b2_PARA = LLB->b2_PARA[Layer], b3_PARA = LLB->b3_PARA[Layer],
			   b4_PARA = LLB->b4_PARA[Layer];
		double a0_PERP = LLB->a0_PERP[Layer], a1_PERP = LLB->a1_PERP[Layer],
				a2_PERP = LLB->a2_PERP[Layer], a3_PERP = LLB->a3_PERP[Layer],
				a4_PERP = LLB->a4_PERP[Layer], a5_PERP = LLB->a5_PERP[Layer],
				a6_PERP = LLB->a6_PERP[Layer], a7_PERP = LLB->a7_PERP[Layer],
				a8_PERP = LLB->a8_PERP[Layer], a9_PERP = LLB->a9_PERP[Layer],
				a1_2_PERP = LLB->a1_2_PERP[Layer];
		double b0_PERP = LLB->b0_PERP[Layer],
				b1_PERP = LLB->b1_PERP[Layer],  b2_PERP = LLB->b2_PERP[Layer],
				b3_PERP = LLB->b3_PERP[Layer],  b4_PERP = LLB->b4_PERP[Layer];

		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			int grain_in_system = grain_in_layer+Offset;
			int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
			double Temperature = Grain.Temp[grain_in_sys], Tc = Grain.Tc[grain_in_sys];

//####################################### DETERMINE THE FITTING METHOD #######################################//
//==================== If susceptibility is default or Kazantseva_susceptibility_fit ========================//
			if(LLB->Susceptibility_Type[Layer]=="default" || LLB->Susceptibility_Type[Layer]=="kazantseva_susceptibility_fit"){
				if(Temperature<Tc){
					LLB->Chi_para[grain_in_sys] = a0_PARA * (Tc/ (4*PI*(Tc-Temperature)) )
			    								   + a1_PARA * (Tc-Temperature)
			    								   + a2_PARA * pow((Tc-Temperature),2.0)
			    							   	   + a3_PARA * pow((Tc-Temperature),3.0)
			    							   	   + a4_PARA * pow((Tc-Temperature),4.0)
			    							   	   + a5_PARA * pow((Tc-Temperature),5.0)
			    	  	  	  	  	  	  	   	   + a6_PARA * pow((Tc-Temperature),6.0)
			    							   	   + a7_PARA * pow((Tc-Temperature),7.0)
			    							   	   + a8_PARA * pow((Tc-Temperature),8.0)
			    	  	  	  	  	  	  	   	   + a9_PARA * pow((Tc-Temperature),9.0);
				}
				else{LLB->Chi_para[grain_in_sys] = b0_PARA * (Tc/ (4.0*PI*(Temperature-Tc)) );}
				// Chi_para must not be a negative value.
				if(LLB->Chi_para[grain_in_sys]<0){LLB->Chi_para[grain_in_sys] -= 1.1*LLB->Chi_para[grain_in_sys];}
				LLB->Chi_para[grain_in_sys] *= 1.000e-4*Chi_scaling_factor;

				if(Temperature<1.068*Tc){
					LLB->Chi_perp[grain_in_sys] = a0_PERP
			    							   	   + a1_PERP*(1.0 - (Temperature/(1.068*Tc)))
			    							   	   + a2_PERP*pow((1.0 - (Temperature/(1.068*Tc))),2.0)
			    	  	  	  	  	  	  	   	   + a3_PERP*pow((1.0 - (Temperature/(1.068*Tc))),3.0)
			    	  	  	  	  	  	  	   	   + a4_PERP*pow((1.0 - (Temperature/(1.068*Tc))),4.0)
			    	  	  	  	  	  	  	   	   + a5_PERP*pow((1.0 - (Temperature/(1.068*Tc))),5.0)
			    	  	  	  	  	  	  	   	   + a6_PERP*pow((1.0 - (Temperature/(1.068*Tc))),6.0)
			    	  	  	  	  	  	  	   	   + a7_PERP*pow((1.0 - (Temperature/(1.068*Tc))),7.0)
			    	  	  	  	  	  	  	   	   + a8_PERP*pow((1.0 - (Temperature/(1.068*Tc))),8.0)
			    	  	  	  	  	  	  	   	   + a9_PERP*pow((1.0 - (Temperature/(1.068*Tc))),9.0);
				}
				else{LLB->Chi_perp[grain_in_sys]= b0_PERP * (Tc/ (4.0*PI*(Temperature-Tc)) );}
				LLB->Chi_perp[grain_in_sys] *= 1.000e-4*Chi_scaling_factor;
			}
//==================== If susceptibility_type == vogler_scusceptibility_fit ==================================//
			else if(LLB->Susceptibility_Type[Layer]=="vogler_scusceptibility_fit"){
				// Determine first the inverse of susceptibility
				if(Temperature<Tc){
					LLB->Chi_para[grain_in_sys] = a0_PARA/(Tc-Temperature);
					LLB->Chi_perp[grain_in_sys] = a0_PERP
												+ a1_PERP*LLB->m_EQ[grain_in_sys]
												+ a2_PERP*pow(LLB->m_EQ[grain_in_sys],2.0)
												+ a3_PERP*pow(LLB->m_EQ[grain_in_sys],3.0)
												+ a4_PERP*pow(LLB->m_EQ[grain_in_sys],4.0);
				}
				// if T>=Tc
				else if(Temperature>=Tc){
					LLB->Chi_para[grain_in_sys] = b0_PARA/(Temperature-Tc);
					LLB->Chi_perp[grain_in_sys] = b0_PERP/(Temperature-Tc);
				}
				// Convert susceptibility in CGS units passing from 1/T to 1/Oe by *1e-4 T/Oe and rescaling by the scaling factor if any
				LLB->Chi_para[grain_in_sys] *= 1.000e-4*Chi_scaling_factor;
				LLB->Chi_perp[grain_in_sys] *= 1.000e-4*Chi_scaling_factor;
			}
//==================== If susceptibility_type == inv_scusceptibility_fit =====================================//
			else{
				// Determine first the inverse of the susceptibility
				if(Temperature<Tc){
					LLB->Chi_para[grain_in_sys] = a0_PARA
			    							  	  + a1_PARA  * (Tc-Temperature)/Tc
			    							  	  + a2_PARA  * pow((Tc-Temperature)/Tc,2)
			    							  	  + a3_PARA  * pow((Tc-Temperature)/Tc,3)
			    							  	  + a4_PARA  * pow((Tc-Temperature)/Tc,4)
			    							  	  + a5_PARA  * pow((Tc-Temperature)/Tc,5)
			    	  	  	  	  	  	  	  	  + a6_PARA  * pow((Tc-Temperature)/Tc,6)
			    							  	  + a7_PARA  * pow((Tc-Temperature)/Tc,7)
			    							  	  + a8_PARA  * pow((Tc-Temperature)/Tc,8)
			    	  	  	  	  	  	  	  	  + a9_PARA  * pow((Tc-Temperature)/Tc,9)
												  + a1_2_PARA * pow((Tc-Temperature)/Tc,0.5);

					LLB->Chi_perp[grain_in_sys] = a0_PERP
			    	  	  	  	  	  	  	  	  + a1_2_PERP * pow((Tc-Temperature)/Tc,0.5)
			    							  	  + a1_PERP   * (Tc-Temperature)/Tc
			    							  	  + a2_PERP   * pow((Tc-Temperature)/Tc,2)
			    							  	  + a3_PERP   * pow((Tc-Temperature)/Tc,3)
			    							  	  + a4_PERP   * pow((Tc-Temperature)/Tc,4)
			    							  	  + a5_PERP   * pow((Tc-Temperature)/Tc,5)
			    	  	  	  	  	  	  	  	  + a6_PERP   * pow((Tc-Temperature)/Tc,6)
			    							  	  + a7_PERP   * pow((Tc-Temperature)/Tc,7)
			    							  	  + a8_PERP   * pow((Tc-Temperature)/Tc,8)
			    	  	  	  	  	  	  	  	  + a9_PERP   * pow((Tc-Temperature)/Tc,9);
				}
				// if T>=Tc
				else{
					LLB->Chi_para[grain_in_sys] = b0_PARA
			    							  	  + b1_PARA * (Temperature-Tc)/Tc
			    							  	  + b2_PARA * pow((Temperature-Tc)/Tc,2)
			    							  	  + b3_PARA * pow((Temperature-Tc)/Tc,3)
			    							  	  + b4_PARA * pow((Temperature-Tc)/Tc,4);

					LLB->Chi_perp[grain_in_sys] = b0_PERP
			    							  	  + b1_PERP * (Temperature-Tc)/Tc
			    							  	  + b2_PERP * pow((Temperature-Tc)/Tc,2)
			    							  	  + b3_PERP * pow((Temperature-Tc)/Tc,3)
			    							  	  + b4_PERP * pow((Temperature-Tc)/Tc,4);
				}
				// Calculate susceptibility by 1/LLB->Chi_para and 1/LLB->Chi_perp
				LLB->Chi_para[grain_in_sys] = 1.0/LLB->Chi_para[grain_in_sys];
				LLB->Chi_perp[grain_in_sys] = 1.0/LLB->Chi_perp[grain_in_sys];
				// Convert susceptibility in CGS units passing from 1/T to 1/Oe by *1e-4 T/Oe and rescaling by the scaling factor
				LLB->Chi_para[grain_in_sys] *= 1.000e-4*Chi_scaling_factor;
				LLB->Chi_perp[grain_in_sys] *= 1.000e-4*Chi_scaling_factor;
			}
		}
	}
	return 0;
}


