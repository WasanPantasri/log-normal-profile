/*
 * KMC_test.cpp
 *
 *  Created on: 13 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file KMC_Solver.cpp
 * \brief kMC solver function, used to set up and call kMC core. */
#include "../../hdr/Solvers/KMC_core.hpp"
#include "../../hdr/Structures.hpp"

#include "math.h"
#include <iostream>
#include <string>
/**
 *
 */
int KMC_solver(const int Num_Grains,const int Num_Layers,const Interaction_t Interac,const double tm,const bool ZeroKelvin, const bool INC_M_LENGTH, double F0, Grain_t*Grain,const std::string Sim=""){

	std::vector<double> m_EQ_store;

	for(int Layer=0;Layer<Num_Layers;++Layer){
		int offset = Layer*Num_Grains;
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			int grain_in_system = offset+grain_in_layer;

			double Ms, K, Vol, kBT, Temp, Tc, m_EQ=1.0;
			double Hk, KV_over_kBT, MsVHk_over_kBT;
			std::vector<double> Spin(3,0.0),Easy_axis(3,0.0),
								H_appl(3,0.0),H_magneto(3,0.0),H_exch(3,0.0),h_eff(3,0.0),
								m_EQ_exch_neigh(Interac.Exchange_neigh_list[grain_in_system].size(),1.0),
								m_EQ_magneto_neigh(Interac.Magneto_neigh_list[grain_in_system].size(),1.0);


			Spin[0] = Grain->m[grain_in_system].x;
			Spin[1] = Grain->m[grain_in_system].y;
			Spin[2] = Grain->m[grain_in_system].z;
			Easy_axis[0] = Grain->Easy_axis[grain_in_system].x;
			Easy_axis[1] = Grain->Easy_axis[grain_in_system].y;
			Easy_axis[2] = Grain->Easy_axis[grain_in_system].z;
			H_appl[0] = Grain->H_appl[grain_in_system].x;
			H_appl[1] = Grain->H_appl[grain_in_system].y;
			H_appl[2] = Grain->H_appl[grain_in_system].z;
			Temp = Grain->Temp[grain_in_system];
			Tc   = Grain->Tc[grain_in_system];
			Vol  = Grain->Vol[grain_in_system]*1e-21;
			kBT  = (KB*Temp);


//############################## DETERMINE TEMPERATURE DEPENDENT VALUES ##############################//
			if(ZeroKelvin==false){		// Case where experimental values are used.
				K = Grain->K[grain_in_system];
				Ms = Grain->Ms[grain_in_system];
			}
			else{		// Case where 0-Kelvin values are used.
				//#################### FOR CURRENT GRAIN ####################//
				/* Special case required for Sigma_Tc simulations.
				 * When m_EQ=0 KMC breaks due to attempted zero division.
				 * To prevent this m_EQ is set to a very small value instead of zero. */
				if(Temp>=0.99999*Tc && Sim=="Sigma_Tc"){
					m_EQ = pow(1.0-(0.99999),Grain->Crit_exp[grain_in_system]);
				}
				else if(Temp>=Tc){
					m_EQ = 0.0;
					std::cout << "KMC DOES NOT WORK BEYOND Tc!!!" << std::endl;
				}
				else{
					m_EQ = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_system]);
				}
				K = Grain->K[grain_in_system]*pow(m_EQ,Grain->Callen_power[grain_in_system]);
				Ms = Grain->Ms[grain_in_system]*m_EQ;
			}
			//#################### FOR GRAIN NEIGHBOURS ####################//
			// Exchange neighbours
			for(size_t neigh=0;neigh<m_EQ_exch_neigh.size();++neigh){
				int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_system][neigh];
				double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
				double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
				double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

				if(Temp_neigh>=0.99999*Tc_neigh && Sim=="Sigma_Tc"){m_EQ_exch_neigh[neigh] = pow(1.0-(0.99999),Crit_exp_neigh);}
				else if(Temp_neigh>=Tc_neigh){m_EQ_exch_neigh[neigh]=0.0;}
				else{m_EQ_exch_neigh[neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);}
			}
			// Magnetostatic neighbours
			for(size_t neigh=0;neigh<m_EQ_magneto_neigh.size();++neigh){
				int NEIGHBOUR_ID = Interac.Magneto_neigh_list[grain_in_system][neigh];
				double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
				double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
				double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

				if(Temp_neigh>=0.99999*Tc_neigh && Sim=="Sigma_Tc"){m_EQ_exch_neigh[neigh] = pow(1.0-(0.99999),Crit_exp_neigh);}
				else if(Temp_neigh>=Tc_neigh){m_EQ_exch_neigh[neigh]=0.0;}
				else{m_EQ_exch_neigh[neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);}
			}
//####################################################################################################//

			Hk = (2.0*K)/Ms;
			KV_over_kBT = (K*Vol)/kBT;
			MsVHk_over_kBT = (Ms*Vol*Hk)/kBT;

			for(size_t neigh=0;neigh<Interac.Wxx[grain_in_system].size();++neigh){
				int NEIGHBOUR_ID = Interac.Magneto_neigh_list[grain_in_system][neigh];

				H_magneto[0] += Grain->Ms[grain_in_system]*(Interac.Wxx[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_exch_neigh[neigh]+
															Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_exch_neigh[neigh]+
															Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_exch_neigh[neigh]);

				H_magneto[1] += Grain->Ms[grain_in_system]*(Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_exch_neigh[neigh]+
															Interac.Wyy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_exch_neigh[neigh]+
															Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_exch_neigh[neigh]);

				H_magneto[2] += Grain->Ms[grain_in_system]*(Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_exch_neigh[neigh]+
															Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_exch_neigh[neigh]+
															Interac.Wzz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_exch_neigh[neigh]);

			}
			for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_system].size();++neigh){
				int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_system][neigh];
				H_exch[0] += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_exch_neigh[neigh];
				H_exch[1] += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_exch_neigh[neigh];
				H_exch[2] += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_exch_neigh[neigh];
			}
			// Using <Hk>*Hk_rel will be better than absolute Hk.
			h_eff[0] = (H_magneto[0] + H_exch[0] + H_appl[0])/(Hk);
			h_eff[1] = (H_magneto[1] + H_exch[1] + H_appl[1])/(Hk);
			h_eff[2] = (H_magneto[2] + H_exch[2] + H_appl[2])/(Hk);

			// Perform KMC for a single grain
			KMC_core(Easy_axis,h_eff,KV_over_kBT,MsVHk_over_kBT,tm,F0,&Spin);

			// Now save spins
			Grain->m[grain_in_system].x = Spin[0];
			Grain->m[grain_in_system].y = Spin[1];
			Grain->m[grain_in_system].z = Spin[2];
			// Save m_EQ for the current grain
			m_EQ_store.push_back(m_EQ);

	}	}

	// Output magnetisation values with correct lengths if desired -- allows for easier transfer between KMC and LLB solvers
	if(INC_M_LENGTH==true){
		for(size_t grain_in_system=0;grain_in_system<Grain->m.size();++grain_in_system){
			Grain->m[grain_in_system].x *= m_EQ_store[grain_in_system];
			Grain->m[grain_in_system].y *= m_EQ_store[grain_in_system];
			Grain->m[grain_in_system].z *= m_EQ_store[grain_in_system];
	}	}
	return 0;
}


