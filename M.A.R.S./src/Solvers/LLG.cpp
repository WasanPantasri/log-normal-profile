/* LLG.cpp
 *  Created on: 30 Apr 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file LLG.cpp
 * \brief Function for Landau-Lifshitz-Gilbert solver
 *
 * This function performs a single time step using the LLG equation of motion. This model requires normalisation of the magnetisation thus
 * it breaks down when \f$T \xrightarrow{} T_c\f$ as the ferromagnetic ordering is lost.
 * As a result any simulations which require high temperatures must use the LLB solver instead. <BR>
 * The LLG solver is useful for temperature independent simulations as it utilises larger time steps than the LLB thus reducing computation
 * time. <BR>
 * The maximum possible time step is \f$3ps\f$, however \f$1ps\f$ is the most reliable. (See LLG time step test).
 * <P> The equation of motion is:
 * \f[
 * 	    \frac{\partial{\vec{m}^i}}{\partial{t}} = -\frac{\gamma_e}{1+\alpha^2} \left(\vec{m}^i \times \vec{H_{eff}}^i \right)-\frac{\alpha\gamma_e}{M_s\left(1+\alpha^2\right)} \vec{m}^i \times \left(\vec{m}^i \times \vec{H_{eff}}^i \right)
 * \f]
 * \f$\gamma_e\f$ is the electron gyromagnetic ratio. \f$|\gamma_e|=1.760859644\times10^7 \frac{rad}{Oes}\f$. <BR>
 * \f$\alpha\f$ is the damping constant. <BR>
 * \f$M_{s}\f$ is the saturation magnetisation in emu\cc. <BR>
 * \f$H_{eff}\f$ is the effective field in Oe, which accounts for thermal effects, interactions and applied fields. <BR>
 * \f$\vec{m}\f$ is the magnetisation unit vector. <BR>
 */

#include <iostream>
#include <vector>
#include "math.h"
#include <string>

#include "../../hdr/Structures.hpp"
#include "../../hdr/RNG/Random.hpp"
/** This function acts on all grains within the system. Grain_t.m is updated for all grains once the
 * calculation is completed. <BR>
 * There are two special settings:
 * <UL>
 * 		<LI> <B> ZeroKelvin </B> is a flag used to set the temperature dependence of anisotropy and
 * 			 saturation magnetisation. When set to <B><EM> False </EM></B> the values are kept
 * 			 constant, however when set to <B><EM> True </EM></B> these values are adjusted with
 * 			 temperature similar to in the LLB solver.
 * 		<LI> <B> Sim </B> is used  only when ZeroKelvin is <B><EM> True </EM></B> and sets special
 * 			 behaviour for Sigma_Tc simulations. <B><EM>By default this setting is off.</EM></B><BR>
 * 			 This setting enables the use of temperatures above \f$T_c\f$ by preventing NaNs. <BR>
 * 			 The use of this setting is not advised without prior investigation into its effects
 * 			 and validity for your simulation.
 *
 */
int LL_G(const int Num_Grains,const int Num_Layers,const Interaction_t Interac,const Voronoi_t VORO,const LLG_t LLG,const bool ZeroKelvin,Grain_t*Grain,const std::string Sim=""){

	// Set total number of grains in system
	double Tot_Grains = Num_Grains*Num_Layers;

	// Internal variables
	double Offset;
	int grain_in_system;
	double MdotE, MagM, m_X_dummy, m_Y_dummy, m_Z_dummy, m_X_dummy_2, m_Y_dummy_2, m_Z_dummy_2,
		   H_eff_X_dummy, H_eff_Y_dummy, H_eff_Z_dummy,Alpha_dummy;
	Vec3 MxH, MxMxH;

	std::vector<double> H_ani (Tot_Grains);
	std::vector<Vec3> M_initial (Tot_Grains), dmdt1 (Tot_Grains), dmdt2 (Tot_Grains),
					  Therm_field (Tot_Grains),H_eff (Tot_Grains), H_magneto (Tot_Grains),
					  H_exchange (Tot_Grains);

	//-- READ IN MAGNETISATION DATA
	for(int i=0;i<Tot_Grains;++i){
		M_initial[i].x = Grain->m[i].x;
		M_initial[i].y = Grain->m[i].y;
		M_initial[i].z = Grain->m[i].z;
	}


	// Determine Ms(t) and K(t)
	std::vector<double> K_t(Tot_Grains);
	std::vector<double> Ms_t(Tot_Grains);

	if(ZeroKelvin==false){		// Case where experimental values are used. - NO Ms(t) and K(t)
		for(int Layer=0;Layer<Num_Layers;++Layer){
			Offset = Num_Grains*Layer;
			for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
				grain_in_system = grain_in_layer+Offset;
				K_t[grain_in_system]= Grain->K[grain_in_system];
				Ms_t[grain_in_system]=Grain->Ms[grain_in_system];
			}
		}
	}
	else{
		std::vector<double> m_EQ(Tot_Grains);

		/* This is a WIP improvement to the LLG to add basic
		 * K(T) and Ms(T) information.
		 * Future plan is to update this to use the extended Callen-Callen
		 * as used by the LLB function instead of just single scaling. */
		for(int Layer=0;Layer<Num_Layers;++Layer){
			Offset = Num_Grains*Layer;
			for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
				grain_in_system = grain_in_layer+Offset;
				double Temp = Grain->Temp[grain_in_system];
				double Tc = Grain->Tc[grain_in_system];
				if(Temp>=0.99999*Tc && Sim=="Sigma_Tc"){  // Special case required for Sigma_Tc simulations.
					m_EQ[grain_in_system] = pow(1.0-(0.99999),Grain->Crit_exp[grain_in_system]);
				}
				else if(Temp>=Tc){
					m_EQ[grain_in_system]=0.0;
				}
				else{
					m_EQ[grain_in_system] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_system]);
				}
				K_t[grain_in_system]=Grain->K[grain_in_system]*pow(m_EQ[grain_in_system],Grain->Callen_power[grain_in_system]);
				Ms_t[grain_in_system]=Grain->Ms[grain_in_system]*m_EQ[grain_in_system];
			}
		}
	}



//################################################## DETERMINE STOCHASTIC TERMS  --- CURRENTLY ONLY FOR A SINGLE LAYER ##################################################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		Offset = Num_Grains*Layer;
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			grain_in_system = grain_in_layer+Offset;

			double RNG_MAG = sqrt( ((2.0*LLG.Alpha[Layer]*KB*Grain->Temp[grain_in_system])/(LLG.Gamma*Ms_t[grain_in_system]*(Grain->Vol[grain_in_system]*1e-21)))/LLG.dt );

			Therm_field[grain_in_system].x = mtrandom::gaussian()*RNG_MAG;
			Therm_field[grain_in_system].y = mtrandom::gaussian()*RNG_MAG;
			Therm_field[grain_in_system].z = mtrandom::gaussian()*RNG_MAG;

			H_ani[grain_in_system] = (2.0*K_t[grain_in_system])/Ms_t[grain_in_system];
	}	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		MdotE = Grain->m[grain_in_system].x*Grain->Easy_axis[grain_in_system].x + Grain->m[grain_in_system].y*Grain->Easy_axis[grain_in_system].y + Grain->m[grain_in_system].z*Grain->Easy_axis[grain_in_system].z;
		H_eff[grain_in_system].x = H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].x + Grain->H_appl[grain_in_system].x;
		H_eff[grain_in_system].y = H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].y + Grain->H_appl[grain_in_system].y;
		H_eff[grain_in_system].z = H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].z + Grain->H_appl[grain_in_system].z;
	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_magneto[grain_in_system].x = H_magneto[grain_in_system].y = H_magneto[grain_in_system].z = 0.0;
		for(size_t neigh=0;neigh<Interac.Wxx[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Magneto_neigh_list[grain_in_system][neigh];
			H_magneto[grain_in_system].x += Ms_t[grain_in_system]*(Interac.Wxx[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x+
																		Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y+
																		Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z);

			H_magneto[grain_in_system].y += Ms_t[grain_in_system]*(Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x+
																		Interac.Wyy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z);

			H_magneto[grain_in_system].z += Ms_t[grain_in_system]*(Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y+
																		Interac.Wzz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z);
		}
		H_exchange[grain_in_system].x = H_exchange[grain_in_system].y = H_exchange[grain_in_system].z = 0.0;
		for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_system][neigh];
			H_exchange[grain_in_system].x += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x;
			H_exchange[grain_in_system].y += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y;
			H_exchange[grain_in_system].z += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z;
		}
	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_eff[grain_in_system].x += H_magneto[grain_in_system].x + H_exchange[grain_in_system].x;
		H_eff[grain_in_system].y += H_magneto[grain_in_system].y + H_exchange[grain_in_system].y;
		H_eff[grain_in_system].z += H_magneto[grain_in_system].z + H_exchange[grain_in_system].z;
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### FIRST HEUN STEP ###########################################################################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		Offset = Num_Grains*Layer;
		Alpha_dummy = LLG.Alpha[Layer];
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			grain_in_system = grain_in_layer+Offset;

			m_X_dummy = Grain->m[grain_in_system].x;
			m_Y_dummy = Grain->m[grain_in_system].y;
			m_Z_dummy = Grain->m[grain_in_system].z;

			H_eff_X_dummy = H_eff[grain_in_system].x + Therm_field[grain_in_system].x;
			H_eff_Y_dummy = H_eff[grain_in_system].y + Therm_field[grain_in_system].y;
			H_eff_Z_dummy = H_eff[grain_in_system].z + Therm_field[grain_in_system].z;

			MxH.x = m_Y_dummy*H_eff_Z_dummy - m_Z_dummy*H_eff_Y_dummy;
			MxH.y = m_Z_dummy*H_eff_X_dummy - m_X_dummy*H_eff_Z_dummy;
			MxH.z = m_X_dummy*H_eff_Y_dummy - m_Y_dummy*H_eff_X_dummy;
			MxMxH.x = m_Y_dummy*MxH.z - m_Z_dummy*MxH.y;
			MxMxH.y = m_Z_dummy*MxH.x - m_X_dummy*MxH.z;
			MxMxH.z = m_X_dummy*MxH.y - m_Y_dummy*MxH.x;

			dmdt1[grain_in_system].x = (-LLG.Gamma/(1.0+Alpha_dummy*Alpha_dummy))*(MxH.x+Alpha_dummy*MxMxH.x);
			dmdt1[grain_in_system].y = (-LLG.Gamma/(1.0+Alpha_dummy*Alpha_dummy))*(MxH.y+Alpha_dummy*MxMxH.y);
			dmdt1[grain_in_system].z = (-LLG.Gamma/(1.0+Alpha_dummy*Alpha_dummy))*(MxH.z+Alpha_dummy*MxMxH.z);

			Grain->m[grain_in_system].x = m_X_dummy + dmdt1[grain_in_system].x*LLG.dt;
			Grain->m[grain_in_system].y = m_Y_dummy + dmdt1[grain_in_system].y*LLG.dt;
			Grain->m[grain_in_system].z = m_Z_dummy + dmdt1[grain_in_system].z*LLG.dt;

			// Normalise
			MagM = sqrt(Grain->m[grain_in_system].x*Grain->m[grain_in_system].x+Grain->m[grain_in_system].y*Grain->m[grain_in_system].y+Grain->m[grain_in_system].z*Grain->m[grain_in_system].z);
			Grain->m[grain_in_system].x /= MagM;
			Grain->m[grain_in_system].y /= MagM;
			Grain->m[grain_in_system].z /= MagM;
	}	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		MdotE = Grain->m[grain_in_system].x*Grain->Easy_axis[grain_in_system].x + Grain->m[grain_in_system].y*Grain->Easy_axis[grain_in_system].y + Grain->m[grain_in_system].z*Grain->Easy_axis[grain_in_system].z;
		H_eff[grain_in_system].x = H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].x + Grain->H_appl[grain_in_system].x;
		H_eff[grain_in_system].y = H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].y + Grain->H_appl[grain_in_system].y;
		H_eff[grain_in_system].z = H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].z + Grain->H_appl[grain_in_system].z;
	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_magneto[grain_in_system].x = H_magneto[grain_in_system].y = H_magneto[grain_in_system].z = 0.0;
		for(size_t neigh=0;neigh<Interac.Wxx[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Magneto_neigh_list[grain_in_system][neigh];
			H_magneto[grain_in_system].x += Ms_t[grain_in_system]*(Interac.Wxx[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x+
																		Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y+
																		Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z);

			H_magneto[grain_in_system].y += Ms_t[grain_in_system]*(Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x+
																		Interac.Wyy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z);

			H_magneto[grain_in_system].z += Ms_t[grain_in_system]*(Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y+
																		Interac.Wzz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z);
		}
		H_exchange[grain_in_system].x = H_exchange[grain_in_system].y = H_exchange[grain_in_system].z = 0.0;
		for(size_t neigh=0;neigh<Interac.H_exch_str[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_system][neigh];
			H_exchange[grain_in_system].x += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x;
			H_exchange[grain_in_system].y += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y;
			H_exchange[grain_in_system].z += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z;
		}
	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_eff[grain_in_system].x += H_magneto[grain_in_system].x + H_exchange[grain_in_system].x;
		H_eff[grain_in_system].y += H_magneto[grain_in_system].y + H_exchange[grain_in_system].y;
		H_eff[grain_in_system].z += H_magneto[grain_in_system].z + H_exchange[grain_in_system].z;
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### SECOND HEUN STEP ##########################################################################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		Offset = Num_Grains*Layer;
		Alpha_dummy = LLG.Alpha[Layer];
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			grain_in_system = grain_in_layer+Offset;

			m_X_dummy_2 = Grain->m[grain_in_system].x;
			m_Y_dummy_2 = Grain->m[grain_in_system].y;
			m_Z_dummy_2 = Grain->m[grain_in_system].z;

			H_eff_X_dummy = H_eff[grain_in_system].x + Therm_field[grain_in_system].x;
			H_eff_Y_dummy = H_eff[grain_in_system].y + Therm_field[grain_in_system].y;
			H_eff_Z_dummy = H_eff[grain_in_system].z + Therm_field[grain_in_system].z;

			MxH.x = m_Y_dummy_2*H_eff_Z_dummy - m_Z_dummy_2*H_eff_Y_dummy;
			MxH.y = m_Z_dummy_2*H_eff_X_dummy - m_X_dummy_2*H_eff_Z_dummy;
			MxH.z = m_X_dummy_2*H_eff_Y_dummy - m_Y_dummy_2*H_eff_X_dummy;
			MxMxH.x = m_Y_dummy_2*MxH.z - m_Z_dummy_2*MxH.y;
			MxMxH.y = m_Z_dummy_2*MxH.x - m_X_dummy_2*MxH.z;
			MxMxH.z = m_X_dummy_2*MxH.y - m_Y_dummy_2*MxH.x;

			dmdt2[grain_in_system].x = (-LLG.Gamma/(1.0+Alpha_dummy*Alpha_dummy))*(MxH.x+Alpha_dummy*MxMxH.x);
			dmdt2[grain_in_system].y = (-LLG.Gamma/(1.0+Alpha_dummy*Alpha_dummy))*(MxH.y+Alpha_dummy*MxMxH.y);
			dmdt2[grain_in_system].z = (-LLG.Gamma/(1.0+Alpha_dummy*Alpha_dummy))*(MxH.z+Alpha_dummy*MxMxH.z);

			Grain->m[grain_in_system].x = M_initial[grain_in_system].x + (dmdt1[grain_in_system].x+dmdt2[grain_in_system].x)*LLG.dt*0.5;
			Grain->m[grain_in_system].y = M_initial[grain_in_system].y + (dmdt1[grain_in_system].y+dmdt2[grain_in_system].y)*LLG.dt*0.5;
			Grain->m[grain_in_system].z = M_initial[grain_in_system].z + (dmdt1[grain_in_system].z+dmdt2[grain_in_system].z)*LLG.dt*0.5;

			// Normalise
			MagM = sqrt(Grain->m[grain_in_system].x*Grain->m[grain_in_system].x+Grain->m[grain_in_system].y*Grain->m[grain_in_system].y+Grain->m[grain_in_system].z*Grain->m[grain_in_system].z);
			Grain->m[grain_in_system].x /= MagM;
			Grain->m[grain_in_system].y /= MagM;
			Grain->m[grain_in_system].z /= MagM;
	}	}
	return 0;
}
