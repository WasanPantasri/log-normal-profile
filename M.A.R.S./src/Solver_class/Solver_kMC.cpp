/*
 * Solver_kMC.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** \file KMC_Solver.cpp
 * \brief kMC solver function, used to set up and call kMC core. */

#include "../../hdr/Solver.h"

#include "math.h"
#include <iostream>
#include <string>
/**
 *
 */
int Solver_t::KMC(const unsigned int Num_Layers,const Interaction_t*Interac,
                const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain){

    std::vector<unsigned int> Included_grains_in_layer;
    std::vector<unsigned int> Included_grains_in_system;
    if(Exclusion==false){
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            Included_grains_in_layer.push_back(g);
            Included_grains_in_system.push_back(g);
        }
        for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
            unsigned int offset=VORO->Num_Grains*L;
            for(unsigned int g=0;g<VORO->Num_Grains;++g){
                Included_grains_in_system.push_back(g+offset);
            }
        }
    }
    else{
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            double Distance_X=0.0, Distance_Y=0.0;
            bool In_X=false;
            // Check if within X boundaries
            if(VORO->Pos_X_final[g]>Centre_x){// Positive X boundary
                Distance_X = VORO->Pos_X_final[g] - Centre_x;
                if(Distance_X<=Exclusion_range_posX){In_X=true;}
            }
            else if(VORO->Pos_X_final[g]<Centre_x){// Negative X boundary
                Distance_X = Centre_x - VORO->Pos_X_final[g];
                if(Distance_X<=Exclusion_range_negX){In_X=true;}
            }
            else{In_X=true;}
            // Check Y boundaries if within X boundaries
            if(In_X==true){
                if(VORO->Pos_Y_final[g]>Centre_y){// Positive Y boundary
                    Distance_Y = VORO->Pos_Y_final[g] - Centre_y;
                    if(Distance_Y<=Exclusion_range_posY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else if(VORO->Pos_Y_final[g]<Centre_y){// Negative Y boundary
                    Distance_Y = Centre_y - VORO->Pos_Y_final[g];
                    if(Distance_Y<=Exclusion_range_negY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else{
                    Included_grains_in_layer.push_back(g);
                    Included_grains_in_system.push_back(g);
                }
            }
            for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
                unsigned int offset=VORO->Num_Grains*L;
                for(size_t g=0;g<Included_grains_in_layer.size();++g){
                    Included_grains_in_system.push_back(Included_grains_in_layer[g]+offset);
                }
            }
        }
    }
    size_t Integratable_grains_in_layer = Included_grains_in_layer.size();
    size_t Integratable_grains_in_system = Included_grains_in_system.size();

    m_EQ.resize(Integratable_grains_in_system);

    // Internal variables
    unsigned int Offset;
    unsigned int grain_in_system;
    std::vector<Vec3> H_eff (Integratable_grains_in_system), H_magneto (Integratable_grains_in_system),
                      H_exchange (Integratable_grains_in_system);

    // Determine Ms(t) and K(t)
    std::vector<double> K_t(Integratable_grains_in_system,0.0);
    std::vector<double> Ms_t(Integratable_grains_in_system,0.0);
    std::vector<std::vector<double>> m_EQ_exch_neigh(Integratable_grains_in_system);
    std::vector<std::vector<double>> m_EQ_magneto_neigh(Integratable_grains_in_system);

    if(!T_variation){       // Case where experimental values are used. - NO Ms(t) and K(t)
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            Offset = Integratable_grains_in_layer*Layer;
            for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
                grain_in_system = grain_in_layer+Offset;
                unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
                K_t[grain_in_system]= Grain->K[grain_in_sys];
                Ms_t[grain_in_system]=Grain->Ms[grain_in_sys];
                for(size_t neigh=0;neigh<Interac->Exchange_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_exch_neigh[grain_in_system].push_back(1.0);
                }
                for(size_t neigh=0;neigh<Interac->Magneto_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_magneto_neigh[grain_in_system].push_back(1.0);
                }
            }
        }
    }
    else{
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            Offset = Integratable_grains_in_layer*Layer;
            for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
                grain_in_system = grain_in_layer+Offset;
                unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
                double Temp = Grain->Temp[grain_in_sys];
                double Tc = Grain->Tc[grain_in_sys];

                for(size_t neigh=0;neigh<Interac->Exchange_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_exch_neigh[grain_in_system].push_back(1.0);
                }
                for(size_t neigh=0;neigh<Interac->Magneto_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_magneto_neigh[grain_in_system].push_back(1.0);
                }

                switch(behaviour_kMC)
                {
                case Behaviours::Thermoremanence:
                    if(Temp>=0.99999*Tc){
                        m_EQ[grain_in_system] = pow(1.0-(0.99999),Grain->Crit_exp[grain_in_sys]);
                    } else {
                        m_EQ[grain_in_system] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_sys]);
                    }
                    break;
                case Behaviours::Standard:
                    if(Temp>=Tc){
                        m_EQ[grain_in_system]=0.0;
                    } else {
                        m_EQ[grain_in_system] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_sys]);
                    }
                    break;
                }

                // Callen-Callen
                if (Temp <= Grain->Callen_range_lowT[grain_in_sys]){
                    K_t[grain_in_system]=Grain->K[grain_in_sys]*Grain->Callen_factor_lowT[grain_in_sys] * pow(m_EQ[grain_in_sys],Grain->Callen_power_lowT[grain_in_sys]);
                }
                else if (Temp > Grain->Callen_range_midT[grain_in_sys]){
                    K_t[grain_in_system]=Grain->K[grain_in_sys]*Grain->Callen_factor_highT[grain_in_sys] * pow(m_EQ[grain_in_sys],Grain->Callen_power_highT[grain_in_sys]);
                } else {
                    K_t[grain_in_system]=Grain->K[grain_in_sys]*Grain->Callen_factor_midT[grain_in_sys] * pow(m_EQ[grain_in_sys],Grain->Callen_power_midT[grain_in_sys]);
                }

                Ms_t[grain_in_system]=Grain->Ms[grain_in_sys]*m_EQ[grain_in_sys];

                //#################### FOR GRAIN NEIGHBOURS ####################//
                // Exchange neighbours
                for(size_t neigh=0;neigh<m_EQ_exch_neigh[grain_in_system].size();++neigh){
                    int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
                    double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
                    double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
                    double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

                    switch(behaviour_kMC)
                    {
                    case Behaviours::Thermoremanence:
                        if(Temp_neigh>=0.99999*Tc_neigh){
                            m_EQ_exch_neigh[grain_in_system][neigh] = pow(1.0-(0.99999),Crit_exp_neigh);
                        } else {
                            m_EQ_exch_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    case Behaviours::Standard:
                        if(Temp_neigh>=Tc_neigh){
                            m_EQ_exch_neigh[grain_in_system][neigh]=0.0;
                        } else {
                            m_EQ_exch_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    }
                }
                // Magnetostatic neighbours
                for(size_t neigh=0;neigh<m_EQ_magneto_neigh[grain_in_system].size();++neigh){
                    int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
                    double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
                    double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
                    double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

                    switch(behaviour_kMC)
                    {
                    case Behaviours::Thermoremanence:
                        if(Temp_neigh>=0.99999*Tc_neigh){
                            m_EQ_magneto_neigh[grain_in_system][neigh] = pow(1.0-(0.99999),Crit_exp_neigh);
                        } else {
                            m_EQ_magneto_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    case Behaviours::Standard:
                        if(Temp_neigh>=Tc_neigh){
                            m_EQ_magneto_neigh[grain_in_system][neigh]=0.0;
                        } else {
                            m_EQ_magneto_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    }
                }
            }
        }
    }

//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        H_magneto[grain_in_system]=0.0;
        for(size_t neigh=0;neigh<Interac->Wxx[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
            H_magneto[grain_in_system].x += Ms_t[grain_in_system]*(Interac->Wxx[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);
            H_magneto[grain_in_system].y += Ms_t[grain_in_system]*(Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);
            H_magneto[grain_in_system].z += Ms_t[grain_in_system]*(Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wzz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);
        }
        H_exchange[grain_in_system]=0.0;
        for(size_t neigh=0;neigh<Interac->H_exch_str[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
            H_exchange[grain_in_system]+=Interac->H_exch_str[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID]*m_EQ_exch_neigh[grain_in_system][neigh];
        }
    }
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        H_eff[grain_in_system] = Grain->H_appl[grain_in_sys] + H_magneto[grain_in_system] + H_exchange[grain_in_system];
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain

        double Hk = (2.0*K_t[grain_in_system])/Ms_t[grain_in_system];
        double KV_over_kBT = (K_t[grain_in_system]*Grain->Vol[grain_in_sys]*1.0e-21)/(KB*Grain->Temp[grain_in_sys]);
        double MsVHk_over_kBT = (Ms_t[grain_in_system]*Grain->Vol[grain_in_sys]*1.0e-21*Hk)/(KB*Grain->Temp[grain_in_sys]);

        // Perform KMC for a single grain
        KMC_core(Grain->Easy_axis[grain_in_sys],(H_eff[grain_in_system]/Hk),KV_over_kBT,MsVHk_over_kBT,&Grain->m[grain_in_sys]);

        // Output magnetisation values with correct lengths if desired -- allows for easier transfer between KMC and LLB solvers
        if(M_length_variation){
            Grain->m[grain_in_sys] *= m_EQ[grain_in_system];
        }
    }
    return 0;
}
