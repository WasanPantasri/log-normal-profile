/*
 * Solver.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Ewan Rannala
 */

#include "../../hdr/Solver.h"

#include <iostream>
#include <cmath>

// TODO Tidy functions/constructors

Solver_t::Solver_t(unsigned int Num_Layers, ConfigFile cfg, const std::vector<ConfigFile> Materials_config)
    : behaviour_kMC(Behaviours::Standard)
    , behaviour_LLG(Behaviours::Standard)
    , Exclusion(false)
    , dt_kMC(1.0e-6)
    , dt_LLG(1.0e-12)
    , dt_LLB(1.0e-15)
    , Measurement_time_kMC(dt_kMC)
    , Measurement_time_LLG(dt_LLG)
    , Measurement_time_LLB(dt_LLB)
    , Alpha(0)
    , Gamma(1.760859644e+7)
    , BoldCyanFont("\033[1;36m")
    , BoldYellowFont("\033[1;33m")
    , BoldRedFont("\033[1;4;31m")
    , ResetFont("\033[0m")
{
    // Get value of solver selection from file
    std::string Selection=cfg.getValueOfKey<std::string>("Solver:Solver_selection");
    if(Selection!="kmc" && Selection!="llb" && Selection!="llg"){
        std::cout << "CFG error: Solver selection invalid." << std::endl;
        exit(-1);
    }
    else{
        if(Selection=="kmc"){type=Solvers::kMC;}
        else if(Selection=="llg"){type=Solvers::LLG;}
        else if(Selection=="llb"){type=Solvers::LLB;}
    }

    Exclusion=cfg.getValueOfKey<bool>("Solver:exclusion_zone");
    if(Exclusion){
        // Input is in grain widths
        double tempNX = cfg.getValueOfKey<double>("Solver:exclusion_range_negX");
        double tempPX = cfg.getValueOfKey<double>("Solver:exclusion_range_posX");
        double tempNY = cfg.getValueOfKey<double>("Solver:exclusion_range_negY");
        double tempPY = cfg.getValueOfKey<double>("Solver:exclusion_range_posY");
        double width_temp = cfg.getValueOfKey<double>("Struct:Avg_Grain_width");
        // Set parameter up to be in real units
        Exclusion_range_negX=tempNX*width_temp;
        Exclusion_range_posX=tempPX*width_temp;
        Exclusion_range_negY=tempNY*width_temp;
        Exclusion_range_posY=tempPY*width_temp;
    }

    Import(cfg);
    switch(type)
    {
    case Solvers::LLG:
    case Solvers::LLB:
        Fittings_import(Num_Layers,Materials_config);
        break;
    default:
        break;
    }
}

// This is useful for simulations which use previously generated systems (i.e. no input system parameters)
Solver_t::Solver_t(ConfigFile cfg)
    : behaviour_kMC(Behaviours::Standard)
    , behaviour_LLG(Behaviours::Standard)
    , Exclusion(false)
    , dt_kMC(1.0e-6)
    , dt_LLG(1.0e-12)
    , dt_LLB(1.0e-15)
    , Measurement_time_kMC(dt_kMC)
    , Measurement_time_LLG(dt_LLG)
    , Measurement_time_LLB(dt_LLB)
    , Alpha(0)
    , Gamma(1.760859644e+7)
    , BoldCyanFont("\033[1;36m")
    , BoldYellowFont("\033[1;33m")
    , BoldRedFont("\033[1;4;31m")
    , ResetFont("\033[0m")
{
    Import(cfg);
}

Solver_t::~Solver_t(){}

// This will import all required values for the solver specified in the function call.
void Solver_t::Force_specific_solver_construction(unsigned int Num_Layers, ConfigFile cfg, const std::vector<ConfigFile> Materials_config, Solvers desired)
{
    if(desired!=type){
        Import(cfg);
        switch(desired) // Ensure Material information required by dynamic solvers is present
        {
        case Solvers::LLG:
        case Solvers::LLB:
            Fittings_import(Num_Layers,Materials_config);
            break;
        default:
            break;
        }
    }
}

void Solver_t::Import(ConfigFile cfg)
{
    switch(type)
    {
    case Solvers::kMC:
        dt_kMC = cfg.getValueOfKey<double>("KMC:dt");
        Measurement_time_kMC = cfg.getValueOfKey<double>("KMC:Measurement_time");
        T_variation = cfg.getValueOfKey<bool>("Solver:T_dependent_variables");
        if(T_variation){
            M_length_variation = cfg.getValueOfKey<bool>("Solver:M_length_variable");
            if(cfg.getValueOfKey<bool>("Solver:Handle_Tc")){
                behaviour_kMC=Behaviours::Thermoremanence;
            }
        }
        f0 = cfg.getValueOfKey<double>("KMC:F0");
        break;
    case Solvers::LLG:
        dt_LLG = cfg.getValueOfKey<double>("LLG:dt");
        Measurement_time_LLG = cfg.getValueOfKey<double>("LLG:Measurement_time");
        T_variation = cfg.getValueOfKey<bool>("Solver:T_dependent_variables");
        if(T_variation){
            M_length_variation = cfg.getValueOfKey<bool>("Solver:M_length_variable");
            if(cfg.getValueOfKey<bool>("Solver:Handle_Tc")){
                behaviour_LLG=Behaviours::Thermoremanence;
            }
        }
        break;
    case Solvers::LLB:
        dt_LLB = cfg.getValueOfKey<double>("LLB:dt");
        Measurement_time_LLB = cfg.getValueOfKey<double>("LLB:Measurement_time");
        break;
    }
}

void Solver_t::Fittings_import(const unsigned int Num_Layers, std::vector<ConfigFile> Materials_config){

    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        Alpha.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Alpha"));

        // Susceptibility parameters
        Susceptibility_Type.push_back(
                Materials_config[Layer].getValueOfKey<std::string>("Mat:susceptibility_type"));
        if (Susceptibility_Type.back() != "default" && Susceptibility_Type.back()!= "kazantseva_susceptibility_fit"
            && Susceptibility_Type.back() != "vogler_susceptibility_fit"&& Susceptibility_Type.back() != "inv_susceptibility_fit") {
                std::cout << BoldRedFont << " Materials_config[Layer] error: Material " << std::to_string(Layer)
                          << " Unknown Susceptibility type. -> " << Susceptibility_Type.back() << ResetFont << std::endl;
            exit(EXIT_FAILURE);
        }
        if (Susceptibility_Type.back() == "default") {
            unit_susceptibility = "1/T";
            Chi_scaling_factor.push_back(9.54393845712027);
            a0_PARA.push_back(1.21e-3);
            a1_PARA.push_back(-2.2e-7);
            a2_PARA.push_back(0.000);
            a3_PARA.push_back(1.95e-13);
            a4_PARA.push_back(-1.3e-17);
            a5_PARA.push_back(0.000);
            a6_PARA.push_back(-4.00e-23);
            a7_PARA.push_back(0.000);
            a8_PARA.push_back(0.000);
            a9_PARA.push_back(-6.51e-32);
            a1_2_PARA.push_back(0.000);
            b0_PARA.push_back(2.12e-3);
            b1_PARA.push_back(0.000);
            b2_PARA.push_back(0.000);
            b3_PARA.push_back(0.000);
            b4_PARA.push_back(0.000);
            a0_PERP.push_back(2.11e-3);
            a1_PERP.push_back(1.10e-1);
            a2_PERP.push_back(-8.55e-1);
            a3_PERP.push_back(3.42);
            a4_PERP.push_back(-7.85);
            a5_PERP.push_back(1.03e+1);
            a6_PERP.push_back(-6.86e-1);
            a7_PERP.push_back(7.97e-1);
            a8_PERP.push_back(1.54);
            a9_PERP.push_back(-6.27e-1);
            a1_2_PERP.push_back(0.000);
            b0_PERP.push_back(4.85e-3);
            b1_PERP.push_back(0.000);
            b2_PERP.push_back(0.000);
            b3_PERP.push_back(0.000);
            b4_PERP.push_back(0.000);
        }
        // N. Kazantseva's functions parameters
        else if (Susceptibility_Type.back()== "kazantseva_susceptibility_fit") {
            unit_susceptibility = "1/T";
            Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
            a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PARA"));
            a1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PARA"));
            a2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PARA"));
            a3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PARA"));
            a4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PARA"));
            a5_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PARA"));
            a6_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PARA"));
            a7_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PARA"));
            a8_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PARA"));
            a9_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PARA"));
            a1_2_PARA.push_back(0.000);
            b0_PARA.push_back(0.000);
            b1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PARA"));
            b2_PARA.push_back(0.000);
            b3_PARA.push_back(0.000);
            b4_PARA.push_back(0.000);
            a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PERP"));
            a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PERP"));
            a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PERP"));
            a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PERP"));
            a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PERP"));
            a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PERP"));
            a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PERP"));
            a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PERP"));
            a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PERP"));
            a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PERP"));
            a1_2_PERP.push_back(0.000);
            b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PERP"));
            b1_PERP.push_back(0.000);
            b2_PERP.push_back(0.000);
            b3_PERP.push_back(0.000);
            b4_PERP.push_back(0.000);
        }
        // Vogler's functions parameters
        else if (Susceptibility_Type.back()
                == "vogler_susceptibility_fit") {
            unit_susceptibility = "1/T";
            Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
            a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PARA"));
            a1_PARA.push_back(0.000);
            a2_PARA.push_back(0.000);
            a3_PARA.push_back(0.000);
            a4_PARA.push_back(0.000);
            a5_PARA.push_back(0.000);
            a6_PARA.push_back(0.000);
            a7_PARA.push_back(0.000);
            a8_PARA.push_back(0.000);
            a9_PARA.push_back(0.000);
            a1_2_PARA.push_back(0.000);
            b0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PARA"));
            b1_PARA.push_back(0.000);
            b2_PARA.push_back(0.000);
            b3_PARA.push_back(0.000);
            b4_PARA.push_back(0.000);
            a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PERP"));
            a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PERP"));
            a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PERP"));
            a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PERP"));
            a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PERP"));
            a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PERP"));
            a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PERP"));
            a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PERP"));
            a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PERP"));
            a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PERP"));
            a1_2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PERP"));
            b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PERP"));
            b1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PERP"));
            b2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b2_PERP"));
            b3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b3_PERP"));
            b4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b4_PERP"));
        }
        // M. Ellis' functions parameters
        else if (Susceptibility_Type.back() == "inv_susceptibility_fit") {
            unit_susceptibility = "T";
            Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
            a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PARA"));
            a1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PARA"));
            a2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PARA"));
            a3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PARA"));
            a4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PARA"));
            a5_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PARA"));
            a6_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PARA"));
            a7_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PARA"));
            a8_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PARA"));
            a9_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PARA"));
            a1_2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PARA"));
            b0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PARA"));
            b1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PARA"));
            b2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b2_PARA"));
            b3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b3_PARA"));
            b4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b4_PARA"));
            a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PERP"));
            a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PERP"));
            a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PERP"));
            a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PERP"));
            a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PERP"));
            a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PERP"));
            a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PERP"));
            a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PERP"));
            a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PERP"));
            a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PERP"));
            a1_2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PERP"));
            b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PERP"));
            b1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PERP"));
            b2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b2_PERP"));
            b3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b3_PERP"));
            b4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b4_PERP"));
        }
    }
}

//########################ACCESSORS########################//

void Solver_t::set_dt(double timestep, Solvers desired){
    switch(desired)
    {
    case Solvers::kMC:
        dt_kMC = timestep;
        break;
    case Solvers::LLG:
        dt_LLG = timestep;
        break;
    case Solvers::LLB:
        dt_LLB = timestep;
        break;
    }
}

void Solver_t::set_dt(double timestep){
    switch(type)
    {
    case Solvers::kMC:
        dt_kMC = timestep;
        break;
    case Solvers::LLG:
        dt_LLG = timestep;
        break;
    case Solvers::LLB:
        dt_LLB = timestep;
        break;
    }
}

double Solver_t::get_dt(Solvers desired_solver) const {
    switch(desired_solver)
    {
    case Solvers::kMC:
        return dt_kMC;
    case Solvers::LLG:
        return dt_LLG;
    case Solvers::LLB:
        return dt_LLB;
    }
    std::cout << "error: Invalid Solver selection in get_dt()" << std::endl;
    exit(-1);
    return 0;
}

double Solver_t::get_dt() const {
    switch(type)
    {
    case Solvers::kMC:
        return dt_kMC;
    case Solvers::LLG:
        return dt_LLG;
    case Solvers::LLB:
        return dt_LLB;
    }
    std::cout << "error: Invalid Solver selection in get_dt()" << std::endl;
    exit(-1);
    return 0;
}

void Solver_t::set_f0(double F0){
    f0 = F0;
}

double Solver_t::get_f0() const {
    return f0;
}

void Solver_t::set_allow_Tc(bool option, Solvers desired_solver){
    switch(desired_solver)
    {
    case Solvers::kMC:
        if(option){
            behaviour_kMC = Behaviours::Thermoremanence;
        } else {
            behaviour_kMC = Behaviours::Standard;
        }
        break;
    case Solvers::LLG:
        if(option){
            behaviour_LLG = Behaviours::Thermoremanence;
        } else {
            behaviour_LLG = Behaviours::Standard;
        }
        break;
    case Solvers::LLB:
        break;
    }
}

void Solver_t::set_ml_output(bool option){
    M_length_variation=option;
}

void Solver_t::set_Solver(Solvers new_solver){
    type = new_solver;
}

Solvers Solver_t::get_Solver() const {
    return type;
}

std::vector<double> Solver_t::get_Alpha() const {
    if(Alpha.size()>0){
        return Alpha;
    } else {
        std::vector<double> empty(1,0.0);
        return empty;
    }
}

double Solver_t::get_Gamma() const {
    return Gamma;
}

double Solver_t::get_mEQ(unsigned int idx) const {
    return m_EQ[idx];
}

double Solver_t::get_ChiPerp(unsigned int idx) const {
    return Chi_perp[idx];
}

bool Solver_t::get_Exclusion() const {
    return Exclusion;
}

void Solver_t::setMeasTime(double Time, Solvers desired_solver){
    switch(desired_solver)
    {
    case Solvers::kMC:
        Measurement_time_kMC=Time;
        break;
    case Solvers::LLG:
        Measurement_time_LLG=Time;
        break;
    case Solvers::LLB:
        Measurement_time_LLB=Time;
        break;
    }
}

double Solver_t::getMeasTime() const {
    switch(type)
    {
    case Solvers::kMC:
        return Measurement_time_kMC;
    case Solvers::LLG:
        return Measurement_time_LLG;
    case Solvers::LLB:
        return Measurement_time_LLB;
    }
    std::cout << "error: Invalid Solver selection in getMeasTime()" << std::endl;
    exit(-1);
    return 0;
}

double Solver_t::getMeasTime(Solvers Desired) const {
    switch(Desired)
    {
    case Solvers::kMC:
        return Measurement_time_kMC;
    case Solvers::LLG:
        return Measurement_time_LLG;
    case Solvers::LLB:
        return Measurement_time_LLB;
    }
    std::cout << "error: Invalid Solver selection in getMeasTime()" << std::endl;
    exit(-1);
    return 0;
}

unsigned int Solver_t::getOutputSteps() const {
    switch(type)
    {
    case Solvers::kMC:
        if(Measurement_time_kMC==0.0){return -1;}
        else if(Measurement_time_kMC<dt_kMC){return 1;}
        return round(Measurement_time_kMC/dt_kMC);
    case Solvers::LLG:
        if(Measurement_time_LLG==0.0){return -1;}
        else if(Measurement_time_LLG<dt_LLG){return 1;}
        return round(Measurement_time_LLG/dt_LLG);
    case Solvers::LLB:
        if(Measurement_time_LLB==0.0){return -1;}
        else if(Measurement_time_LLB<dt_LLB){return 1;}
        return round(Measurement_time_LLB/dt_LLB);
    }
    std::cout << "error: Invalid Solver selection in getOutputSteps()" << std::endl;
    exit(-1);
    return 0;
}

void Solver_t::disableExclusion(){
    Exclusion=false;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

int Solver_t::Integrate(Voronoi_t*VORO, unsigned int Num_layers, Interaction_t*Int_sys, double Cx, double Cy, Grain_t*Grain){

    // TODO fix error reporting (state needs to output 1 if fails and 0 if works).
    int state=1;
    switch(type)
    {
    case Solvers::kMC:
        state=KMC(Num_layers,Int_sys,VORO,Cx,Cy,Grain);
        break;
    case Solvers::LLG:
        state=LLG(Num_layers,Int_sys,VORO,Cx,Cy,Grain);
        break;
    case Solvers::LLB:
        state=LLB(Num_layers,Int_sys,VORO,Cx,Cy,Grain);
        break;
    }
    return state;
}
