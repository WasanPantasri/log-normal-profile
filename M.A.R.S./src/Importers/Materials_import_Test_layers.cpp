/* Materials_import_test_layers.cpp
 *  Created on: 29 Jul 2018
 *      Author: Andrea Meo, Samuel Ewan Rannala
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <cstring>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Importers/Materials_import_Test_layers.hpp"

int Materials_import_Test_layers(const std::string FILE_LOC, const int Num_Layers, std::vector<ConfigFile>*Material_Config, Material_t*Material){

	const std::string BoldCyanFont = "\033[1;36m", BoldYellowFont = "\033[1;33m",
			          BoldRedFont = "\033[1;4;31m", ResetFont = "\033[0m";
	std::vector<double> temp_mag, temp_EA;
	int Material_number;
    std::string unit_susceptibility;

	std::cout << "Assigning material parameters..." << std::endl;
	Material->Initial_mag.resize(Num_Layers);
	Material->Initial_anis.resize(Num_Layers);
	Material->z.push_back(0.0);

	for(int Layer=0;Layer<Num_Layers;++Layer){
		// Determine Material number
		Material_number = Layer+1;
		std::string key_prefix = "mat" + std::to_string(Material_number) + ".cfg";

		std::string FILE_NAME;
		FILE_NAME = FILE_LOC + "/mat" + std::to_string(Material_number) + ".cfg";
		ConfigFile MAT_FILE(FILE_NAME.c_str());
		Material_Config->push_back(MAT_FILE); // Need the config file for future use.

		// Import all variables
		Material->Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:type"));

		Material->Mag_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:Initial_mag_type"));
		if(Material->Mag_Type.back() != "assigned" && Material->Mag_Type.back() != "random"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown Mag type. -> " << Material->Mag_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		if(Material->Mag_Type.back()=="assigned"){
			temp_mag = MAT_FILE.getValueOfKey<std::vector<double>>("Mat:Initial_mag");
			double temp_MAG = temp_mag[0]*temp_mag[0]+temp_mag[1]*temp_mag[1]+temp_mag[2]*temp_mag[2];
			Material->Initial_mag[Layer].x = temp_mag[0]/temp_MAG;
			Material->Initial_mag[Layer].y = temp_mag[1]/temp_MAG;
			Material->Initial_mag[Layer].z = temp_mag[2]/temp_MAG;
		}
		Material->Easy_axis_polar.push_back(MAT_FILE.getValueOfKey<double>("Mat:easy_axis_polar"));
 		Material->Easy_axis_azimuth.push_back(MAT_FILE.getValueOfKey<double>("Mat:easy_axis_azimuth"));

 		Material->dz.push_back(MAT_FILE.getValueOfKey<double>("Mat:thickness"));
 		if(Layer>0){Material->z.push_back(Material->z[Layer-1]+(Material->dz[Layer-1]+Material->dz[Layer])*0.5);}
 		Material->Ms.push_back(MAT_FILE.getValueOfKey<double>("Mat:ms"));

		Material->K_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:k_Dist_type"));
		if(Material->K_Dist_Type.back() != "normal" && Material->K_Dist_Type.back() != "log-normal"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown K distribution type. -> " << Material->K_Dist_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		Material->Avg_K.push_back(MAT_FILE.getValueOfKey<double>("Mat:avg_k"));
		Material->StdDev_K.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_k"));
		// Anisotropy model type
		Material->Ani_method.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:Ani_method"));
		if(Material->Ani_method.back() != "callen" && Material->Ani_method.back() != "chi"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown Anisotropy_method. -> " << Material->Ani_method.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		else if(Material->Ani_method.back()=="callen"){
			// Temperature dependence of anisotropy (Callen-Callen)
			Material->Callen_power_range.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:Callen_power_range"));
			if(Material->Callen_power_range.back() != "single" && Material->Callen_power_range.back() != "multiple"){
				std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown Callen power range type. -> " << Material->Callen_power_range.back() << ResetFont << std::endl;
				exit (EXIT_FAILURE);
			}
			else if(Material->Callen_power_range.back()=="single"){
				// Variables set to ensure no undesired usage occurs.
				Material->Callen_power.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
				Material->Callen_factor_lowT.push_back (1.000);
				Material->Callen_factor_midT.push_back (1.000);
				Material->Callen_factor_highT.push_back(1.000);
				Material->Callen_power_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
				Material->Callen_power_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
				Material->Callen_power_highT.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));
				Material->Callen_range_lowT.push_back (1600.0);
				Material->Callen_range_midT.push_back (2000.0);
			}
			else{
				Material->Callen_power.push_back(0.0);
				Material->Callen_factor_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_factor_lowT"));
				Material->Callen_factor_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_factor_midT"));
				Material->Callen_factor_highT.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_factor_highT"));
				Material->Callen_power_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power_lowT"));
				Material->Callen_power_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_power_midT"));
				Material->Callen_power_highT.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power_highT"));
				Material->Callen_range_lowT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_range_lowT"));
				Material->Callen_range_midT.push_back (MAT_FILE.getValueOfKey<double>("Mat:Callen_range_midT"));
			}
		}
		else{
			Material->Callen_power_range.push_back("single");
			Material->Callen_power.push_back(1.000);
			Material->Callen_factor_lowT.push_back (1.000);
			Material->Callen_factor_midT.push_back (1.000);
			Material->Callen_factor_highT.push_back(1.000);
			Material->Callen_power_lowT.push_back (1.000);
			Material->Callen_power_midT.push_back (1.000);
			Material->Callen_power_highT.push_back(1.000);
			Material->Callen_range_lowT.push_back (1600.0);
			Material->Callen_range_midT.push_back (2000.0);
		}

		Material->Tc_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:tc_dist_type"));
		if(Material->Tc_Dist_Type.back() != "normal" && Material->Tc_Dist_Type.back() != "log-normal"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown Tc distribution type. -> " << Material->Tc_Dist_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		Material->Avg_Tc.push_back(MAT_FILE.getValueOfKey<double>("Mat:avg_tc"));
		Material->StdDev_Tc.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_tc"));
		//#################### Temperature dependence of magnetisation ####################//
		Material->mEQ_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:mEQ_type"));
		if(Material->mEQ_Type.back() != "bulk" && Material->mEQ_Type.back() != "polynomial"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown mEQ type. -> " << Material->mEQ_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		if(Material->mEQ_Type.back() == "bulk"){
			Material->Crit_exp.push_back(MAT_FILE.getValueOfKey<double>("Mat:Critical_Exponent"));
			Material->a0_mEQ.push_back  (0.0);
			Material->a1_mEQ.push_back  (0.0);
			Material->a2_mEQ.push_back  (0.0);
			Material->a3_mEQ.push_back  (0.0);
			Material->a4_mEQ.push_back  (0.0);
			Material->a5_mEQ.push_back  (0.0);
			Material->a6_mEQ.push_back  (0.0);
			Material->a7_mEQ.push_back  (0.0);
			Material->a8_mEQ.push_back  (0.0);
			Material->a9_mEQ.push_back  (0.0);
			Material->a1_2_mEQ.push_back(0.0);
			Material->b1_mEQ.push_back  (0.0);
			Material->b2_mEQ.push_back  (0.0);
		}
		else if(Material->mEQ_Type.back() == "polynomial"){
			Material->Crit_exp.push_back(0.0);
			Material->a0_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a0_mEQ"));
			Material->a1_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a1_mEQ"));
			Material->a2_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a2_mEQ"));
			Material->a3_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a3_mEQ"));
			Material->a4_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a4_mEQ"));
			Material->a5_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a5_mEQ"));
			Material->a6_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a6_mEQ"));
			Material->a7_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a7_mEQ"));
			Material->a8_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a8_mEQ"));
			Material->a9_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:a9_mEQ"));
			Material->a1_2_mEQ.push_back(MAT_FILE.getValueOfKey<double>("Mat:a1_2_mEQ"));
			Material->b1_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:b1_mEQ"));
			Material->b2_mEQ.push_back  (MAT_FILE.getValueOfKey<double>("Mat:b2_mEQ"));
		}

		Material->J_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:j_dist_type"));
		if(Material->J_Dist_Type.back() != "normal" && Material->J_Dist_Type.back() != "log-normal"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown J distribution type. -> " << Material->J_Dist_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		Material->StdDev_J.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_j"));
		Material->H_sat.push_back(MAT_FILE.getValueOfKey<double>("Mat:h_sat"));
		Material->Anis_angle.push_back(MAT_FILE.getValueOfKey<double>("Mat:anis_angle"));

		if(Num_Layers>1){
			Material->Hexch_str_out_plane_UP.push_back(MAT_FILE.getValueOfKey<double>("Mat:Hexch_out_of_layer_UP"));
			if(Layer==0){
				Material->Hexch_str_out_plane_DOWN.push_back(0.0);
			}
			else if(Layer!=0){
				Material->Hexch_str_out_plane_DOWN.push_back(MAT_FILE.getValueOfKey<double>("Mat:Hexch_out_of_layer_DOWN"));
		}	}

	}
//####################Output parameter values to end-user####################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		Material_number = Layer+1;
		std::cout << "..............................Material " + std::to_string(Material_number) + ".............................." << std::endl;
		std::cout << BoldCyanFont << "\tType                                       = \t" << BoldYellowFont << Material->Type[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\tThickness                                  = \t" << BoldYellowFont << Material->dz[Layer] << " nm\t" << std::endl;
		std::cout << BoldCyanFont << "\tMs                                         = \t" << BoldYellowFont << Material->Ms[Layer] << " emu/cc\t" << std::endl;
		/******************************************************************************/
		std::cout << BoldCyanFont << "\tM(T) type                             		= \t" << BoldYellowFont << Material->mEQ_Type[Layer] << " \t" << std::endl;
		if(Material->mEQ_Type[Layer]=="bulk"){
			std::cout << BoldCyanFont << "\t     M critical exp                     = \t" << BoldYellowFont << Material->Crit_exp[Layer] << " \t" << std::endl;
		}
		else{
			std::cout << BoldCyanFont << "\t     a0_mEQ                                = \t" << BoldYellowFont << Material->a0_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a1_mEQ                                = \t" << BoldYellowFont << Material->a1_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a2_mEQ                                = \t" << BoldYellowFont << Material->a2_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a3_mEQ                                = \t" << BoldYellowFont << Material->a3_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a4_mEQ                                = \t" << BoldYellowFont << Material->a4_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a5_mEQ                                = \t" << BoldYellowFont << Material->a5_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a6_mEQ                                = \t" << BoldYellowFont << Material->a6_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a7_mEQ                                = \t" << BoldYellowFont << Material->a7_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a8_mEQ                            	   = \t" << BoldYellowFont << Material->a8_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a9_mEQ                                = \t" << BoldYellowFont << Material->a9_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     a1_2_mEQ                              = \t" << BoldYellowFont << Material->a1_2_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     b1_mEQ                                = \t" << BoldYellowFont << Material->b1_mEQ[Layer] << " \t" << std::endl;
			std::cout << BoldCyanFont << "\t     b2_mEQ                                = \t" << BoldYellowFont << Material->b2_mEQ[Layer] << " \t" << std::endl;
		}
		std::cout << BoldCyanFont << "\tK distribution type                        = \t" << BoldYellowFont << Material->K_Dist_Type[Layer]  << std::endl;
		std::cout << BoldCyanFont << "\t     Average K                             = \t" << BoldYellowFont << Material->Avg_K[Layer] << " erg/cc\t" << std::endl;
		std::cout << BoldCyanFont << "\t     Standard deviation K                  = \t" << BoldYellowFont << Material->StdDev_K[Layer] << " erg/cc\t" << std::endl;
		/******************************************************************************/
		std::cout << BoldCyanFont << "\t     K range low T                         = \t" << BoldYellowFont << Material->Callen_range_lowT[Layer] << " K\t" << std::endl;
		std::cout << BoldCyanFont << "\t     K range mid T                         = \t" << BoldYellowFont << Material->Callen_range_midT[Layer] << " K\t" << std::endl;
		std::cout << BoldCyanFont << "\t     K factor low T                        = \t" << BoldYellowFont << Material->Callen_factor_lowT[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\t     K factor mid  T                       = \t" << BoldYellowFont << Material->Callen_factor_midT[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\t     K factor high T                       = \t" << BoldYellowFont << Material->Callen_factor_highT[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\t     K power low T                         = \t" << BoldYellowFont << Material->Callen_power_lowT[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\t     K power mid  T                        = \t" << BoldYellowFont << Material->Callen_power_midT[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\t     K power high T                        = \t" << BoldYellowFont << Material->Callen_power_highT[Layer] << " \t" << std::endl;
		/******************************************************************************/
		std::cout << BoldCyanFont << "\tTc distribution type                       = \t" << BoldYellowFont << Material->Tc_Dist_Type[Layer]  << std::endl;
		std::cout << BoldCyanFont << "\t     Average Tc                            = \t" << BoldYellowFont << Material->Avg_Tc[Layer] << " K\t" << std::endl;
		std::cout << BoldCyanFont << "\t     Standard deviation Tc                 = \t" << BoldYellowFont << Material->StdDev_Tc[Layer] << " K\t" << std::endl;
		std::cout << BoldCyanFont << "\tJ distribution type                        = \t" << BoldYellowFont << Material->J_Dist_Type[Layer]  << std::endl;
		std::cout << BoldCyanFont << "\t     H_Sat                                 = \t" << BoldYellowFont << Material->H_sat[Layer] << " Oe\t" << std::endl;
		std::cout << BoldCyanFont << "\t     Standard deviation J                  = \t" << BoldYellowFont << Material->StdDev_J[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\tAnisotropy dispersion angle                = \t" << BoldYellowFont << Material->Anis_angle[Layer] << " degrees\t" << std::endl;
		if(Num_Layers>1){
			std::cout << BoldCyanFont << "\tH exchange out of plane_UP                 = \t" << BoldYellowFont << Material->Hexch_str_out_plane_UP[Layer] << " Oe\t" << std::endl;
			if(Layer!=0){
				std::cout << BoldCyanFont << "\tH exchange out of plane_DOWN               = \t" << BoldYellowFont << Material->Hexch_str_out_plane_DOWN[Layer] << " Oe\t" << std::endl;
		}	}
		std::cout << ResetFont << std::endl;
	}
//	std::cout << "======================================================================\n" << std::endl;
	return 0;
}

