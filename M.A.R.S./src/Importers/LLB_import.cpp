/* LLB_import.cpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int LLB_import(const ConfigFile cfg, const std::vector<ConfigFile> Materials_config, const int Num_Layers, LLB_t*LLB){
	std::cout << "Assigning LLB parameters..." << std::endl;

	const std::string BoldCyanFont = "\033[1;36m";
	const std::string BoldYellowFont = "\033[1;33m";
	const std::string BoldRedFont = "\033[1;4;31m";
	const std::string ResetFont = "\033[0m";
	std::string unit_susceptibility;

//####################Retrieve values from hash table####################//
	LLB->Exclusion_zone=cfg.getValueOfKey<bool>("LLB:exclusion_zone");
	if(LLB->Exclusion_zone==true){
		// Input is in grain widths
		double tempNX = cfg.getValueOfKey<double>("LLB:exclusion_range_negX");
		double tempPX = cfg.getValueOfKey<double>("LLB:exclusion_range_posX");
		double tempNY = cfg.getValueOfKey<double>("LLB:exclusion_range_negY");
		double tempPY = cfg.getValueOfKey<double>("LLB:exclusion_range_posY");
		double width_temp = cfg.getValueOfKey<double>("Struct:Avg_Grain_width");
		// Set parameter up to be in real units
		LLB->Exclusion_range_negX=tempNX*width_temp;
		LLB->Exclusion_range_posX=tempPX*width_temp;
		LLB->Exclusion_range_negY=tempNY*width_temp;
		LLB->Exclusion_range_posY=tempPY*width_temp;
	}

	for(int Layer=0;Layer<Num_Layers;++Layer){
		LLB->Alpha.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Alpha"));

		// Susceptibility parameters
		LLB->Susceptibility_Type.push_back(Materials_config[Layer].getValueOfKey<std::string>("Mat:susceptibility_type"));
		if(LLB->Susceptibility_Type.back() != "default" && LLB->Susceptibility_Type.back() != "kazantseva_susceptibility_fit" && LLB->Susceptibility_Type.back() != "vogler_susceptibility_fit" && LLB->Susceptibility_Type.back() != "inv_susceptibility_fit"){
			std::cout << BoldRedFont << " Materials_config[Layer] error: Material " << std::to_string(Layer) << " Unknown Susceptibility type. -> " << LLB->Susceptibility_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		if(LLB->Susceptibility_Type.back()=="default"){
			unit_susceptibility="1/T";
			LLB->Chi_scaling_factor.push_back(9.54393845712027);
        	LLB->a0_PARA.push_back(1.21e-3  );
        	LLB->a1_PARA.push_back(-2.2e-7  );
			LLB->a2_PARA.push_back(0.000    );
			LLB->a3_PARA.push_back(1.95e-13 );
			LLB->a4_PARA.push_back(-1.3e-17 );
			LLB->a5_PARA.push_back(0.000    );
			LLB->a6_PARA.push_back(-4.00e-23);
			LLB->a7_PARA.push_back(0.000    );
			LLB->a8_PARA.push_back(0.000    );
			LLB->a9_PARA.push_back(-6.51e-32);
			LLB->a1_2_PARA.push_back(0.000  );
			LLB->b0_PARA.push_back(2.12e-3  );
			LLB->b1_PARA.push_back(0.000    );
			LLB->b2_PARA.push_back(0.000    );
			LLB->b3_PARA.push_back(0.000    );
			LLB->b4_PARA.push_back(0.000    );
			LLB->a0_PERP.push_back(2.11e-3  );
			LLB->a1_PERP.push_back(1.10e-1  );
			LLB->a2_PERP.push_back(-8.55e-1 );
			LLB->a3_PERP.push_back(3.42     );
			LLB->a4_PERP.push_back(-7.85    );
			LLB->a5_PERP.push_back(1.03e+1  );
			LLB->a6_PERP.push_back(-6.86e-1 );
			LLB->a7_PERP.push_back(7.97e-1  );
			LLB->a8_PERP.push_back(1.54     );
			LLB->a9_PERP.push_back(-6.27e-1 );
			LLB->a1_2_PERP.push_back(0.000  );
			LLB->b0_PERP.push_back(4.85e-3  );
			LLB->b1_PERP.push_back(0.000    );
			LLB->b2_PERP.push_back(0.000    );
			LLB->b3_PERP.push_back(0.000    );
			LLB->b4_PERP.push_back(0.000    );
		}
		// N. Kazantseva's functions parameters
		else if(LLB->Susceptibility_Type.back()=="kazantseva_susceptibility_fit"){
			unit_susceptibility="1/T";
			LLB->Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
			LLB->a0_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a0_PARA"));
			LLB->a1_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_PARA"));
			LLB->a2_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a2_PARA"));
			LLB->a3_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a3_PARA"));
			LLB->a4_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a4_PARA"));
			LLB->a5_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a5_PARA"));
			LLB->a6_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a6_PARA"));
			LLB->a7_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a7_PARA"));
			LLB->a8_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a8_PARA"));
			LLB->a9_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a9_PARA"));
			LLB->a1_2_PARA.push_back(0.000);
			LLB->b0_PARA.push_back  (0.000);
			LLB->b1_PARA.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:b1_PARA"));
			LLB->b2_PARA.push_back  (0.000);
			LLB->b3_PARA.push_back  (0.000);
			LLB->b4_PARA.push_back  (0.000);
			LLB->a0_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a0_PERP"));
			LLB->a1_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_PERP"));
			LLB->a2_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a2_PERP"));
			LLB->a3_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a3_PERP"));
			LLB->a4_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a4_PERP"));
			LLB->a5_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a5_PERP"));
			LLB->a6_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a6_PERP"));
			LLB->a7_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a7_PERP"));
			LLB->a8_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a8_PERP"));
			LLB->a9_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:a9_PERP"));
			LLB->a1_2_PERP.push_back(0.000);
			LLB->b0_PERP.push_back  (Materials_config[Layer].getValueOfKey<double>  ("Mat:b0_PERP"));
			LLB->b1_PERP.push_back  (0.000);
			LLB->b2_PERP.push_back  (0.000);
			LLB->b3_PERP.push_back  (0.000);
			LLB->b4_PERP.push_back  (0.000);
        }
       // Vogler's functions parameters
       else if(	LLB->Susceptibility_Type.back()=="vogler_susceptibility_fit"){
    	   unit_susceptibility="1/T";
    	   LLB->Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
    	   LLB->a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a0_PARA"));
    	   LLB->a1_PARA.push_back(0.000);
    	   LLB->a2_PARA.push_back(0.000);
    	   LLB->a3_PARA.push_back(0.000);
    	   LLB->a4_PARA.push_back(0.000);
    	   LLB->a5_PARA.push_back(0.000);
    	   LLB->a6_PARA.push_back(0.000);
    	   LLB->a7_PARA.push_back(0.000);
    	   LLB->a8_PARA.push_back(0.000);
    	   LLB->a9_PARA.push_back(0.000);
    	   LLB->a1_2_PARA.push_back(0.000);
    	   LLB->b0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b0_PARA"));
    	   LLB->b1_PARA.push_back(0.000);
    	   LLB->b2_PARA.push_back(0.000);
    	   LLB->b3_PARA.push_back(0.000);
    	   LLB->b4_PARA.push_back(0.000);
    	   LLB->a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a0_PERP"));
		   LLB->a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_PERP"));
		   LLB->a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a2_PERP"));
		   LLB->a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a3_PERP"));
		   LLB->a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a4_PERP"));
		   LLB->a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a5_PERP"));
		   LLB->a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a6_PERP"));
		   LLB->a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a7_PERP"));
		   LLB->a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a8_PERP"));
		   LLB->a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a9_PERP"));
		   LLB->a1_2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_2_PERP"));
		   LLB->b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b0_PERP"));
		   LLB->b1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b1_PERP"));
		   LLB->b2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b2_PERP"));
		   LLB->b3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b3_PERP"));
		   LLB->b4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b4_PERP"));
        }
		// M. Ellis' functions parameters
        else if(LLB->Susceptibility_Type.back()=="inv_susceptibility_fit"){
        	unit_susceptibility="T";
        	LLB->Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
    	    LLB->a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a0_PARA"));
    	    LLB->a1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_PARA"));
    	    LLB->a2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a2_PARA"));
    	    LLB->a3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a3_PARA"));
    	    LLB->a4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a4_PARA"));
    	    LLB->a5_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a5_PARA"));
    	    LLB->a6_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a6_PARA"));
    	    LLB->a7_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a7_PARA"));
    	    LLB->a8_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a8_PARA"));
    	    LLB->a9_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a9_PARA"));
    	    LLB->a1_2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PARA"));
    	    LLB->b0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b0_PARA"));
    	    LLB->b1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b1_PARA"));
    	    LLB->b2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b2_PARA"));
    	    LLB->b3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b3_PARA"));
    	    LLB->b4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b4_PARA"));
    	    LLB->a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a0_PERP"));
    	    LLB->a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_PERP"));
    	    LLB->a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a2_PERP"));
    	    LLB->a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a3_PERP"));
    	    LLB->a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a4_PERP"));
    	    LLB->a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a5_PERP"));
    	    LLB->a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a6_PERP"));
    	    LLB->a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a7_PERP"));
    	    LLB->a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a8_PERP"));
    	    LLB->a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a9_PERP"));
    	    LLB->a1_2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:a1_2_PERP"));
    	    LLB->b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b0_PERP"));
    	    LLB->b1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b1_PERP"));
    	    LLB->b2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b2_PERP"));
    	    LLB->b3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b3_PERP"));
    	    LLB->b4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>  ("Mat:b4_PERP"));
        }
	}
	LLB->dt    = cfg.getValueOfKey<double>("llb:dt");

//####################Output parameter values to end-user####################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		std::cout << ResetFont << std::endl;
		if(Num_Layers>1){
			std::cout << "..............................Material " << Layer+1 << ".............................." << std::endl;
		}
		std::cout << BoldCyanFont << "\tAlpha " << Layer+1 << "                                    = \t" << BoldYellowFont << LLB->Alpha[Layer] << " \t" << std::endl;
		// Parallel susceptibility parameters
		std::cout << BoldCyanFont << "\tSusceptibility_type                        = \t" << BoldYellowFont << LLB->Susceptibility_Type[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\tSusceptibility_factor                      = \t" << BoldYellowFont << LLB->Chi_scaling_factor[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\ta0_PARA                                    = \t" << BoldYellowFont << LLB->a0_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta1_PARA                                    = \t" << BoldYellowFont << LLB->a1_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta2_PARA                                    = \t" << BoldYellowFont << LLB->a2_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta3_PARA                                    = \t" << BoldYellowFont << LLB->a3_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta4_PARA                                    = \t" << BoldYellowFont << LLB->a4_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta5_PARA                                    = \t" << BoldYellowFont << LLB->a5_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta6_PARA                                    = \t" << BoldYellowFont << LLB->a6_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta7_PARA                                    = \t" << BoldYellowFont << LLB->a7_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta8_PARA                                    = \t" << BoldYellowFont << LLB->a8_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta9_PARA                                    = \t" << BoldYellowFont << LLB->a9_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta1_2_PARA                                  = \t" << BoldYellowFont << LLB->a1_2_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb0_PARA                                    = \t" << BoldYellowFont << LLB->b0_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb1_PARA                                    = \t" << BoldYellowFont << LLB->b1_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb2_PARA                                    = \t" << BoldYellowFont << LLB->b2_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb3_PARA                                    = \t" << BoldYellowFont << LLB->b3_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb4_PARA                                    = \t" << BoldYellowFont << LLB->b4_PARA[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		// Perpendicular susceptibility parameters
		std::cout << BoldCyanFont << "\ta0_PERP                                    = \t" << BoldYellowFont << LLB->a0_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta1_PERP                                    = \t" << BoldYellowFont << LLB->a1_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta2_PERP                                    = \t" << BoldYellowFont << LLB->a2_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta3_PERP                                    = \t" << BoldYellowFont << LLB->a3_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta4_PERP                                    = \t" << BoldYellowFont << LLB->a4_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta5_PERP                                    = \t" << BoldYellowFont << LLB->a5_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta6_PERP                                    = \t" << BoldYellowFont << LLB->a6_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta7_PERP                                    = \t" << BoldYellowFont << LLB->a7_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta8_PERP                                    = \t" << BoldYellowFont << LLB->a8_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta9_PERP                                    = \t" << BoldYellowFont << LLB->a9_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\ta1_2_PERP                                  = \t" << BoldYellowFont << LLB->a1_2_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb0_PERP                                    = \t" << BoldYellowFont << LLB->b0_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb1_PERP                                    = \t" << BoldYellowFont << LLB->b1_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb2_PERP                                    = \t" << BoldYellowFont << LLB->b2_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb3_PERP                                    = \t" << BoldYellowFont << LLB->b3_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
		std::cout << BoldCyanFont << "\tb4_PERP                                    = \t" << BoldYellowFont << LLB->b4_PERP[Layer] << " " << unit_susceptibility << "\t" << std::endl;
	}
	std::cout << BoldCyanFont << "\tdt                                         = \t" << BoldYellowFont << LLB->dt << " \t" << std::endl;
	std::cout << ResetFont << std::endl;
	std::cout << "======================================================================\n" << std::endl;

	return 0;
}


