/*
 * Experiment_time_evolution_import.cpp
 *
 *  Created on: 2 Dec 2019
 *      Author: Ewan Rannala
 */

#include <algorithm>

#include "../../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../../hdr/Structures.hpp"

int Time_evol_import(const ConfigFile cfg, SNR_t*SNR_data){

	SNR_data->Snapshots = cfg.getValueOfKey<unsigned int>("EXPT:Snapshots");
	SNR_data->Times = cfg.getValueOfKey<std::vector<double>>("EXPT:Times");
	// Sort Times to be in increasing order
	std::sort(SNR_data->Times.begin(),SNR_data->Times.end());

	return 0;
}
