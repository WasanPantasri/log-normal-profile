/*
 * Read_back_import.cpp
 *
 *  Created on: 7 Nov 2019
 *      Author: Ewan Rannala
 */

#include "../../../hdr/Structures.hpp"
#include "../../../hdr/Config_File/ConfigFile_import.hpp"

int Read_Back_import(const ConfigFile cfg, Expt_data_read_t*Expt_data_read){

	Expt_data_read->ReadBack = cfg.getValueOfKey<bool>("EXPT:ReadBack");
	if(Expt_data_read->ReadBack==true){
		Expt_data_read->Head_speed = cfg.getValueOfKey<double>("EXPT:Read_Head_speed");
		Expt_data_read->Head_timestep = cfg.getValueOfKey<double>("EXPT:Read_Head_record_interval");
		Expt_data_read->Head_width = cfg.getValueOfKey<double>("EXPT:Read_Head_width_X");
		Expt_data_read->Head_length = cfg.getValueOfKey<double>("EXPT:Read_Head_width_Y");
		Expt_data_read->Bit_spacing_Y =  cfg.getValueOfKey<double>("HAMR:Bit_spacing_Y");
		Expt_data_read->Bit_length = cfg.getValueOfKey<double>("HAMR:Bit_length");
		Expt_data_read->Bit_number_Y = cfg.getValueOfKey<int>("HAMR:Bit_number_in_y");
	}
	return 0;
}
