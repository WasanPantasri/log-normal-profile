/* Parameter_import.cpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include <fstream>
#include <string>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int Structure_import(const ConfigFile cfg, Structure_t*Structure){
	std::cout << "Assigning Structure parameters..." << std::endl;

	const std::string BoldCyanFont = "\033[1;36m";
	const std::string BoldYellowFont = "\033[1;33m";
	const std::string BoldRedFont = "\033[1;4;31m";
	const std::string ResetFont = "\033[0m";

//####################Retrieve values from hash table####################//
	Structure->Dim_x = cfg.getValueOfKey<double>("struct:dim-x");
	Structure->Dim_y = cfg.getValueOfKey<double>("struct:dim-y");
	Structure->Num_layers = cfg.getValueOfKey<unsigned int>("struct:num_layers");
	Structure->Grain_width = cfg.getValueOfKey<double>("struct:avg_grain_width");
	Structure->StdDev_grain_pos = cfg.getValueOfKey<double>("struct:std_dev_grain_vol");
	Structure->Packing_fraction = cfg.getValueOfKey<double>("struct:packing_fraction");
	Structure->Magneto_Interaction_Radius = cfg.getValueOfKey<double>("struct:interaction_radius");
	Structure->Magnetostatics_gen_type = cfg.getValueOfKey<std::string>("struct:magnetostatics");
	if(Structure->Magnetostatics_gen_type != "dipole" && Structure->Magnetostatics_gen_type != "import"){
		std::cout << BoldRedFont << " CFG error: Structure: Unknown magnetostatics type. -> " << Structure->Magnetostatics_gen_type << ResetFont << std::endl;
		exit (EXIT_FAILURE);
	}

//####################Output parameter values to end-user####################//
	std::cout << BoldCyanFont << "\tX dimension (dx)                           = \t" << BoldYellowFont << Structure->Dim_x << " nm\t" << std::endl;
	std::cout << BoldCyanFont << "\tY dimension (dy)                           = \t" << BoldYellowFont << Structure->Dim_y << " nm\t" << std::endl;
	std::cout << BoldCyanFont << "\tNumber of layers in the system is          = \t" << BoldYellowFont << Structure->Num_layers << " Layer\t" << std::endl;
	std::cout << BoldCyanFont << "\tThe average grain width is                 = \t" << BoldYellowFont << Structure->Grain_width << " nm\t" << std::endl;
	std::cout << BoldCyanFont << "\tThe standard deviation of the grain volume = \t" << BoldYellowFont << Structure->StdDev_grain_pos << "\t" << std::endl;
	std::cout << BoldCyanFont << "\tThe packing fraction is                    = \t" << BoldYellowFont << Structure->Packing_fraction << "\t" << std::endl;
	std::cout << ResetFont << std::endl;

	return 0;
}



