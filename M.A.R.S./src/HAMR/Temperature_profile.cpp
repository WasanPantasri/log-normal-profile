/*
 * Temperature_profile.cpp
 *
 *  Created on: 5 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Temperature_profile.cpp
 * \brief Functions for laser profiles */
#include "math.h"

#include "../../hdr/Structures.hpp"
/** Laser profile with temperature as a function of time, the profile follows a standard Gaussian distribution.
 *  \f[
 *  T(t) = T_{min} + (T_{max}-T_{min})\exp{\Bigg[-\Bigg(\frac{t-3t_{cool}}{t_{cool}}\Bigg)^2\Bigg]}
 *  \f]
 *  \f$T_{min}\f$ is the background temperature of the simulation. <BR>
 *  \f$T_{max}\f$ is the maximum laser temperature. <BR>
 *  \f$t_{cool}\f$ is a sixth of the total time taken to pulse the laser.
 *
 *  \imageSize{Temp_profile_no_spatial.png,height:300px;width:440px;}
 *  \image html  Temp_profile_no_spatial.png
 *  \image latex Temp_profile_no_spatial.eps
 * The function loops through all grains in the system and updates Grain_t.Temp to account for the presence of the laser.
 * */
int Temperature_profile(const Expt_laser_t Expt_laser, const double Time, double*Temperature, std::vector<double>*Grain_temp){

	*Temperature = Expt_laser.Laser_Temp_MIN + (Expt_laser.Laser_Temp_MAX-Expt_laser.Laser_Temp_MIN)*exp( -pow( ((Time-3*Expt_laser.cooling_time)/Expt_laser.cooling_time),2.0 ) );
	for(size_t grain_num=0;grain_num<Grain_temp->size();grain_num++){Grain_temp->at(grain_num) = *Temperature;}

	return 0;
}

/** Laser profile with temporal and spatial dependencies, the profile follows a standard Gaussian in
 * both space and time.
 * This profile is defined in the following work: Vogler et al, J. Appl. Phys. 119, 223903 (2016)
 * https://doi.org/10.1063/1.4953390
 * \f[
 * T(x,y,t) = T_{min} + T(t)\exp{\Bigg[-\Bigg(\frac{(x-C_x)^2}{P_g\gamma_x^2} + \frac{(y-C_y)^2}{P_g\gamma_y^2}\Bigg)\Bigg]}
 * \f]
 * \f$T_{min}\f$ is the background temperature of the simulation. <BR>
 * \f$T(t) \f$ is equivalent to Temperature_profile(const Expt_laser_t Expt_laser, const double Time, double*Temperature, std::vector<double>*Grain_temp) <BR>
 * \f$C_x\f$ and\f$C_y\f$ describe the centre of the pulse in x and y respectively. <BR>
 * \f$P_g\f$ is the profile gradient, which is used to set the sharpness of the profile. <BR>
 * \f$\gamma_x\f$ and \f$\gamma_y\f$ are the Full Width Half Maximums of the profile in x and y respectively. <BR>
 *
 * The function loops through all grains in the system and updates Grain_t.Temp to account for the presence of the laser.
 * */
int Temperature_profile_spatial(const int Num_Layers, const Voronoi_t VORO, const Expt_laser_t Expt_laser,
						const double cen_X, const double cen_Y, const double Time, double*Spatial_Temp, Grain_t*Grain){

	double Temporal_Temp = (Expt_laser.Laser_Temp_MAX-Expt_laser.Laser_Temp_MIN)*exp(-pow(((Time-3*Expt_laser.cooling_time)/Expt_laser.cooling_time),2.0));

	for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
		double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];

		//########## Generic definition of the laser profile.
		*Spatial_Temp = Expt_laser.Laser_Temp_MIN + Temporal_Temp * exp(-(pow(Grain_X-cen_X,2.0)/(Expt_laser.Tprofile_gradient*pow(Expt_laser.Tprofile_FWHM_X,2.0)) + pow(Grain_Y-cen_Y,2.0)/(Expt_laser.Tprofile_gradient*pow(Expt_laser.Tprofile_FWHM_Y,2.0))));
		//########## Vogler definition of the laser profile.
//		*Spatial_Temp = Expt_laser.Laser_Temp_MIN + Temporal_Temp * exp(-( pow(Grain_X-cen_X,2.0)/(2.0*pow(sigma_X,2.0)) + pow(Grain_Y-cen_Y,2.0)/(2.0*pow(sigma_Y,2.0)) ) );
	 	// Assign temperature to each layer.
	 	for(int Layer=0;Layer<Num_Layers;++Layer){
	 		int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
	 		Grain->Temp[grain_in_system] = *Spatial_Temp;
	 }	}

	return 0;
}

/** Laser profile for a continuously moving laser (along track only), the spatial dependence in the x-direction is dependent on the speed of the laser.
 * This profile is defined in the following work: Vogler et al, J. Appl. Phys. 119, 223903 (2016)
 * https://doi.org/10.1063/1.4953390
 * \f[
 * T(x,y,t) = T_{min} + (T_{max}-T_{min})exp\Bigg[-\Bigg(\frac{(x-vt)^2}{2\sigma_x^2}\Bigg)\Bigg]exp\Bigg[-\Bigg(\frac{(y-C_y)^2}{2\sigma_y^2}\Bigg)\Bigg]
 * \f]
 * \f[
 * \sigma_i = \frac{\gamma_i}{\sqrt{8\log(2)}}
 * \f]
 * \f$T_{min}\f$ is the background temperature of the simulation. <BR>
 * \f$T_{max}\f$ is the maximum laser temperature. <BR>
 * \f$C_y\f$ describe the centre of the pulse in y . <BR>
 * \f$\gamma_x\f$ and \f$\gamma_y\f$ are the Full Width Half Maximums of the profile in x and y respectively. <BR>
 *
 * The function loops through all grains in the system and updates Grain_t.Temp to account for the presence of the laser.
 * */
int Temperature_profile_spatial_cont(const int Num_Layers, const Voronoi_t VORO, const Expt_laser_t Expt_laser,
						const double cen_X, const double cen_Y, const double Speed, const double Time, double*Total_Temp, Grain_t*Grain){

	double Temporal_Temp, Spatial_temp;
    const double sigma_X = Expt_laser.Tprofile_FWHM_X / sqrt(8.0 * log(2.0));
    const double sigma_Y = Expt_laser.Tprofile_FWHM_Y / sqrt(8.0 * log(2.0));
    const double NPS_X = Expt_laser.NFT_to_Pole_spacing_X;


	for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
		double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];

		//########## Vogler definition of the continuous laser profile.
		Temporal_Temp = exp(-pow(Grain_X - (Speed*Time+NPS_X),2.0)/(2.0*pow(sigma_X,2.0)));
		Spatial_temp  = exp(-pow(Grain_Y - cen_Y     ,2.0)/(2.0*pow(sigma_Y,2.0)));
		*Total_Temp = Expt_laser.Laser_Temp_MIN + (Expt_laser.Laser_Temp_MAX-Expt_laser.Laser_Temp_MIN) * Temporal_Temp * Spatial_temp;
	 	// Assign temperature to each layer.
	 	for(int Layer=0;Layer<Num_Layers;++Layer){
	 		int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
	 		Grain->Temp[grain_in_system] = *Total_Temp;
	 }	}

	return 0;
}
