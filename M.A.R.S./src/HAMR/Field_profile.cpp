/*
 * Field_profile.cpp
 *
 *  Created on: 5 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Field_profile.cpp
 * \brief Functions for field profiles. */

#include "math.h"

#include "../../hdr/Structures.hpp"
/** Field profile as a function of time, this is a simple trapezoid.
 * \f[
 *  	H_{app}=\left \{
 *	  	\begin{aligned}
 *  	    &H_{app_{min}}+\bigg(\frac{H_{app_{max}}-H_{app_{min}}}{t_{ramp}}\bigg)t, && \text{if } t<t_{ramp} \\
 *      	&H_{app_{max}}, && \text{if } t_{ramp} \leq t \leq 6t_{cool}-t_{ramp} \\
 *      	&H_{app_{min}}-\bigg(\frac{H_{app_{max}}-H_{app_{min}}}{t_{ramp}}\bigg)t, && \text{if } 6t_{cool}-t_{ramp}<t<6t_{cool} \\
 *      	&H_{app_{min}}, && \text{otherwise. }
 *  	\end{aligned} \right.
 *  \f]
 *  \f$H_{app_{min}}\f$ is the minimum applied field. <BR>
 *  \f$H_{app_{max}}\f$ is the maximum applied field. <BR>
 *  \f$t_{ramp}\f$ is const Expt_H_app_t.Field_ramp_time. <BR>
 *
 *  \imageSize{Field_profile_no_spatial.png,height:300px;width:440px;}
 *  \image html Field_profile_no_spatial.png
 *
 *  This function loops through all grains and updates Grain_t.H_appl to account for the applied field.
 */
int Field_profile(const int Num_Layers, const Voronoi_t VORO, const Expt_H_app_t Expt_field,
				  const double cooling_time, const double Time, const double Time_increment, double*Field_MAG, Grain_t*Grain){

	Vec3 Applied_Field;

	if(Time<Expt_field.Field_ramp_time && *Field_MAG<Expt_field.H_appl_MAG_max){
		*Field_MAG += (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}
	// Ensure field is applied in case were time-step exceeds field ramp time.
    else if(Time>=Expt_field.Field_ramp_time && Time<6*cooling_time-Expt_field.Field_ramp_time && *Field_MAG<Expt_field.H_appl_MAG_max){
        *Field_MAG = Expt_field.H_appl_MAG_max;
    }
	else if(Time > 6*cooling_time-Expt_field.Field_ramp_time && Time < 6*cooling_time){
		*Field_MAG -= (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}

	Applied_Field.x = Expt_field.H_appl_unit.x*(*Field_MAG);
	Applied_Field.y = Expt_field.H_appl_unit.y*(*Field_MAG);
	Applied_Field.z = Expt_field.H_appl_unit.z*(*Field_MAG);


	for(int layer=0;layer<Num_Layers;layer++){
		int offset = layer*VORO.Num_Grains;
		for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
			int grain_in_system = offset + grain_in_layer;
			Grain->H_appl[grain_in_system].x = Applied_Field.x;
			Grain->H_appl[grain_in_system].y = Applied_Field.y;
			Grain->H_appl[grain_in_system].z = Applied_Field.z;
	}	}

	return 0;
}
/** Field profile as a function of time and space, this is a simple trapezoid. <BR>
 *  The temporal field profile is the same as that of Field_profile(const int Num_Layers, const Voronoi_t VORO, const Expt_H_app_t Expt_field, const double cooling_time, const double Time, const double Time_increment, double*Field_MAG, Grain_t*Grain) <BR>
 *  The spatial dependency is simply determined by checking that the grain is within
 *  Expt_H_app_t.Field_width_X and Expt_H_app_t.Field_width_Y of the head centre w.r.t x and y
 *  respectively.
 *
 *  This function loops through all grains and updates Grain_t.H_appl to account for the applied field.
 */
int Field_profile_spatial(const int Num_Layers, const Voronoi_t VORO, const Expt_H_app_t Expt_field,
		  const double cooling_time, const double Head_x, const double Head_y,const double Time, const double Time_increment,
		  double*Field_MAG, Grain_t*Grain){

	Vec3 Applied_Field;

	// Keep track of the temporal field variation.
	if(Time<Expt_field.Field_ramp_time && *Field_MAG<Expt_field.H_appl_MAG_max){
		*Field_MAG += (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}
	// Ensure field is applied in case were time-step exceeds field ramp time.
    else if(Time>=Expt_field.Field_ramp_time && Time<6*cooling_time-Expt_field.Field_ramp_time && *Field_MAG<Expt_field.H_appl_MAG_max){
        *Field_MAG = Expt_field.H_appl_MAG_max;
    }
	else if(Time > 6*cooling_time-Expt_field.Field_ramp_time && Time < 6*cooling_time){
		*Field_MAG -= (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}

	Applied_Field.x = Expt_field.H_appl_unit.x*(*Field_MAG);
	Applied_Field.y = Expt_field.H_appl_unit.y*(*Field_MAG);
	Applied_Field.z = Expt_field.H_appl_unit.z*(*Field_MAG);

	// Apply field profile only to grains within range of the write head.
	for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
		double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];
		if(fabs(Head_x-Grain_X)<=(Expt_field.Field_width_X/2.0) && fabs(Head_y-Grain_Y)<=(Expt_field.Field_width_Y/2.0)){
		 	for(int Layer=0;Layer<Num_Layers;++Layer){
		 		int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
				Grain->H_appl[grain_in_system].x = Applied_Field.x;
				Grain->H_appl[grain_in_system].y = Applied_Field.y;
				Grain->H_appl[grain_in_system].z = Applied_Field.z;
		 	}
		}
		else{
			// Reset all the other grains not in range of writing head to zero applied field
		 	for(int Layer=0;Layer<Num_Layers;++Layer){
		 		int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
				Grain->H_appl[grain_in_system].x = 0.0;
				Grain->H_appl[grain_in_system].y = 0.0;
				Grain->H_appl[grain_in_system].z = 0.0;
		 	}
		}
	}

	return 0;
}


