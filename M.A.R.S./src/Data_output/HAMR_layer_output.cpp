/*
 * HAMR_layer_output.cpp
 *
 *  Created on: 8 Oct 2018
 *      Author: ewan
 */

#include <vector>
#include "math.h"
#include <fstream>
#include <iomanip>

#include "../../hdr/Structures.hpp"


// This code is now set up to work for a set range of grains - enabling entire system data or specific region system data output.
// Need to set up a way to organise the output of this data - separate files?


int HAMR_layer_averages(const int Num_layers, const int Num_Grains, const Grain_t Grain, const double Field_MAG, const double Temperature,
						const double Time, const std::vector<double> Grain_list, const std::string OUTPUT){

	std::fstream OUTPUT_FILE;
	std::vector<double>  Normaliser (Num_layers),
						 Mx (Num_layers), My (Num_layers),
						 Mz (Num_layers), Ml (Num_layers);
	double MxT=0.0,MyT=0.0,MzT=0.0,MlT=0.0,
		   Total_normaliser=0.0,
		   Number_of_Grains = Grain_list.size();

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ASSIGN TITLES
	OUTPUT_FILE.open(OUTPUT.c_str());
	if(OUTPUT_FILE.peek() == std::ifstream::traits_type::eof()){ // If file is empty.
		OUTPUT_FILE.close();
		OUTPUT_FILE.open(OUTPUT.c_str(), std::ofstream::out);
		OUTPUT_FILE << std::setprecision(10) << "Time Temp Field ";
		for(int Layer=0;Layer<Num_layers;++Layer){
			std::string Layer_string = std::to_string(Layer+1);
			OUTPUT_FILE << "Mx" + Layer_string + " My" + Layer_string + " Mz" + Layer_string + " Ml" + Layer_string + " ";
		}
		OUTPUT_FILE << "MxT MyT MzT MlT" << std::endl;
	}
	else{
		OUTPUT_FILE.close();
		OUTPUT_FILE.open(OUTPUT.c_str(), std::ofstream::app);
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ WRITE DATA
	for(int Layer=0;Layer<Num_layers;++Layer){
		double Avg_vol=0.0;
		int Offset=(Num_Grains*Layer);
		Mx[Layer]=My[Layer]=Mz[Layer]=Ml[Layer]=0.0;
		for(int grain_in_layer=0;grain_in_layer<Number_of_Grains;++grain_in_layer){
			int data_grain = Grain_list[grain_in_layer];
			int data_grain_in_system = data_grain+Offset;
			Mx[Layer] += Grain.m[data_grain_in_system].x*Grain.Vol[data_grain_in_system];
			My[Layer] += Grain.m[data_grain_in_system].y*Grain.Vol[data_grain_in_system];
			Mz[Layer] += Grain.m[data_grain_in_system].z*Grain.Vol[data_grain_in_system];
			Ml[Layer] += Grain.Vol[data_grain_in_system]*sqrt(Grain.m[data_grain_in_system].x*Grain.m[data_grain_in_system].x
					    									 +Grain.m[data_grain_in_system].y*Grain.m[data_grain_in_system].y
					    									 +Grain.m[data_grain_in_system].z*Grain.m[data_grain_in_system].z);
			Avg_vol += Grain.Vol[data_grain_in_system];
		}
		Normaliser[Layer] = Avg_vol;
		Total_normaliser += Normaliser[Layer];

		MxT += Mx[Layer];
		MyT += My[Layer];
		MzT += Mz[Layer];
		MlT += Ml[Layer];
		Mx[Layer] /= Normaliser[Layer];
		My[Layer] /= Normaliser[Layer];
		Mz[Layer] /= Normaliser[Layer];
		Ml[Layer] /= Normaliser[Layer];
	}
	// Determine average values for combined system
	Normaliser.push_back(Total_normaliser);
	Mx.push_back(MxT/Normaliser.back());
	My.push_back(MyT/Normaliser.back());
	Mz.push_back(MzT/Normaliser.back());
	Ml.push_back(MlT/Normaliser.back());

	// OUTPUT_FILE initial values to file
	OUTPUT_FILE << Time << " " << Temperature << " " << Field_MAG << " ";
	for(size_t Layer=0;Layer<Mx.size();++Layer){
		OUTPUT_FILE << Mx[Layer] << " " << My[Layer] << " " << Mz[Layer] << " " << Ml[Layer] << " ";
	}
	OUTPUT_FILE << std::endl;

	OUTPUT_FILE.close();

	return 0;
}


