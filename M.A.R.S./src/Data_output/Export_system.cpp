/*
 * Export_system.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

#include <iostream>
#include <fstream>

#include "../../hdr/Structures.hpp"

int Export_system(const int Num_layers, const std::vector<double> dz, const Voronoi_t VORO,
		          const Interaction_t Int, const Grain_t Grain, const std::string Output_int){

	std::cout << "Exporting system" << std::endl;

	const int total_grains = VORO.Num_Grains*Num_layers;

	std::ofstream OUTPUT_VORO_FILE     ("Output/voro.dat");
	std::ofstream OUTPUT_ST_MAT_FILE   ("Output/st_mat.dat");
	std::ofstream OUTPUT_GPV_FILE      (("Output/gpv_"+ Output_int +".dat").c_str());
	std::ofstream OUTPUT_GPC_FILE      ("Output/gpc.dat");
	std::ofstream OUTPUT_GP_FILE       (("Output/gp_"+ Output_int +".dat").c_str());
	std::ofstream OUTPUT_INT_MN_FILE   ("Output/int_mn.dat");
	std::ofstream OUTPUT_INT_EN_FILE   ("Output/int_en.dat");
	std::ofstream OUTPUT_INT_W_FILE    ("Output/int_w.dat");
	std::ofstream OUTPUT_INT_HEXC_FILE ("Output/int_hexc.dat");

//#####################################################################################//

	OUTPUT_VORO_FILE << VORO.Centre_X << " " << VORO.Centre_Y << " "
					 << VORO.Vx_MAX   << " " << VORO.Vy_MAX << " "
					 << VORO.Vx_MIN   << " " << VORO.Vy_MIN << std::endl;

	OUTPUT_ST_MAT_FILE << VORO.Num_Grains << " " << Num_layers << "\n";
	for(int LAYER=0;LAYER<Num_layers;++LAYER){
		OUTPUT_ST_MAT_FILE << dz[LAYER] << " ";
	}
	OUTPUT_ST_MAT_FILE << std::endl;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################################INTERACTIONS#####################################//

	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		OUTPUT_INT_MN_FILE << grain_in_system << " " << Int.Magneto_neigh_list[grain_in_system].size() << std::endl;
		for(size_t Mneigh=0;Mneigh<Int.Magneto_neigh_list[grain_in_system].size();++Mneigh){
			OUTPUT_INT_MN_FILE << Int.Magneto_neigh_list[grain_in_system][Mneigh] << " ";
		}
		OUTPUT_INT_MN_FILE << std::endl;
	}

	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		OUTPUT_INT_EN_FILE << grain_in_system << " " << Int.Exchange_neigh_list[grain_in_system].size() << std::endl;
		for(size_t Neigh_num=0;Neigh_num<Int.Exchange_neigh_list[grain_in_system].size();++Neigh_num){
			OUTPUT_INT_EN_FILE << Int.Exchange_neigh_list[grain_in_system][Neigh_num] << " ";
		}
		OUTPUT_INT_EN_FILE << std::endl;
	}

	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		OUTPUT_INT_W_FILE << grain_in_system << " " << Int.Wxx[grain_in_system].size() << std::endl;
		for(size_t W=0;W<Int.Wxx[grain_in_system].size();++W){
			OUTPUT_INT_W_FILE << Int.Wxx[grain_in_system][W] << " " << Int.Wxy[grain_in_system][W] << " "
			                  << Int.Wxz[grain_in_system][W] << " " << Int.Wyy[grain_in_system][W] << " "
			                  << Int.Wyz[grain_in_system][W] << " " << Int.Wzz[grain_in_system][W] << std::endl;
		}
		OUTPUT_INT_W_FILE << std::endl;
	}

	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		OUTPUT_INT_HEXC_FILE << grain_in_system << " " << Int.H_exch_str[grain_in_system].size() << std::endl;
		for(size_t H=0;H<Int.H_exch_str[grain_in_system].size();++H){
			OUTPUT_INT_HEXC_FILE << Int.H_exch_str[grain_in_system][H] << " ";
		}
		OUTPUT_INT_HEXC_FILE << std::endl;
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################GRAIN PARAMETERS###################################//

	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		OUTPUT_GPV_FILE << Grain.m[grain_in_system]         << " "
						<< Grain.Easy_axis[grain_in_system] << " "
						<< Grain.H_appl[grain_in_system]    << std::endl;
	}

	for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
		OUTPUT_GP_FILE << Grain.Temp[grain_in_system] << " " << Grain.Vol[grain_in_system] << " "
				       << Grain.K[grain_in_system]    << " " << Grain.Tc[grain_in_system]  << " "
				       << Grain.Ms[grain_in_system]   << " " << Grain.Crit_exp[grain_in_system] << std::endl;
	}

	for(int LAYER=0;LAYER<Num_layers;++LAYER){
		int Offset = VORO.Num_Grains*LAYER;
		OUTPUT_GPC_FILE << Grain.Callen_power_range[Offset] << std::endl;
		if(Grain.Callen_power_range[Offset]=="single"){
			for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
				int grain_in_system = Offset+grain_in_layer;
				OUTPUT_GPC_FILE << Grain.Callen_power[grain_in_system] << std::endl;
			}
		}
		else if(Grain.Callen_power_range[Offset]=="multiple"){
			for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
				int grain_in_system = Offset+grain_in_layer;
				OUTPUT_GPC_FILE << Grain.Callen_factor_lowT[grain_in_system]  << " " << Grain.Callen_power_lowT[grain_in_system]  << " "
								<< Grain.Callen_range_lowT[grain_in_system]   << " " << Grain.Callen_factor_midT[grain_in_system] << " "
								<< Grain.Callen_power_midT[grain_in_system]   << " " << Grain.Callen_range_midT[grain_in_system]  << " "
								<< Grain.Callen_factor_highT[grain_in_system] << " " << Grain.Callen_power_highT[grain_in_system] << std::endl;
			}
		}
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	return 0;
}
