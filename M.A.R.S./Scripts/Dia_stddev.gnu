#!/usr/bin/gnuplot

f0 = "../Output/DIAMETER_FILE.dat"

#bin_width
#PARTICLES

bin_width=0.1
set boxwidth bin_width
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * bin_number(x) + bin_width/2.0 

galton(x)=(1.0/(x*(sqrt(log(1.0+(sigma*sigma)/(mu*mu))))*sqrt(2.*pi)))*exp(-((log(x)-(log(mu/sqrt(1.0+((sigma*sigma)/(mu*mu))))))**2)/(2.*(sqrt(log(1.0+(sigma*sigma)/(mu*mu))))**2))

gauss(x)=1.0/(sigma*sqrt(2.*pi))*exp(-(x-mu)**2./(2.*sigma**2))

set table "../Output/AREA_gnutable.dat"

plot f0 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes

unset table

sigma=21.6
mu = 10

plot f0 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes, gauss (x) lw 2
#plot f0 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes, "../Output/AREA_gnutable.dat" u 1:2 w lp ps 2 pt 6, gauss (x) lw 2
#galton(x) lw 2,gauss(x) lw 2

set fit logfile "../Output/AREA_gnulogfile.dat"

#fit galton(x) "../Output/AREA_gnutable.dat" using 1:2 via mu, sigma
fit gauss(x) "../Output/AREA_gnutable.dat" using 1:2 via mu, sigma

pause -1
