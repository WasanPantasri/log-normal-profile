
f0 = "../Output/Final_magnetisation.dat"

set xlabel "Tp"
set ylabel "dM_s/dT (emu/Tcc)"

Ms=1





# derivative functions.  Return 1/0 for first point, otherwise delta y or (delta y)/(delta x)
d(y)=($0==0)?(y1=y,1/0):(y2=y1,y1=y,y1-y2)
d2(x,y)=($0==0)?(x1=x,y1=y,1/0):(x2=x1,x1=x,y2=y1,y1=y,(y1-y2)/(x1-x2))

set key top left Left reverse

# offset for derivatives (half the x spacing)
dx = 0.25

plot f0 u ($4-dx):(d2($4,$3*Ms)) t '2-variable derivative', \
     '' u ($4-dx):(d2($4,$3*Ms)) smooth csplines t '2-variable derivative (smoothed)'
     
# p f0 u ($4):($3*Ms) w l t 'data'



pause -1
