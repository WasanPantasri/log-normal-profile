set term postscript eps color enhanced font "Times-Roman,16"
set output "Sigma_Tc_fit.eps"

system('echo -n "\tFitting to differentiated..."')

####### File
#OG_data_file = "Final_magnetisation.dat"

####### Data inputs
#Ms=1100
#T_min=500
#T_max=800
#T_step=0.5

####### Gnuplot inputs
#interp_min=5
#interp_max=33
#interp_steps=2

####### Fit parameters
#Height=20
#Centre=650
#Width=20

####### Derivative
dx=T_step*0.5
d(y)=($0==0)?(y1=y,1/0):(y2=y1,y1=y,y1-y2)
x0=NaN
y0=NaN

# Determine inital Sigma_tc for original data

set fit logfile "Sigma_Tc_fit_0_point_logfile.dat"
set fit quiet
set print "Sigma_Tc_values.dat" append

Gauss(x) = Height*exp(-((x-Centre)**2)/(2*Width**2))

fit Gauss(x) OG_data_file u ($4-dx):(d($3*Ms)) via Height,Centre,Width

print 0,Height,Centre,Width


# loop over all interpolations
do for [i=interp_min:interp_max:interp_steps]{
	interp_points=i


	input_file(interp_points)=sprintf("Derivative_data_%.0f_point.dat",interp_points)
	fit_logfile(interp_points)=sprintf("Sigma_Tc_fit_%.0f_point_logfile.dat",interp_points)
	set fit logfile fit_logfile(interp_points)		# All fit logs now go to this file
	set fit quiet
	set print "Sigma_Tc_values.dat" append

	Gauss(x) = Height*exp(-((x-Centre)**2)/(2*Width**2))

	fit Gauss(x) input_file(interp_points) u 1:3 via Height,Centre,Width

	print interp_points,Height,Centre,Width
	
	p OG_data_file u ($4-dx):(d($3*Ms)) w p ps 0.5 t 'OG data',\
	  input_file(interp_points) u 1:3 w p pt 6 ps 0.5 t 'Smoothed data'.interp_points.'-points',\
	  Gauss(x) w l lc -1 t 'Gaussian fit'
}

system('echo "done."')
