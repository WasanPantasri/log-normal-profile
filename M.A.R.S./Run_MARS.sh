DataLoc="Simulation_`date +%Y_%m_%d_Time_%H:%M`"

mkdir simulations/$DataLoc
mkdir simulations/$DataLoc/M.A.R.S.

make voro

cp -r ../voro++-0.4.6 simulations/$DataLoc

make

mv MARS        simulations/$DataLoc/M.A.R.S.
cp -r hdr     simulations/$DataLoc/M.A.R.S.
cp -r src     simulations/$DataLoc/M.A.R.S.
cp -r obj     simulations/$DataLoc/M.A.R.S.
cp -r Input   simulations/$DataLoc/M.A.R.S.
cp -r Scripts simulations/$DataLoc/M.A.R.S.
cp Makefile    simulations/$DataLoc/M.A.R.S.
cp Run_MARS.sh simulations/$DataLoc/M.A.R.S.

mkdir simulations/$DataLoc/M.A.R.S./Output

cd simulations/$DataLoc/M.A.R.S.

printf "\n*****\n*****\nView simulation progress with: \n\t'less -R simulations/$DataLoc/M.A.R.S./Terminal.dat'\n\t Use Shift+F for live updates to output file in less.\n*****\n*****\n"

./MARS > Terminal.dat


