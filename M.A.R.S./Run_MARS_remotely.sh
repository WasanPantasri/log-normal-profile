DataLoc="Simulation_`date +%Y_%m_%d_Time_%H:%M`"

TESTING=$1

mkdir simulations/$DataLoc
mkdir simulations/$DataLoc/M.A.R.S.

cp -r ../voro++-0.4.6 simulations/$DataLoc

make

mv MARS        simulations/$DataLoc/M.A.R.S.
cp -r hdr     simulations/$DataLoc/M.A.R.S.
cp -r src     simulations/$DataLoc/M.A.R.S.
cp -r obj     simulations/$DataLoc/M.A.R.S.
cp -r Input   simulations/$DataLoc/M.A.R.S.
cp -r Scripts simulations/$DataLoc/M.A.R.S.
# Copy test directories only if desired
# To specify copying of tests use: " sh Run_MARS_remotely.sh test "
if [ -z $TESTING ]
then
	echo "" 
elif [ $TESTING = "test" ]
then
	cp -r Tests/   simulations/$DataLoc/M.A.R.S.
fi

cp Makefile    simulations/$DataLoc/M.A.R.S.
cp Run_MARS.sh simulations/$DataLoc/M.A.R.S.
cp Run_MARS_remotely.sh simulations/$DataLoc/M.A.R.S.
cp Submit_MARS.qsub simulations/$DataLoc/M.A.R.S.

mkdir simulations/$DataLoc/M.A.R.S./Output

cd simulations/$DataLoc/M.A.R.S.

qsub Submit_MARS.qsub

