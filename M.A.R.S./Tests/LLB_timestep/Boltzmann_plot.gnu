set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (rad)"
set xrange[0:]
set yrange[0:]

#===================10fs 300K===================#
set output "LLB_Boltzmann_test_300K_10.0fs.eps"
f0 = "Output/LLB300.000000K_10.000000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 10fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================10fs 1000K===================#
set output "LLB_Boltzmann_test_1000K_10.0fs.eps"
f0 = "Output/LLB1000.000000K_10.000000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 10fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================5fs 300K===================#
set output "LLB_Boltzmann_test_300K_5.0fs.eps"
f0 = "Output/LLB300.000000K_5.000000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 5fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================5fs 1000K===================#
set output "LLB_Boltzmann_test_1000K_5.0fs.eps"
f0 = "Output/LLB1000.000000K_5.000000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 5fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================1fs 300K===================#
set output "LLB_Boltzmann_test_300K_1.0fs.eps"
f0 = "Output/LLB300.000000K_1.000000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 1fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================1fs 1000K===================#
set output "LLB_Boltzmann_test_1000K_1.0fs.eps"
f0 = "Output/LLB1000.000000K_1.000000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 1fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical'
   
#===================0.5fs===================#
set output "LLB_Boltzmann_test_300K_0.5fs.eps"
f0 = "Output/LLB300.000000K_0.500000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 0.5fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================0.5fs===================#
set output "LLB_Boltzmann_test_1000K_0.5fs.eps"
f0 = "Output/LLB1000.000000K_0.500000fs.dat"

set xrange[0:]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t 0.5fs",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  

