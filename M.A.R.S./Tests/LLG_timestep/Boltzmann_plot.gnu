set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (rad)"
set xrange[0:]
set yrange[0:]

  
#===================10ps===================#
set output "LLG_Boltzmann_test_300K_10ps.eps"
f0 = "Output/LLG300.000000K_10.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t10ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================9ps===================#
set output "LLG_Boltzmann_test_300K_9ps.eps"
f0 = "Output/LLG300.000000K_9.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t9ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================8ps===================#
set output "LLG_Boltzmann_test_300K_8ps.eps"
f0 = "Output/LLG300.000000K_8.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t8ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================7ps===================#
set output "LLG_Boltzmann_test_300K_7ps.eps"
f0 = "Output/LLG300.000000K_7.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t7ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================6ps===================#
set output "LLG_Boltzmann_test_300K_6ps.eps"
f0 = "Output/LLG300.000000K_6.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t6ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================5ps===================#
set output "LLG_Boltzmann_test_300K_5ps.eps"
f0 = "Output/LLG300.000000K_5.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t5ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================4ps===================#
set output "LLG_Boltzmann_test_300K_4ps.eps"
f0 = "Output/LLG300.000000K_4.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t4ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================3ps===================#
set output "LLG_Boltzmann_test_300K_3ps.eps"
f0 = "Output/LLG300.000000K_3.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t3ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
  
#===================2ps===================#
set output "LLG_Boltzmann_test_300K_2ps.eps"
f0 = "Output/LLG300.000000K_2.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t2ps",\
  '' u ($1+0.005):($5/$6) w l lw 2 t 'Analytical'

#===================1ps===================#
set output "LLG_Boltzmann_test_300K_1ps.eps"
f0 = "Output/LLG300.000000K_1.000000ps.dat"

set xrange[0:0.5]

p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data {/Symbol d}t=1ps",\
  '' u ($1+0.005):($5/$6) w l lw 3 t 'Analytical' 
