K  = 5.0e+6 
Ms = 800.0
Hk = 2*K/Ms
f0= 10.0**(9)
Temp=100.0
V = 554.256258422042*1e-21
Kb = 1.3806485279e-16
beta(T) = K*V/(Kb*T)
Hc(x) = x 
h(x) = Hc(x)/Hk
a(x) = 1.0 - h(x)

set term postscript eps enhanced color font "Helvetica,16"
set key bottom right #at 1600,15

set output "R_f_Hc_1_10K.eps"
set ylabel "H_c/H_K"  offset 2,0
set xlabel "Field sweep rate (Oe/s)"

#set title"R= f(H_c)"

set xrange [10.0**(-4):10.0**17]

do for [Temp=100:700:100]{
	log_R(x,T) =  -1*( beta(T)*a(x)**2 + log( a(x) ) - log( Hk*f0/(2*beta(T)) ) ) #ln(R) = f(Hc)
	set table "Output/theoretical2_".Temp.".dat"
	p [0:Hk] exp(log_R(x,Temp))
	unset table
}

system("for i in 100 200 300 400 500 600 700; do head -n -2 Output/theoretical2_$i.dat > Output/theoretical_$i.dat; done")
!rm Output/theoretical2_*

set logscale x 
p "Output/theoretical_100.dat" u ($2):($1/Hk) title '100K' w l lc 1,\
  "Output/KMC_Hc.dat" i 0 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 1,\
  "Output/theoretical_200.dat" u ($2):($1/Hk) title '200K' w l lc 2,\
  "Output/KMC_Hc.dat" i 1 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 2,\
  "Output/theoretical_300.dat" u ($2):($1/Hk) title '300K' w l lc 3,\
  "Output/KMC_Hc.dat" i 2 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 3,\
  "Output/theoretical_400.dat" u ($2):($1/Hk) title '400K' w l lc 4,\
  "Output/KMC_Hc.dat" i 3 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 4,\
  "Output/theoretical_500.dat" u ($2):($1/Hk) title '500K' w l lc 5,\
  "Output/KMC_Hc.dat" i 4 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 5,\
  "Output/theoretical_600.dat" u ($2):($1/Hk) title '600K' w l lc 6,\
  "Output/KMC_Hc.dat" i 5 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 6,\
  "Output/theoretical_700.dat" u ($2):($1/Hk) title '700K' w l lc 7,\
  "Output/KMC_Hc.dat" i 6 u (($1)):(($4-$3)/(2*Hk)) t ''   w p pt 7 lc 7
