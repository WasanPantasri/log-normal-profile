set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (degrees)"
set xrange[0:30]
set yrange[0:]
set style fill solid

#===================LLG====================#
#===================900K===================#
set output "LLG_Boltzmann_test_900K.eps"
f0 = "Output/LLG_900.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=900K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================500K===================#
set output "LLG_Boltzmann_test_500K.eps"
f0 = "Output/LLG_500.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=500K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================300K===================#
set output "LLG_Boltzmann_test_300K.eps"
f0 = "Output/LLG_300.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================50K====================#
set output "LLG_Boltzmann_test_50K.eps"
f0 = "Output/LLG_50.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=50K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

#===================LLB====================#
set xrange[0:180]
#===================900K===================#
set output "LLB_Boltzmann_test_900K.eps"
f0 = "Output/LLB_900.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=900K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================500K===================#
set output "LLB_Boltzmann_test_500K.eps"
f0 = "Output/LLB_500.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=500K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================300K===================#
set output "LLB_Boltzmann_test_300K.eps"
f0 = "Output/LLB_300.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=300K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================50K====================#
set output "LLB_Boltzmann_test_50K.eps"
f0 = "Output/LLB_50.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=50K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================800K===================#
set output "LLB_Boltzmann_test_800K.eps"
f0 = "Output/LLB_800.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=800K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================900K===================#
set output "LLB_Boltzmann_test_750K.eps"
f0 = "Output/LLB_750.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=750K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

